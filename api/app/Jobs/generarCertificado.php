<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Expediente;
use App\Models\Certificado;
use App\Events\CertificadoGeneradoEvent;
use App\Models\Configuracion;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode as QrCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;









class generarCertificado implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $expediente;
    public $dataExtraExpediente;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente, $dataExtraExpediente,) {

        $this->expediente = $expediente;
        $this->dataExtraExpediente = $dataExtraExpediente;
    }




    
    
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle() {
        

        $expediente = $this->expediente;
        $dataExtraExpediente = $this->dataExtraExpediente;
        
        
        try {

            if ($dataExtraExpediente[0]->objeto === 'Nuevo'){
                $objetoDestino = 'Obra Nueva';
            } elseif ($expediente->superficie_a_construir == null) {
                $objetoDestino  = 'Obra Existente con permiso';
            } else {
                $objetoDestino  = 'Obra Existente sin permiso';
            }
    
            //conseguir datos
                
            $from = 0;
            $to = 1844674407370955161;
            $randomNumber = rand($from, $to);
            
            $nombre_archivo_logo = Configuracion::get()->first()->nombre_archivo_logo;

            
            $codigoQR = base64_encode(QrCode::format('png')->size(115)
                ->generate('http://192.168.0.138:8000/validador_certificados/'.$randomNumber)
            );

            $nombreArchivo = "Certificado - Expet Nº " . $expediente->expediente_numero . '.pdf';

            $pdf = PDF::loadView('pdfs.certificado',
                compact (
                    'expediente', 'dataExtraExpediente', 'objetoDestino',
                    'nombre_archivo_logo', 'codigoQR', 'randomNumber'
                )
            );

            $pdf->output();
            $dom_pdf = $pdf->getDomPDF();
            
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(500, 820, "Página {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            
            /*  return $pdf; */

            DB::beginTransaction();

                // almacenar certificado
                $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires');
            
                $certificado = Certificado::create([
                
                    'certificado'=> $nombreArchivo,
                    'expediente_id'=>$expediente->expediente_id,
                    'codigo_qr' => $codigoQR,
                    'random_number' => $randomNumber,
                    'fecha'=>$fecha,
                ]);   
            
                
                /* dd($certificado); */
            
                $expediente->update([
                    'certificado_id' => $certificado->certificado_id,
                ]);
            
            
            
            
                // PARA NOTIFICAR
                
                event(New CertificadoGeneradoEvent($expediente, $dataExtraExpediente));
            
            
            
                // 
                Storage::put('public/certificados/' . $nombreArchivo, $pdf->output());
            
                /* $ruta = $pdf->storeAs($rutaDestino, $nombreArchivo);    no funcionó, por eso usamos Storage::put       */

            DB::commit();

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;


        }

    }



    


}
