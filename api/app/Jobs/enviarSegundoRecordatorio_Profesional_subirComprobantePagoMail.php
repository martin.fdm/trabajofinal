<?php

namespace App\Jobs;

use App\Events\SegundoRecordatorio_Profesional_subirComprobantePagoEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;
use App\Mail\SegundoRecordatorio_Profesional_subirComprobantePagoMail;
use App\Models\Configuracion;
use App\Models\Expediente;
use App\Models\User;







class enviarSegundoRecordatorio_Profesional_subirComprobantePagoMail implements ShouldQueue {
    
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $expediente;
    protected $dataForEmail;
    /* protected $emisor; */



    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente, $dataForEmail/* , $emisor */) {

        $this->expediente = $expediente;
        $this->dataForEmail = $dataForEmail;
       /*  $this->emisor = $emisor; */
    }






    
    /**
    * Execute the job.
    *
    * @return void
    */

    public function handle() {


        $expediente = $this->expediente;
        $dataForEmail = $this->dataForEmail;
        /* $emisor = $this->emisor; */



        if ($expediente->condicion_id >= config('app.esperando_comprobante_1') && $expediente->condicion_id <= config('app.esperando_comprobante_3')){


            
            $profesional_email = (User::where('profesional_id','=', $expediente->profesional_id)->get()->first())->email;           

            $propietario_email = $dataForEmail[0]->propietario_email;



            if ($propietario_email != "") {

                $emails = array($profesional_email, $propietario_email);
            }
    
            try{


                // NOTIFICACION

                $evento = event (new SegundoRecordatorio_Profesional_subirComprobantePagoEvent($expediente/* , $emisor */));


                // MAIL

                $correo = new SegundoRecordatorio_Profesional_subirComprobantePagoMail($expediente, $dataForEmail);
                Mail::to(isset($emails) ? $emails : $profesional_email)->send($correo);

                /* dd($correo); */



                /**
                *  LANZAMOS EL 3er JOB DEL PROCESO AUTOMATIZADO 1: Interacción con el profesional para cerrar el expediente
                **/

                $cantidad_dias = Configuracion::get()->first()->tiempo_cerrar_expediente_1er_proceso_automatizado;

                finalProcesoAutomatizado_cerrarExpediente::dispatch($expediente)
                /* ->delay(now()->addDays($cantidad_dias)) */
                /* ->delay(now()->addMinute()) */
                ->delay(now()->addSeconds($cantidad_dias)); 




                return 0;

            } catch(\Exception $e){
                return $e;
            }

            
        } else {
            
            return ("Enviar Mail Segundo Recordatorio al Profesional para subir Comprobante de Pago y 3er Job para Cerrar Expediente Cancelado");
        }
    }



}
