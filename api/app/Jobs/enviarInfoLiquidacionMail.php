<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Mail\InfoLiquidacionMail;
use App\Models\Expediente;
use Illuminate\Support\Facades\Mail;
use App\Models\User;








class enviarInfoLiquidacionMail implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $expediente;
    protected $dataForEmail;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente, $dataForEmail) {

        $this->expediente = $expediente;
        $this->dataForEmail = $dataForEmail;
    }




    
    
    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle() {
        

        $expediente = $this->expediente;
        $dataForEmail = $this->dataForEmail;

        /* dd($dataForEmail[0]->profesional_nombres, $dataForEmail[0]->profesional_apellidos); */


        $profesional_email = (User::where('profesional_id','=',$expediente->profesional_id)->get()->first())->email;


        /* dd($profesional_mail); */
        
        // BUSCAR MAIL DE PROPIETARIO.

        $propietario_email = $dataForEmail[0]->propietario_email;


        /* dd($propietario_email); */

       /*  $propietario_mail = null; */


        if ($propietario_email != "") {

            $emails = array($profesional_email, $propietario_email );
        }


        try{

            $correo = new InfoLiquidacionMail($expediente);
            Mail::to(isset($emails) ? $emails : $profesional_email)->send($correo);
    
            return 0;       
        
        } catch(\Exception $e){
            return $e;
        } catch(\Throwable $e){
            return $e;
        }


    }




}
