<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


















class NotaReemplazoProfesional extends Model implements Auditable {
    
    
    use HasFactory;
    use \OwenIt\Auditing\Auditable;



    protected $table='notas_reemplazo_profesional';

    protected $primaryKey='nota_reemplazo_profesional_id';

    public $timestamps=false;





    protected $filelable=[

        'profesional_entrante_id',
        'profesional_saliente_id',
        'expediente_afectado_id',
        'expediente_afectado_numero',
        'fecha',
        'nota_reemplazo_profesional',
        'nombre_archivo',
        'prueba_avance_1',
        'prueba_avance_2',
        'nota_aprobada',
        'condicion_nrp_id',
        'observaciones',
        'pendiente_cambios',
        'alta',

        
    ];





    protected $guarded = [
        
    ];





    // Relaciones: 


    public function expediente(){
        return $this->hasOne('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }



    public function profesional(){
        return $this->belongsTo('App\Models\Profesional', 'profesional_entrante_id', 'id');
    }



    public function condicionNRP(){
        return $this->hasOne('App\Models\CondicionNRP', 'condicion_nota_reemplazo_profesional_id', 'condicion_nota_reemplazo_profesional_id');
    }




}

 


