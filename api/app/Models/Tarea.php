<?php

namespace App\Models;




use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;













class Tarea extends Model  implements Auditable {


    use HasFactory;
    use \OwenIt\Auditing\Auditable;



    protected $table='tareas';

    protected $primaryKey='tarea_id';

    public $timestamps=false;





    protected $filelable=[

        // fks
        'tipo_tarea_id',
        'plano_id',
        'expediente_id',

        'fecha_inicio',
        'fecha_fin',
        'reemplazo',
    ];




    protected $guarded = [
        
    ];





    
    // Relaciones: 


    public function plano(){
        return $this->hasOne('App\Models\Plano', 'plano_id', 'plano_id');
    }



    public function tipoTarea(){
        return $this->hasOne('App\Models\TipoTarea', 'tipo_tarea_id', 'tipo_tarea_id');
    }



    public function expediente(){
        return $this->belongsTo('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }



}
 


