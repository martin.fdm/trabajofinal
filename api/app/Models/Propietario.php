<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;














class Propietario extends Model implements Auditable {


    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='propietarios';

    protected $primaryKey='id';

    public $timestamps=false;

    protected $appends = ['full_name'];





    protected $filelable=[
        'propietario_nombres',
        'propietario_apellidos',
        'propietario_cuit',
        'propietario_email',
        'profesional_id',
        'alta'
    ];




    protected $guarded = [
        
    ];





    // Relaciones: FUNCIONAN AMBAS

    
    public function obras(){
        return $this->hasMany('App\Models\Obra');
    }



    public function profesionales(){
        
        return $this->belongsToMany('App\Models\Profesional', 'profesional_propietario', 'propietario_id', 'profesional_id')/* ->withDefault() */;

    }






    // Más métodos, propios 

  
    public function getFullName(){
        return $this->propietario_nombres . " " . $this->propietario_apellidos;
    }




}



