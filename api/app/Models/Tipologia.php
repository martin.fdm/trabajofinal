<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;














class Tipologia extends Model implements Auditable {


    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='tipologias';

    protected $primaryKey='tipologia_id';

    public $timestamps=false;





    protected $filelable=[
        'tipologia',

        // fks
        'expediente_id',

    ];
    





    protected $guarded = [
        
    ];





    

    // Relaciones:


    public function expedientes(){
        return $this->belongsToMany('App\Models\Expediente');
    }




}
 


