<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;









class Obra extends Model implements Auditable {


    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='obras';

    protected $primaryKey='obra_id';


    public $timestamps=false;



    
    protected $filelable=[


        // fks

        'expediente_id',
        'localidad_id',
        'propietario_id',
        
        'seccion',
        'chacra',
        'manzana',
        'parcela',
        
        'calle',
        'numero',
        'barrio',
        
        'partida_inmobiliaria',
        
    ];

    protected $guarded = [
        
    ];








    // Relaciones: 
    

    public function expedientes(){
        return $this->belongsToMany('App\Models\Expediente', 'expediente_id', 'expediente_id');
    }


    public function localidad(){
        return $this->hasOne('App\Models\Localidad', 'localidad_id', 'localidad_id');
    }



    //FUNCIONA
    public function propietario(){
        return $this->belongsTo('App\Models\Propietario');
    }



    public function partidasInmobiliarias(){
        return $this->hasMany('App\Models\PartidaInmobiliaria');
    }



    

}
 


