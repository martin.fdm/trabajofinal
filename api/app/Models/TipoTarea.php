<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;















class TipoTarea extends Model implements Auditable {

    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    protected $table='tipo_tareas';

    protected $primaryKey='tipo_tarea_id';

    public $timestamps=false;








    protected $filelable=[
        'tipo_tarea',

        // fks
        'tarea_id',
    ];







    protected $guarded = [
        
    ];






    

    // Relaciones:


    public function tareas(){
        return $this->belongsToMany('App\Models\Tarea');
    }


}
 


