<?php

namespace App\View\Components\comboBox;

use Illuminate\View\Component;




class tipologias extends Component{


    public $content;

    /**
     * Create a new component instance.
     *
     * @return void
     */

     
    public function __construct($content)
    {
        $this->content=$content;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.combo-box.tipologias');
    }
}
