<?php

namespace App\Repositories;

use App\Models\Profesional;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Repositories\PropietarioQueries;
use Illuminate\Support\Facades\Auth;






class ExpedienteQueries  {


    public $propietarioQueries;




    public function __construct (PropietarioQueries $propietarioQueries){

        $this->propietarioQueries = $propietarioQueries;
    
    }






    public function getDataForExpedienteAdministrativoIndex ($request){

        /* dd($request); */


        $expedienteNumero = $request->input('expediente_numero');
        $profesional = $request->input('profesional');
        $propietario = $request->input('propietario');
        $tipologia = $request->input('tipologia');
        $tipo_tarea = $request->input('tipo_tarea');
        $localidad = $request->input('localidad');
        $estado = $request->input('estado');
        $condicion = $request->input('condicion');
        $objeto = $request->input('objeto');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        $data= DB::table('expedientes')

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')

        //recuperar tipologia de expediente
        ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expedientes.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        //recuperar condicion de expediente
        ->join('condiciones', 'expedientes.condicion_id', '=', 'condiciones.condicion_id')

            

        ->select (
            'expedientes.*',
            'prop.propietario_nombres', 'prop.propietario_apellidos', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 
            'obras.partida_inmobiliaria',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion_short',
        )

        ->when($expedienteNumero, function ($query, $expedienteNumero) {
            return $query->where('expedientes.expediente_numero', $expedienteNumero);
        })

        ->when($profesional, function ($query, $profesional) {
            return $query->where('expedientes.profesional_id', $profesional);
        })

        ->when($propietario, function ($query, $propietario) {
            return $query->where('obras.propietario_id', $propietario);
        })

        ->when($tipologia, function ($query, $tipologia) {
            return $query->where('expedientes.tipologia_id', $tipologia);
        })

        ->when($tipo_tarea, function ($query, $tipo_tarea) {
            return $query->where('tareas.tipo_tarea_id', $tipo_tarea);
        })

        ->when($localidad, function ($query, $localidad) {
            return $query->where('obras.localidad_id', $localidad);
        })

        ->when($estado, function ($query, $estado) {
            return $query->where('expedientes.estado_id', $estado);
        })

        ->when($condicion, function ($query, $condicion) {
            return $query->where('expedientes.condicion_id', $condicion);
        })

        ->when($objeto, function ($query, $objeto) {
            return $query->where('expedientes.objeto_id', $objeto);
        })
        
        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        })

        ->orderBy('expedientes.prioridad_administracion','asc')
        ->orderBy('expedientes.fecha_inicio', 'desc')
        ->paginate(15);
        // ->get();

        // debug($data);
        return $data;

    }










    public function getDataForExpedienteProfesionalIndex ($request){

        $expedienteNumero = $request->input('expediente_numero');
        $propietario = $request->input('propietario');
        $tipologia = $request->input('tipologia');
        $tipo_tarea = $request->input('tipo_tarea');
        $localidad = $request->input('localidad');
        $estado = $request->input('estado');
        $condicion = $request->input('condicion');
        $objeto = $request->input('objeto');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');


        $data= DB::table('expedientes')

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')

        //recuperar tipologia de expediente
        ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expedientes.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        //recuperar condicion de expediente
        ->join('condiciones', 'expedientes.condicion_id', '=', 'condiciones.condicion_id')

        
        
        ->select (
            'expedientes.*',
            'prop.propietario_nombres', 'prop.propietario_apellidos', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 
            'obras.partida_inmobiliaria',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion_short',
        )

        ->where('expedientes.profesional_id', '=', Auth()->user()->profesional_id)

        ->when($expedienteNumero, function ($query, $expedienteNumero) {
            return $query->where('expedientes.expediente_numero', $expedienteNumero);
        })

        ->when($propietario, function ($query, $propietario) {
            return $query->where('obras.propietario_id', $propietario);
        })

        ->when($tipologia, function ($query, $tipologia) {
            return $query->where('expedientes.tipologia_id', $tipologia);
        })

        ->when($tipo_tarea, function ($query, $tipo_tarea) {
            return $query->where('tareas.tipo_tarea_id', $tipo_tarea);
        })

        ->when($localidad, function ($query, $localidad) {
            return $query->where('obras.localidad_id', $localidad);
        })

        ->when($estado, function ($query, $estado) {
            return $query->where('expedientes.estado_id', $estado);
        })

        ->when($condicion, function ($query, $condicion) {
            return $query->where('expedientes.condicion_id', $condicion);
        })

        ->when($objeto, function ($query, $objeto) {
            return $query->where('expedientes.objeto_id', $objeto);
        })
        
        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('expedientes.fecha_inicio', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('expedientes.fecha_inicio', '<=' , $fecha2);
        })

        ->orderBy('expedientes.prioridad_profesional','asc')
        ->orderBy('expedientes.fecha_inicio', 'desc')
        ->paginate(15);



        return $data;

    }
















    public function getExpedientesNumerosExistentes($currentUserRoles){


        if ($currentUserRoles->contains('profesional')){

            $profesional_id = Auth()->user()->profesional_id;
            $expedientesNumeros = DB::table('expedientes')
            ->select('expedientes.expediente_numero')
            ->where('expedientes.profesional_id', '=', $profesional_id)
            ->orderBy('expedientes.expediente_id', 'desc')
            ->get();

            /* dd($expedientesNumeros); */

        } else {

            $expedientesNumeros = DB::table('expedientes')
            ->select('expedientes.expediente_numero')
            ->orderBy('expedientes.expediente_id', 'desc')
            ->get();

        }      


        return $expedientesNumeros;

    }










    public function getDataForIndexFiltros($currentUserRoles){

        $profesionales=DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('id','desc')
        ->get();


        $propietarios = $this->propietarioQueries->getPropietariosForUser($currentUserRoles);


        $estados = DB::table('estados')
        ->orderBy('estado_id','asc')
        ->get();


        $condiciones = DB::table('condiciones')
        ->orderBy('condicion_id','asc')
        ->get();

        
        $objetos = DB::table('objetos')
        ->orderBy('objeto_id','asc')
        ->get();


        $tipos_tareas= DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')
        ->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')
        ->get();


        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')
        ->get();


        return ([
            $profesionales,
            $propietarios,
            $estados,
            $condiciones,
            $objetos,
            $tipos_tareas,
            $tipologias,
            $localidades,
        ]);   
    

    }






















    public function getDataToCreateExpediente(){

        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')->get();


        $objetos=DB::table('objetos')
        ->orderBy('objeto_id','asc')->get();


        $profesionales=DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('id','desc')->get();


        $currentUserRole = Auth()->user()->getRoleNames();
        // $currentUserRole = $currentUserRole[0];

        $propietarios = $this->propietarioQueries->getPropietariosForUser($currentUserRole);



        /* $propietarios=DB::table('propietarios')
        ->where('alta','=','1')
        ->orderBy('propietario_id','desc')->get(); */

        

        $tipos_tareas=DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')->get();

        return ([
            $localidades,
            $objetos,
            $profesionales,
            $propietarios,
            $tipos_tareas,
            $tipologias
        ]);   
    

    }








    public function getDataToShowExpediente($id){

        /* dd($id); */
        // data es el conjunto de datos relacionados al expediente que se encuentra por id 
        $data = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')


        ->join('objetos', 'expt.objeto_id', '=', 'objetos.objeto_id')
        ->join('estados', 'expt.estado_id', '=', 'estados.estado_id')

        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')


        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')


        /* ->join('obras', 'localidades.localidad_id', '=', 'obras.localidad_id') */
        ->join('localidades', 'obras.localidad_id', '=', 'localidades.localidad_id')


        ->join('planos', 'tareas.plano_id', '=', 'planos.plano_id')
        
        ->join('condiciones', 'expt.condicion_id', '=', 'condiciones.condicion_id')



        ->select (
        'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.id as propietario_id',
        'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula', 
        'objetos.objeto',
        'tipologias.tipologia', 
        'tipos_tareas.tipo_tarea', 'tipos_tareas.tipo_tarea_id',
        'localidades.localidad',
        'obras.*',
        'estados.*',
        'planos.nombre_archivo' ,
        'expt.expediente_id',
        'condiciones.condicion',
        )

        ->where('expt.expediente_id', '=', $id)

        ->get();

    

        return $data;   

    }









    public function getDataToShowExpedienteConComprobantePago($id){

        /* dd($id); */
        // data es el conjunto de datos relacionados al expediente que se encuentra por id 
        $data = DB::table(DB::raw('expedientes'))


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')


        ->join('objetos', 'expedientes.objeto_id', '=', 'objetos.objeto_id')
        ->join('estados', 'expedientes.estado_id', '=', 'estados.estado_id')
        
        ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')
        
        ->join('tareas', 'expedientes.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        ->join('planos', 'tareas.plano_id', '=', 'planos.plano_id')
        
        
        ->join('localidades', 'obras.localidad_id', '=', 'localidades.localidad_id')
        
        ->join('condiciones', 'expedientes.condicion_id', '=', 'condiciones.condicion_id')

        ->join('comprobantes_pago','expedientes.comprobante_pago_id', '=' ,'comprobantes_pago.comprobante_pago_id')


        ->select (
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.propietario_email', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula', 'prof.alta as profesional_alta',
            'objetos.objeto',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea', 'tipos_tareas.tipo_tarea_id',
            'localidades.localidad',
            'obras.*',
            'estados.*',
            'planos.nombre_archivo' ,
            'expedientes.expediente_id', 
            'expedientes.superficie_a_construir', 'expedientes.superficie_con_permiso', 'expedientes.superficie_sin_permiso',
            'condiciones.condicion',
            'comprobantes_pago.nombre_archivo as comprobantePago_nombreArchivo',    
        )

        ->where('expedientes.expediente_id', '=', $id)
        ->get();



        /* dd($data); */
            
        return $data;   

    }








    public function expedienteHasPlano($id){

        $data = DB::table('expedientes as expt')

        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')

        ->join('planos', 'tareas.plano_id', '=', 'planos.plano_id')

        ->select ('planos.plano_id')
        ->where('expt.expediente_id', '=', $id)
        ->get();

        return $data;

    }














    public function getDataToShowExpedienteSinPlano($id){

        /* dd($id); */
        // data es el conjunto de datos relacionados al expediente que se encuentra por id 
        $data = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')


        ->join('objetos', 'expt.objeto_id', '=', 'objetos.objeto_id')
        ->join('estados', 'expt.estado_id', '=', 'estados.estado_id')

        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')


        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')


        /* ->join('obras', 'localidades.localidad_id', '=', 'obras.localidad_id') */
        ->join('localidades', 'obras.localidad_id', '=', 'localidades.localidad_id')

        
        ->join('condiciones', 'expt.condicion_id', '=', 'condiciones.condicion_id')



        ->select (
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula',
            'objetos.objeto',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea', 'tipos_tareas.tipo_tarea_id',
            'localidades.localidad',
            'obras.*',
            'estados.*',
            'expt.expediente_id',
            'condiciones.condicion',
        )

        ->where('expt.expediente_id', '=', $id)

        ->get();

    

        return $data;   

    }










    public function getFileFromExpediente($expediente_afectado_id){

        $data = DB::table('expedientes as expt')

        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')

        ->join('planos', 'tareas.plano_id', '=', 'planos.plano_id')

        ->select ('planos.*')
        ->where('expt.expediente_id', '=', $expediente_afectado_id)
        ->get();

        return $data;

    }













    public function getDataForEmail($id){


        $data = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')

        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        ->join('condiciones', 'expt.condicion_id', '=', 'condiciones.condicion_id')

        // ->join('historial_condiciones','expt.expediente_id','=', 'historial_condiciones.expediente_id')


        ->select (
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.propietario_email',
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula',
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion',
            /* 'historial_condiciones.condicion_actual_id','historial_condiciones.fecha', 'historial_condiciones.expediente_id as historial_expediente_id', */
            'expt.expediente_id as expt_id',
        )

        ->where('expt.expediente_id', '=', $id)
        /* ->where('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_1'))
        ->orWhere('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_2'))
        ->orWhere('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_3')) */
        ->groupBy(    
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.propietario_email',
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula',
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion',
            /*'historial_condiciones.condicion_actual_id', 'historial_condiciones.fecha',  'historial_expediente_id', */
            'expt_id')
        // ->orderBy('','')
        ->get();
        
        
        /* dd($data); */
        

        return $data;
    }











    public static function getExpedienteData($expediente_id){

        $data = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        ->join('objetos', 'expt.objeto_id', '=', 'objetos.objeto_id')

        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')


        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')


        /* ->join('obras', 'localidades.localidad_id', '=', 'obras.localidad_id') */
        ->join('localidades', 'obras.localidad_id', '=', 'localidades.localidad_id')


        ->select (
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 
            'objetos.objeto',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea_id',
            'localidades.localidad_id',
            'obras.*',
            'expt.expediente_id',
        )

        ->where('expt.expediente_id', '=', $expediente_id)

        ->get()->first();

    

        return ([
        
            $data
        ]);   
    }










    




    public function getCondiciones($historialCondicionesExpediente){

        
        $condiciones = null;

        foreach ($historialCondicionesExpediente as $registro) {


            $query = (DB::table('condiciones')
            /*  ->join('condiciones', 'historial_condiciones.condicion_anterior_id', '=', 'condiciones.condicion_id') */

            ->select('condiciones.*')
            //->where('condiciones.condicion_id','=',$registro->condicion_anterior_id)
            ->Where('condiciones.condicion_id','=',$registro->condicion_actual_id)
            /* ->where('condiciones.condicion_id','=',$historialCondicionesExpediente->condicion_anterior_id)
            ->where('condiciones.condicion_id','=',$historialCondicionesExpediente->condicion_actual_id) */
            ->get());

            $condiciones= $query->merge($condiciones);

        }

    



        return $condiciones;
        
 
    }













    public function getCondiciones_Anteriores($expediente){

        $condiciones_anteriores = (DB::table('historial_condiciones')
        ->join('condiciones', 'historial_condiciones.condicion_anterior_id', '=', 'condiciones.condicion_id')
        ->join('expedientes','historial_condiciones.expediente_id','=','expedientes.expediente_id')
        ->select('condiciones.condicion', 'historial_condiciones.fecha', 'historial_condiciones.expediente_id as historial_expediente_id', 'expedientes.expediente_id as expt_id')
        ->where('historial_condiciones.expediente_id','=',$expediente->expediente_id)
        /* ->groupBy('condiciones.condicion', 'historial_condiciones.fecha') */
        ->get());

        return $condiciones_anteriores;
        

    }






    public function getCondiciones_Actuales($expediente){

        $condiciones_actuales = DB::table('historial_condiciones')
        ->join('condiciones', 'historial_condiciones.condicion_actual_id', '=', 'condiciones.condicion_id')
        ->join('expedientes','historial_condiciones.expediente_id','=','expedientes.expediente_id')

        ->select(
            'condiciones.condicion', 
            'historial_condiciones.fecha', 'historial_condiciones.expediente_id as historial_expediente_id', 
            'expedientes.expediente_id as expt_id'
        )
        ->where('historial_condiciones.expediente_id','=',$expediente->expediente_id)
        /* ->groupBy('condiciones.condicion', 'historial_condiciones.fecha') */
        ->get();

        return $condiciones_actuales;
        

    }












    public function getMotivosFormularioIncompleto($expediente){

        

        if ($expediente->condicion_id == config('app.formulario_incompleto') && $expediente->nota_reemplazo_profesional_id == null){

            $user = User::where('profesional_id','=', $expediente->profesional_id)->get()->first();


            $notifs = $user->notifications->where('type','=','App\Notifications\FormularioExpedienteIncompletoNotification')->pluck('data');



            $notif = $notifs->where('expediente_id','=', $expediente->expediente_id);
            /* dd($notif); */


            $motivo = $notif->pluck('motivo');
            $otroMotivo = $notif->pluck('otroMotivo');

            $motivo = $motivo[0];
            $otroMotivo = $otroMotivo[0];

            /* dd($motivo, $otroMotivo); */

            $motivos = (isset($motivo) ? $motivo  . " ; " . $otroMotivo : $otroMotivo);

            return $motivos;

        } else {

            return null;
        }
    }





}