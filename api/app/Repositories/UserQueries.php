<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\DB;





class UserQueries {


    public function getUsers(){

        $users = DB::table('users')->paginate(10);
        // dd($users);



        // INTENTO ACCEDER DE OTRA MANERA A LOS ROLES PARA REDUCIR LA CARGA DE LA PAGINA

        // foreach($users as $user){

        //     dd($user);
        //     $roles = DB::table('model_has_roles')
        //     ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
        //     ->select('roles.name')
        //     ->where('model_id', '=', $user->id)
        //     ->get();
        //     $user->roles =  $roles;

        //     dd($user->roles);

        //     testeando como accedo a los roles
            
        //     foreach($user->roles as $role){
        //         if($role->name === 'supervisor'){

        //         }
        //     }

            
        // }

        // dd($users);


        return $users;
    }




    public function getUsersByRole($request){


        switch($request->input('rol')){

            case (config('app.por_administrativo')):
                $users = User::role(config('app.por_administrativo'))->get();
                break;

            case (config('app.por_administrativo_hibrido')):
                $users = User::role(config('app.por_administrativo_hibrido'))->get();
                break;


            case (config('app.por_supervisor')):
                $users = User::role(config('app.por_supervisor'))->get();
                break;


            case (config('app.por_profesional')):
                $users = User::role(config('app.por_profesional'))->get();
                break;


            case (config('app.por_propietario')):
                $users = User::role(config('app.por_propietario'))->get();
                break;


            // case (config('app.por_profesional_y_propietario')):
            //     $users = User::role(['profesional','propietario'])->get();
            //     break;

            // case (config('app.por_administrativo_y_propietario')):
            //     $users = User::role(['administrativo','propietario'])->get();
            //     break;

            // case (config('app.por_administrativo_y_profesional')):
            //     $users = User::role(['administrativo','profesional'])->get();
            //     break;

        }
        
            
        // dd($users);
        return $users;

    }










    public function getUserss($request){


        $rol_value = $request->input('rol');

        $users = DB::table('users')

        ->when($rol_value, function ($query, $rol_value) {

            switch ($rol_value){

                case (1):
                    return $query->where('users.id', $user_id);
                    break;

            }

            return $query->where('users.id', $user_id);
        })

        ->get()->first();


        // dd($users);

        return $users;

    }

}