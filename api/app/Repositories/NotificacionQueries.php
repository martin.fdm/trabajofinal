<?php

namespace App\Repositories;



use Illuminate\Support\Facades\DB;








class NotificacionQueries  {





    public function getDataForNotificacionIndex(){


        $data = DB::table('notifications as notifs')
        ->select(
            'notifs.*',
        )
        
        ->where('notifs.notifiable_id', '=', Auth()->user()->id)

        ->orderBy('id','desc')
        ->paginate(20)
        /* ->get() */;

        return $data;

    }










    public function getDataForExpedienteProfesionalIndex (){


        $data= DB::table('expedientes as expt')

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.propietario_id')

        //recuperar partida inmobiliaria de una obra
        ->join('partidas_inmobiliarias as partidas', 'obras.partida_inmobiliaria_id', '=', 'partidas.partida_inmobiliaria_id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.profesional_id')

        //recuperar tipologia de expediente
        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        
        ->select (
            'expt.*',
            'prop.propietario_nombre', 'prop.propietario_apellido', 
            'prof.profesional_nombre', 'prof.profesional_apellido', 
            'partidas.partida_inmobiliaria_numero',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea'
        )


        ->where('expt.estado_id','=','1')
        ->where('expt.profesional_id', '=', Auth()->user()->profesional_id)
        /*->where('expt.aprobado', '=', '0') */
        ->orderBy('expediente_id','desc')
        ->paginate(20)
        /* ->get() */;




        return $data;

    }











    public function getNotificationById($notif_id){

        debug($notif_id);
        $notification_id = $notif_id;

        $notification = DB::table('notifications')
        ->where('notifications.created_at', 'LIKE' , '%'.$notification_id.'%')
        ->get()/* ->first() */;

        /* dd($notification_id,$notification); */

        return $notification;

    }













    // USO ESTO? NO ENCOTRÉ

    /* public static function getUserNotifications(){


        $data = DB::table('notifications as notifs')

        ->select('notifs.data',
        
        )
        
        ->where('notifs.notifiable_id', '=', Auth()->user()->id)


        ->orderBy('id','desc')
        ->paginate(20)


        ;

        $data = $data[0];
        return $data;
    } */



















    public function getDataToCreateExpediente(){

        $localidades=DB::table('localidades')
        ->orderBy('localidad_id','asc')->get();


        $objetos=DB::table('objetos')
        ->orderBy('objeto_id','asc')->get();


        $profesionales=DB::table('profesionales')
        ->where('alta','=','1')
        ->orderBy('profesional_id','desc')->get();


        $propietarios=DB::table('propietarios')
        ->where('alta','=','1')
        ->orderBy('propietario_id','desc')->get();


        $tipos_tareas=DB::table('tipos_tareas')
        ->orderBy('tipo_tarea_id','asc')->get();


        $tipologias=DB::table('tipologias')
        ->orderBy('tipologia_id','asc')->get();

        return ([
            $localidades,
            $objetos,
            $profesionales,
            $propietarios,
            $tipos_tareas,
            $tipologias
        ]);   
    

    }








    public function getDataToShowExpediente($id){

        /* dd($id); */
        // data es el conjunto de datos relacionados al expediente que se encuentra por id 
        $data = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.propietario_id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.profesional_id')


        ->join('objetos', 'expt.objeto_id', '=', 'objetos.objeto_id')

        ->join('tipologias', 'expt.tipologia_id', '=', 'tipologias.tipologia_id')


        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')


        /* ->join('obras', 'localidades.localidad_id', '=', 'obras.localidad_id') */
        ->join('localidades', 'obras.localidad_id', '=', 'localidades.localidad_id')


        ->join('planos', 'tareas.plano_id', '=', 'planos.plano_id')


        ->select (
        'prop.propietario_nombre', 'prop.propietario_apellido', 'prop.propietario_cuit', 
        'prof.profesional_nombre', 'prof.profesional_apellido', 'prof.profesional_numero_matricula',
        'objetos.objeto',
        'tipologias.tipologia', 
        'tipos_tareas.tipo_tarea',
        'localidades.localidad',
        'obras.*',
        'planos.plano' ,
        'expt.expediente_id'
        )

        ->where('expt.expediente_id', '=', $id)

        ->get();

    

        return ([
        
            $data
        ]);   

    }











}