<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;





class AuditoriaQueries {





    public function getAuditorias($request){

        $tabla = $request->input('tabla');
        $evento = $request->input('evento');
        $user_id = $request->input('user_id');
        $fecha1 = $request->input('fecha1');
        $fecha2 = $request->input('fecha2');



        $auditorias = DB::table('audits')

        ->join('users', 'audits.user_id' , '=', 'users.id')

        ->select('audits.*', 'users.name as usuario')

        ->when($tabla, function ($query, $tabla) {
            return $query->where('audits.auditable_type', "App\Models\\".$tabla);
        })

        ->when($evento, function ($query, $evento) {
            return $query->where('audits.event', $evento);
        })

        ->when($user_id, function ($query, $user_id) {
            return $query->where('audits.user_id', $user_id);
        })
        
        ->when($fecha1, function ($query, $fecha1) {
            return $query->where('audits.created_at', '>=' , $fecha1);
        })

        ->when($fecha2, function ($query, $fecha2) {
            return $query->where('audits.created_at', '<=' , $fecha2);
        })

        ->orderBy('audits.id', 'desc')

        /* ->get() */
        ->paginate(15);

        /* dd($auditorias); */

        return $auditorias;


    }





















    // // ESTO FUNCIONA PERO NO PUEDO PAGINAR

    // public function getAuditorias($request){

    
    //     $evento = $request->input('evento');

    //     /* dd($request); */

    //     $auditorias = Auditoria::query();

    //     if ($request->filled('tabla')) {

    //         $auditorias->where('auditable_type' , "App\Models\\".$request->tabla);
    //     }


    //     if ($request->filled('user_id')) {

    //         $auditorias->where('user_id' , $request->user_id);
    //     }


    //     if ($request->filled('evento')) {

    //         $auditorias->where('event', '=' , $evento);
    //     }


    //     if ($request->filled('fecha1')) {

    //         $auditorias->where('created_at', '>=' , $request->fecha1);
    //     }


    //     if ($request->filled('fecha2')) {

    //         $auditorias->where('created_at', '<=' , $request->fecha2);
    //     }


    //     return $auditorias;


    // }







}