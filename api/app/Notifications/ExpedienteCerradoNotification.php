<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\Models\Expediente;

use Carbon\Carbon;




class ExpedienteCerradoNotification extends Notification implements ShouldQueue {


    public $expediente;
    public $emisor;

    use Queueable;

   


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Expediente $expediente, $emisor)
    {
        $this->expediente = $expediente;
        $this->emisor = $emisor;
    }




    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function via($notifiable){
        return ['database'];
    }





    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }








    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */


    public function toArray($notifiable){
        

        $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s');

        return [            
            'emisor' => $this->emisor,
            'expediente_id' => $this->expediente->expediente_id,
            'asunto' => "Su Expediente Nº " . $this->expediente->expediente_numero . " ha sido cerrado exitosamente",
            'fecha' => $fecha,
        ];

    }





}
