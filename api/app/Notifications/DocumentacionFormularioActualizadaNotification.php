<?php

namespace App\Notifications;

use App\Models\ComprobantePago;
use App\Models\Expediente;
use App\Models\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;


use Carbon\Carbon;


class DocumentacionFormularioActualizadaNotification  extends Notification implements ShouldQueue {

    use Queueable;

    public $expediente;
    public $emisor;





    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Expediente $expediente, $emisor) {

        $this->expediente = $expediente;
        $this->emisor = $emisor;


    }




    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function via($notifiable){
        return ['database'];
    }





    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }




    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function toArray($notifiable){

        $fecha = Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s');

        return [

            'emisor' => $this->emisor,
            'expediente_id' => $this->expediente->expediente_id,
            'expediente_numero' => $this->expediente->expediente_numero,
            'asunto' => "La documentación del Formulario Nº " . $this->expediente->expediente_numero . " ha sido actualizada",
            'fecha' => $fecha,
            
        ];

    }



}
