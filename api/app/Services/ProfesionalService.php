<?php


namespace App\Services;

use Illuminate\Support\Facades\DB;

use App\Models\Profesional;
use App\Http\Requests\ProfesionalFormRequest;









class ProfesionalService {





    public function createProfesional(ProfesionalFormRequest $request){

        
        try {


            DB::beginTransaction();
        
        
            $profesional = Profesional::firstOrCreate ([
                
                'propietario_nombres' => strtoupper($request->input('nombres')),
                'propietario_apellidos' => strtoupper($request->input('apellidos')),
                'propietario_numero_matricula' => strtoupper($request->input('numero_matricula')),
                'propietario_cuit' => $request->input('cuit'),
                'alta'=> 0,

            ]);


            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $profesional;

        
    }












    public function updateProfesional(ProfesionalFormRequest $request, $id){


        try {
        
            DB::beginTransaction();
            

            $profesional = Profesional::where('id','=',$id)->update([

                'profesional_nombres' => strtoupper($request->input('nombres')),
                'propietario_apellidos' => strtoupper($request->input('apellidos')),
                'propietario_numero_matricula' => $request->input('numero_matricula'),
                'propietario_cuit' => $request->input('cuit'),

            ]);


            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $profesional;

    }







    public function deleteProfesional($id){


        try {
        
            DB::beginTransaction();

            $profesional = Profesional::where('id','=',$id)->update([

                'alta'=>0

            ]);

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $profesional;

    }

    

    public function cambiarValorAlta($id){

        dd('hellow');

    }



}