<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;

use App\Models\Propietario;
use App\Models\Profesional;

use App\Http\Requests\PropietarioFormRequest;
use App\Models\User;

// use Spatie\Permission\Models\Role;



class PropietarioService {





    public function createPropietario(PropietarioFormRequest $request){

        /* dd(strtoupper($requestPropietario->input('nombres'))); */

        
        try {
            

            $profesional_id = Auth()->user()->profesional_id;
            $profesional = Profesional::findOrFail($profesional_id);

            DB::beginTransaction();
        

        
            $propietario = Propietario::firstOrCreate ([
                
                'propietario_nombres'   => trim(strtoupper($request->input('nombres'))),
                'propietario_apellidos' => trim(strtoupper($request->input('apellidos'))),
                'propietario_cuit'      => trim($request->input('cuit')),
                
            ], [
                'propietario_email'     => trim(strtoupper($request->input('email'))),
                'alta'                  => 1,
            ]);


            if ($propietario->wasRecentlyCreated){

                $profesional->propietarios()->attach([$propietario->id]);

            } else {   // YA EXISTE EL MODELO
                
                /* dd($propietario); */
                
                if(!$profesional->propietarios->contains($propietario)){ // ALGUIEN MAS REGISTRO EL PROPIETARIO, ENTONCES LO AGREGAMOS 

                    $profesional->propietarios()->attach([$propietario->id]);

                } else {

                    if ($propietario->alta == 0){
                        $propietario->update(['alta' => 1]);
                        $profesional->propietarios()->attach([$propietario->id]);

                    } else {

                        return $propietario = null;

                    }
                }

            }

            

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $propietario;
        
    }












    public function updatePropietario(PropietarioFormRequest $request, $id){

    /*     $propietario = Propietario::where('propietario_id','=',$id);
        dd($propietario); */

        DB::beginTransaction();

        try {


            $propietario = Propietario::where('id','=',$id)->update([
                'propietario_nombres'   => trim(strtoupper($request->input('nombres'))),
                'propietario_apellidos' => trim(strtoupper($request->input('apellidos'))),
                'propietario_cuit'      => trim($request->input('cuit')),
                'propietario_email'     => trim(strtoupper($request->input('email'))),

            ]);



            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $propietario;

    }







    public function deletePropietario ($id){

        try {

            DB::beginTransaction();


            $currentUserRole = Auth()->user()->getRoleNames();
            $currentUserRole = $currentUserRole[0];
    
            switch($currentUserRole){
            
                case "administrativo":
                case "supervisor":
            
                    $propietario = Propietario::where('id','=',$id)->update([
                    
                        'alta'=>0
                    
                    ]);

                    break;


                case "profesional":

                    $profesional = Profesional::findOrFail(Auth()->user()->profesional_id);

                    $profesional->propietarios()->detach([$id]);

                    break;
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return 0;
    }

    










    public function registrarseComoPropietario(){

        /* dd(strtoupper($requestPropietario->input('nombres'))); */

        
        try {
            

            $user           = User::findOrFail(Auth()->user()->id);
            $profesional    = Profesional::findOrFail($user->profesional_id);

            DB::beginTransaction();
        

        
            $propietario = Propietario::firstOrCreate ([
                
                'propietario_nombres'   => $user->name,
                'propietario_apellidos' => $user->last_name,
                'propietario_cuit'      => $user->cuit,
                
            ], [
                'propietario_email'     => $user->email,
                'alta'                  => 1,
            ]);



            $user->assignRole('propietario');

            $user->update([
                'propietario_id' => $propietario->id,
            ]);


            if(!$user->hasRole('profesional')){
                return $propietario;
            }


            if ($propietario->wasRecentlyCreated){

                $profesional->propietarios()->attach([$propietario->id]);

            } else {   // YA EXISTE EL MODELO
                
                /* dd($propietario); */
                
                if(!$profesional->propietarios->contains($propietario)){ // ALGUIEN MAS REGISTRO EL PROPIETARIO, ENTONCES LO AGREGAMOS 

                    $profesional->propietarios()->attach([$propietario->id]);

                } else {

                    if ($propietario->alta == 0){
                        $propietario->update(['alta' => 1]);
                        $profesional->propietarios()->attach([$propietario->id]);

                    } else {

                        return $propietario = null;

                    }
                }

            }

           
        


            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;

        }

        return $propietario;
        
    }











}