<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;


use Illuminate\Support\Facades\Notification;
use App\Events\NuevoFormularioExpedienteEvent;
use App\Models\User;
use App\Notifications\NuevoFormularioExpedienteNotification;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;






class NuevoFormularioExpedienteListener /* implements ShouldQueue */ {



   /*  use Dispatchable, InteractsWithSockets, SerializesModels; */
    //use Queueable;

    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct() {
        
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(NuevoFormularioExpedienteEvent $event){
        

        $users = User::role('supervisor')->get();

        $emisor = Auth()->user()->name;

        Notification::send($users, new NuevoFormularioExpedienteNotification($event->expediente, $emisor));
        
    }


}


