<?php

namespace App\Listeners;


use Illuminate\Broadcasting\InteractsWithSockets;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;



use Illuminate\Support\Facades\Notification;
use App\Notifications\ExpedienteLiquidadoNotification;
use App\Events\ExpedienteLiquidadoEvent;
use App\Events\PrimerRecordatorio_Profesional_subirComprobantePagoEvent;
use App\Jobs\enviarInfoLiquidacionMail;
use App\Jobs\enviarPrimerRecordatorio_Profesional_subirComprobantePagoMail;
use App\Jobs\enviarSegundoRecordatorio_Profesional_subirComprobantePagoMail;
use App\Models\Configuracion;
use App\Services\ExpedienteService;

use App\Models\User;
use App\Models\Expediente;
use App\Models\Obra;
use App\Models\Propietario;
use App\Repositories\ExpedienteQueries;
use Illuminate\Support\Facades\DB;







class ExpedienteLiquidadoListener {

    public $expedienteService;
    public $expedienteQueries;
    

    use Dispatchable, InteractsWithSockets, SerializesModels;


    

    /** 
     * Create the event listener.
     *
     * @return void
     */

    public function __construct(ExpedienteService $expedienteService, ExpedienteQueries $expedienteQueries) {
        $this->expedienteService = $expedienteService;
        $this->expedienteQueries = $expedienteQueries;
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ExpedienteLiquidadoEvent $event){


        $expediente = Expediente::findOrFail($event->expediente->expediente_id);

        try {
            
            DB::beginTransaction();



            // NOTIFICACION

            $user = User::where('profesional_id', "=", $expediente->profesional_id)->get()->first();

            $emisor = Auth()->user()->name;

            Notification::sendNow($user, new ExpedienteLiquidadoNotification($event->expediente, $emisor)); 
        


            /** 
             * estoy probando si puedo continuar usando getDataForEmail() sin editarlo.
             * EL problema es que en ese metodo comparo con esperando_comprobante1, 2 y 3.
             * Entonces tengo que  
             * QUIERO CAMBIAR LA CONDICION DEL EXP DEPENDIENDO SI TIENE  */



            $obra= Obra::find($expediente->obra_id);

            $propietario_email = Propietario::find($obra->propietario_id)->propietario_email;

            
            if ($propietario_email !== ""){

                $condicion_id = config('app.esperando_comprobante_3');

            } else {

                $condicion_id = config('app.esperando_comprobante_1'); 

            }


            
            $expediente->update([
                'condicion_id' => $condicion_id,
            ]);


            $this->expedienteService->actualizarHistorialCondicionExpediente($expediente);




            $dataForEmail = $this->expedienteQueries->getDataForEmail($expediente->expediente_id);       
            
            /* dd($dataForEmail); */

            // $propietario_email = $dataForEmail[0]->propietario_email;




            $expediente->update([
                'liquidacion' => $event->decision['monto'],
                'prioridad_administracion' => 4,
                'prioridad_profesional'    => 1,
            ]);


            
            if ($expediente->estado_id == config('app.para_revisar')){
                $expediente->update([
                    'estado_id' => config('app.estado_abierto'),
                    'aprobado'  => true,
                ]);
            }




            // MAIL

            $liquidacionMail = enviarInfoLiquidacionMail::dispatch($expediente, $dataForEmail);





            /**
             *  LANZAMOS EL 1er JOB DEL PROCESO AUTOMATIZADO 1: Interacción con el profesional para cerrar el expediente
             **/

            $cantidad_dias = Configuracion::get()->first()->tiempo_primer_recordatorio_1er_proceso_automatizado;

            $primerRecordatorioRetorno = enviarPrimerRecordatorio_Profesional_subirComprobantePagoMail::dispatch($expediente, $dataForEmail)
            /* ->delay(now()->addDays($cantidad_dias)) */
            /* ->delay(now()->addMinute()) */
            ->delay(now()->addSeconds($cantidad_dias)); 



            

            DB::commit();
            return 0 ;


        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    
        
    }


}
