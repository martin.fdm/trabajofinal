<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Events\ValoracionNotaReemplazoProfesionalEvent;
use App\Models\User;
use App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalEntrante_Notification;
use App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalSaliente_Notification;
use App\Notifications\NotaReemplazoProfesionalDesaprobada_ProfesionalEntrante_Notification;







class ValoracionNotaReemplazoProfesionalListener {


    use Dispatchable, InteractsWithSockets, SerializesModels;


    
 /** 
     * Create the event listener.
     *
     * @return void
     */

    public function __construct() {
        //
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ValoracionNotaReemplazoProfesionalEvent $event){
        

        $profesional_entrante = User::where('profesional_id', $event->nota->profesional_entrante_id)->get()->first();
        $profesional_saliente = User::where('profesional_id', $event->nota->profesional_saliente_id)->get()->first();

        if ($event->nota->condicion_nrp_id == config('app.nrp_aprobada')){

            $asunto = "Su nota para reemplazar al Profesional " . $profesional_saliente->name . " en tarea Dirección de Obra 
            ha sido aprobada";

            Notification::send($profesional_entrante, new NotaReemplazoProfesionalAprobada_ProfesionalEntrante_Notification(
                $event->expediente, $event->emisor, $asunto
            ));
            

            $asunto = "Ha sido aprobada una Nota de Reemplazo Profesional presentada, que lo aparta de la tarea Dirección de Obra 
            registrada en su expediente Nº ". $event->nota->expediente_afectado_numero;

            Notification::send($profesional_saliente, new NotaReemplazoProfesionalAprobada_ProfesionalSaliente_Notification(
                $event->nota ,$event->emisor, $asunto
            ));

        } else {

            $asunto = "Su nota para Reemplazar al Profesional " . $profesional_saliente->name . " en tarea Dirección de Obra 
            ha sido desaprobada. Observaciones: ".$event->nota->observaciones;

            Notification::send($profesional_entrante, new NotaReemplazoProfesionalDesaprobada_ProfesionalEntrante_Notification(
                $event->nota, $event->emisor, $asunto
            ));

        }

            
    }


}
