<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;


use App\Models\User;
use App\Events\SolicitudReemplazoProfesionalEvent;
use App\Notifications\SolicitudReemplazoProfesionalNotification;

class SolicitudReemplazoProfesionalListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    /** 
     * Create the event listener.
     *
     * @return void
     */

    public function __construct() {
        //
    }




    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(SolicitudReemplazoProfesionalEvent $event){
        

        // NOTIFICACION

        $users = User::role('supervisor')->get();

        Notification::sendNow($users, new SolicitudReemplazoProfesionalNotification($event->nota, $event->emisor));
        
    }


}
