<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Events\ComprobantePagoDesestimadoEvent;
use App\Notifications\ComprobantePagoDesestimadoNotification;
use App\Services\ExpedienteService;

use App\Models\User;
use App\Models\Expediente;
use Illuminate\Support\Facades\DB;

class ComprobantePagoDesestimadoListener {

    public $expediente;
    protected $expedienteService;

    use Dispatchable, InteractsWithSockets, SerializesModels;


    


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ExpedienteService $expedienteService){
        $this->expedienteService = $expedienteService;
    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ComprobantePagoDesestimadoEvent $event){
        

        $expediente = Expediente::findOrFail($event->expediente->expediente_id);

        try {
            
            DB::beginTransaction();


            // RECUPERAR DIRECTAMENTE DESDE LA RELACION CON EL EXPEDIENTE, NO DESDE ESTE TIPO DE QUERY
            // $expediente->historial_condiciones me devolvia sólo un registro, y era el primero, no el ultimo, el cual necesitaba
            // $historial_condiciones = DB::table('historial_condiciones')
            // ->where('expediente_id', '=', $expediente->expediente_id)
            // ->orderBy('id', 'desc')
            // ->get()->first();


            // $ultima_condicion_id = $historial_condiciones->condicion_actual_id;


            $expediente->update([
                'condicion_id'              => config('app.esperando_comprobante_1'),
                'comprobante_pago_id'       => null,
                'prioridad_administracion'  => 4,
                'prioridad_profesional'     => 1,
            ]);


            $this->expedienteService->actualizarHistorialCondicionExpediente($expediente);


            // NOTIFICACION

            $user = User::where('profesional_id', "=", $expediente->profesional_id)->get();

            $emisor = Auth()->user()->name;

            Notification::send($user, new ComprobantePagoDesestimadoNotification($event->expediente, $event->motivo, $emisor));



            DB::commit();

            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
            
    }


}
