<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Notifications\CertificadoGeneradoNotification;
use App\Events\CertificadoGeneradoEvent;
use App\Models\User;







class CertificadoGeneradoListener {



    use Dispatchable, InteractsWithSockets, SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(){
        //        
    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(CertificadoGeneradoEvent $event){
        

        $users = User::where('profesional_id', "=", $event->expediente->profesional_id)->get();

        Notification::send($users, new CertificadoGeneradoNotification($event->expediente, $event->dataExtraExpediente, $event->emisor));
            
    }
}
