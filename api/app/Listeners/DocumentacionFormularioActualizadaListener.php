<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Events\DocumentacionFormularioActualizadaEvent;

use App\Models\User;
use App\Notifications\DocumentacionFormularioActualizadaNotification;





class DocumentacionFormularioActualizadaListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;


    


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(){

    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(DocumentacionFormularioActualizadaEvent $event){


        $users = User::Role('supervisor')->get();

        Notification::send($users, new DocumentacionFormularioActualizadaNotification($event->expediente, $event->emisor));
            
    }
}
