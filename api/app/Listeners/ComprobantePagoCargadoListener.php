<?php

namespace App\Listeners;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


use Illuminate\Support\Facades\Notification;
use App\Events\ComprobantePagoCargadoEvent;
use App\Jobs\borrarJobsRecordatorios as JobsBorrarJobsRecordatorios;
use App\Notifications\ComprobantePagoCargadoNotification;

use App\Models\User;









class ComprobantePagoCargadoListener {

    use Dispatchable, InteractsWithSockets, SerializesModels;



    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(){

    }




    
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ComprobantePagoCargadoEvent $event){
    

        $users = User::Role('supervisor')->get();

        $emisor = Auth()->user()->name;
        
        Notification::send($users, new ComprobantePagoCargadoNotification($event->expediente, $event->comprobantePagoID, $emisor));
            



        // JOB PARA ELIMINAR LOS JOBS DE RECORDATORIO AL PROFESIONAL QUE PUDIERAN EXISTIR

        JobsborrarJobsRecordatorios::dispatch($event->expediente)/* ->delay(now()->addMinute()) */;




    }



}
