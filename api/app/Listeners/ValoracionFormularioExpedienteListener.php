<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Notification;

use App\Models\User;
use App\Models\Expediente;

use App\Services\ExpedienteService;

use App\Events\ValoracionFormularioExpedienteEvent;
use App\Notifications\FormularioExpedienteAprobadoNotification;
use App\Notifications\FormularioExpedienteIncompletoNotification;
use App\Notifications\FormularioExpedienteDesaprobadoNotification;




class ValoracionFormularioExpedienteListener {


    protected $expedienteService; 





    /**
     * Create the event listener.
     *
     * @return void
     */


    public function __construct(ExpedienteService $expedienteService) {
        $this->expedienteService = $expedienteService;
    }







    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */

    public function handle(ValoracionFormularioExpedienteEvent $event){
        
    
        /**
         * 
         *  Input: modal Valorar Formulario Expediente
         *  
         *  A partir de la decisión del usuario 
         *  se cambia la condicion del expediente y 
         *  se envían las notificaciones correspondientes. 
         * 
         */


        $expediente = Expediente::findOrFail($event->expediente->expediente_id);

        /* dd($event->decision['aprobadoCheckbox']); */

        
        try {
            
            DB::beginTransaction();

            // DATOS PARA LAS NOTIFICACIONES

            $user = User::where('profesional_id', "=", $expediente->profesional_id)->get()->first();

            

            if ($event->decision['aprobadoCheckbox'] === 'on'){      //FUE APROBADO


                $expediente->aprobado                   = true;
                $expediente->estado_id                  = config('app.estado_abierto');
                $expediente->condicion_id               = config('app.para_liquidar');
                $expediente->prioridad_administracion   = 1;
                $expediente->prioridad_profesional      = 4;

                // NOTIFICACION 

                Notification::sendNow($user, new FormularioExpedienteAprobadoNotification($event->expediente, $event->emisor));


            } elseif ($event->decision['PlanosODocumentosCheckbox'] === 'on') {      // ESTA INCOMPLETO


                $expediente->condicion_id               = config('app.formulario_incompleto');
                $expediente->prioridad_administracion   = 3;
                $expediente->prioridad_profesional      = 2;
            
                // NOTIFICACION 

                Notification::sendNow($user, new FormularioExpedienteIncompletoNotification(
                    $event->expediente, 
                    $event->decision['motivo'],
                    $event->decision['otroMotivo'],
                    $event->emisor
                ));


            } else { // FUE DESAPROBADO


                $expediente->aprobado                   = false;
                $expediente->estado_id                  = config('app.estado_cerrado');
                $expediente->condicion_id               = config('app.formulario_desaprobado');
                $expediente->prioridad_administracion   = 4;
                $expediente->prioridad_profesional      = 4;

                // NOTIFICACION                 

                Notification::sendNow($user, new FormularioExpedienteDesaprobadoNotification(
                    $event->expediente, 
                    $event->decision['motivo'],
                    $event->decision['otroMotivo'],
                    $event->emisor
                ));

            }



            $expediente->save();   

            $this->expedienteService->actualizarHistorialCondicionExpediente($expediente);

        
        
            DB::commit();
            return 0;

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


    }


}


