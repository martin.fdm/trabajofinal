<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PropietarioFormRequest;
use App\Models\Propietario;
use DB; 





class UserController extends Controller {

    // implementado para editar el perfil del usuario


    public function __construct() {

    }



    





    public function store(PropietarioFormRequest $request){
        
        $propietario= new Propietario; 
        $propietario->propietario_nombre=$request->get('nombre');
        $propietario->propietario_apellido=$request->get('apellido');
        $propietario->propietario_cuit=$request->get('cuit');
        $propietario->alta=true;


        $propietario->save();
        return Redirect::to('propietario');
    }







    // public function show($id){
    //     return view("propietario.show",["propietario"=>Propietario::findOrFail($id)]);
    // }


    // public function edit($id){
    //     return view("propietario.edit",["propietario"=>Propietario::findOrFail($id)]);
    // }






    public function update(PropietarioFormRequest $request, $id){

        dd('YEAH');
        /* $propietario=Propietario::findOrFail($id);
        $propietario->propietario_nombre=$request->get('nombre');
        $propietario->propietario_apellido=$request->get('apellido');
        $propietario->propietario_cuit=$request->get('cuit');
        $propietario->update();

        return Redirect::to('propietario'); */
    }






    
    public function destroy($id){
        $propietario=Propietario::findOrFail($id);
        $propietario->alta=false;
        $propietario->update();

        return Redirect::to('propietario');
    }
}
