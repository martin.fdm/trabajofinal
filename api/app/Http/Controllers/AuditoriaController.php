<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\AuditoriaQueries;
use App\Services\AuditoriaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;






class AuditoriaController extends Controller {


    public $auditoriaQueries;
    public $auditoriaService;



    public function __construct(AuditoriaQueries $auditoriaQueries, AuditoriaService $auditoriaService) {

        $this->middleware('auth');
        $this->auditoriaQueries = $auditoriaQueries;
        $this->auditoriaService = $auditoriaService;

    }





    public function index(Request $request) {


        if (Auth()->user()->can('Ver Auditoria en sidebar')) {

            $auditorias =  $this->auditoriaQueries->getAuditorias($request);

            /* dd($auditorias); */

            $auditorias = AuditoriaService::staticFormatearColumnas($auditorias);

            $users = User::all();


            return view('auditoria.index',

                compact('auditorias', 'users'),

            );

        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }

    }








    public function search(Request $request) {


        if (Auth()->user()->can('Ver Auditoria en sidebar')) {

            $auditorias =  $this->auditoriaQueries->getAuditorias($request);

            /* dd($auditorias); */

            $auditorias = AuditoriaService::staticFormatearColumnas($auditorias);

            $users = User::all();


            return view('auditoria.index',

                compact('auditorias', 'users'),

            );


        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }

    }





    public function show (){}


























































    // CODIGO ACTUAL QUE SE QUIERE REEMPLAZAR: FUNCIONA PERO NO PUEDO PAGINAR EN FRONT

    // public function index(Request $request) {


    //     if (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor")) {

    //         $auditorias =  $this->auditoriaQueries->getAuditorias($request);

    //         /* dd($auditorias); */

    //         // $auditorias = AuditoriaService::staticFormatearColumnas($auditorias);

    //         $users = User::all();


    //         return view('auditoria.index',

    //                 ['auditorias' => $auditorias->get()->sortByDesc('id')],
    //                 compact('users'),

    //         );

    //     } else {

    //         return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

    //     }

    // }



    // public function search(Request $request) {


    //     if (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor")) {

    //         $auditorias =  $this->auditoriaQueries->getAuditorias($request);

    //         /* dd($auditorias); */

    //         // $auditorias = AuditoriaService::staticFormatearColumnas($auditorias);

    //         $users = User::all();


    //         return view('auditoria.index',

    //                 ['auditorias' => $auditorias->get()->sortByDesc('id')],
    //                 compact('users'),

    //         );


    //     } else {

    //         return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

    //     }

    // }








}








