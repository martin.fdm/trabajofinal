<?php


namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;



use App\Models\Propietario;
use App\Http\Requests\PropietarioFormRequest;
use App\Repositories\InformeQueries;
use App\Services\InformeService;











class InformeController extends Controller {



    public $informeService;
    public $informeQueries;



    public function __construct(InformeService $informeService, InformeQueries $informeQueries) {
        $this->middleware('auth');
        $this->informeService = $informeService;
        $this->informeQueries = $informeQueries;

    }



    public function index(Request $request) {


        if (Auth()->user()->can('Ver Informes en sidebar')) {

            $dataForFiltros = $this->informeQueries->getDataForIndexFiltros();

            return view('informe.index',[
                'condiciones'   => $dataForFiltros[1],
                'tipologias'    => $dataForFiltros[3],
            ]);
        
        } else {
            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      
        }


    }














    public function generarInformeExpedientesTramitesInconclusos(Request $request) {

        $informe = $this->informeService->generarInformeExpedientesTramitesInconclusos($request);

        return $informe->stream("Informe-ExpedientesConTramitesInconclusos-".\Carbon\Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s'));
        
    }






    public function generarInformeProfesionalesMasExpedientes(Request $request) {

        $informe = $this->informeService->generarInformeProfesionalesMasExpedientes($request);

        return $informe->stream("Informe-ProfesionalesConMasExpedientes-".\Carbon\Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s'));
        
    }






    // public function create(){
    //     return view('propietario.create');
    // }



    // public function store(PropietarioFormRequest $request){
    //     $propietario= new Propietario; 
    //     $propietario->propietario_nombre=$request->get('nombre');
    //     $propietario->propietario_apellido=$request->get('apellido');
    //     $propietario->propietario_cuit=$request->get('cuit');
    //     $propietario->alta=true;


    //     $propietario->save();
    //     return Redirect::to('propietario');
    // }



    public function show($id){
        return view("propietario.show",["propietario"=>Propietario::findOrFail($id)]);
    }



    // public function edit($id){
    //     return view("propietario.edit",["propietario"=>Propietario::findOrFail($id)]);
    // }



    // public function update(PropietarioFormRequest $request, $id){
    //     $propietario=Propietario::findOrFail($id);
    //     $propietario->propietario_nombre=$request->get('nombre');
    //     $propietario->propietario_apellido=$request->get('apellido');
    //     $propietario->propietario_cuit=$request->get('cuit');
    //     $propietario->update();

    //     return Redirect::to('propietario');
    // }


    
    // public function destroy($id){
    //     $propietario=Propietario::findOrFail($id);
    //     $propietario->alta=false;
    //     $propietario->update();

    //     return Redirect::to('propietario');
    // } 


}
