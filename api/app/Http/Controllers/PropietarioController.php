<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckUserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


use App\Models\Propietario;
use App\Http\Requests\PropietarioFormRequest;


use App\Services\PropietarioService;

use App\Repositories\PropietarioQueries;



use DB; 



class PropietarioController extends Controller {


    protected $propietarioService;
    protected $propietarioQueries;




    public function __construct(PropietarioService $propietarioService, PropietarioQueries $propietarioQueries) {
        $this->middleware('auth');
        // $this->middleware(CheckUserRoles::class);
        $this->propietarioService = $propietarioService;
        $this->propietarioQueries = $propietarioQueries;
    }





    public function index(Request $request) {

        /* $profesional = Profesional::findOrfail(Auth()->user()->profesional_id);

        dd($profesional->propietarios); */
        $user = Auth()->user();
        
        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
            // $currentUserRole = $currentUserRole[0];
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles   = Auth()->user()->getRoleNames();

        $propietarios       = $this->propietarioQueries->getDataForPropietarioIndex($request, $currentUserRoles);

        $listaPropietarios  = $this->propietarioQueries->getDataForFiltros($currentUserRoles);

        return view('propietario.index', compact(
                'propietarios',
                'listaPropietarios',
                'currentUserRoles'
            )
        );
            
        
    }












    public function search(Request $request) {

        /* $profesional = Profesional::findOrfail(Auth()->user()->profesional_id);

        dd($profesional->propietarios); */

        // $currentUserRole = $currentUserRole[0];

        $user = Auth()->user();

        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles = Auth()->user()->getRoleNames();

        $propietarios= $this->propietarioQueries->getDataForPropietarioIndex($request, $currentUserRoles);

        $listaPropietarios = $this->propietarioQueries->getDataForFiltros($currentUserRoles);

        return view('propietario.index', compact(
                'propietarios',
                'listaPropietarios',
                'currentUserRoles'
            )
        );

    }
            

            
        
    
















    public function create(){

        return view('propietario.create');
    }






    


    public function store(PropietarioFormRequest $requestPropietario){

        $propietario = $this->propietarioService->createPropietario($requestPropietario);
        /* var_dump($propietario); */

        /* dd($requestPropietario); */
        /* dd($propietario); */

        if ($propietario == null) {

            return redirect()->back()->with('error','Ya ha agregado anteriormente al Propietario que intenta registrar.');
        } 

        return Redirect::to('propietarios')->with('success','Propietario agregado con éxito.');
    }










    public function show($id){

        return view("propietarios.show",["propietario"=>Propietario::findOrFail($id)]);
    }










    public function edit($id){

        $propietario= Propietario::findOrFail($id);

        return view("propietario.modalEdit", $propietario);
    }









    public function update(PropietarioFormRequest $request, $id){

        $propietario= $this->propietarioService->updatePropietario($request, $id);

        return Redirect::to('propietarios')->with('success','Propietario ha sido modificado con éxito.');
    }







    
    public function destroy($id){
        
        $propietario= $this->propietarioService->deletePropietario($id);

        return Redirect::to('propietarios')->with('success','Propietario ha sido eliminado con éxito.');
    }







    public function registrarseComoPropietario(){

        $propietario = $this->propietarioService->registrarseComoPropietario();
        
        return Redirect::to('propietarios')->with('success','Se ha registrado como propietario con éxito.');

    }



}
