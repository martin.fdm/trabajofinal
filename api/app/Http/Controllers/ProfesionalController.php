<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfesionalFormRequest;
use App\Repositories\ProfesionalQueries;
use App\Services\ProfesionalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;









class ProfesionalController extends Controller {


    protected $profesionalService;
    protected $profesionalQueries;




    public function __construct(ProfesionalService $propietarioService, ProfesionalQueries $profesionalQueries) {

        $this->middleware('auth');
        $this->propietarioService = $propietarioService;
        $this->profesionalQueries = $profesionalQueries;
    }





    public function index(Request $request) {

        if (Auth()->user()->can('Ver Profesionales en sidebar')) {

            $profesionales = $this->profesionalQueries->getDataForIndex($request);


            return view('profesional.index',
                compact('profesionales')
            
            );

        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }

    }









    public function search(Request $request) {


        if (Auth()->user()->can('Ver Profesionales en sidebar')) {

            $profesionales = $this->profesionalQueries->getDataForIndex($request);


            return view('profesional.index',
                compact('profesionales')
            
            );

        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }


    }




/* 
    public function create(){

        return view('propietario.create');
    }
 */





    

/* 
    public function store(PropietarioFormRequest $requestPropietario){


        $propietario= $this->propietarioService->createPropietario($requestPropietario);
        var_dump($propietario);

        dd($requestPropietario);

        return Redirect::to('propietarios')->with('success','Propietario agregado con éxito.');
    } */












    public function update(ProfesionalFormRequest $request, $id){


        $profesional = $this->profesionalService->updateProfesional($request, $id);

        return Redirect::to('profesionales')->with('success','Profesional ha sido modificado con éxito.');

    }







    
    public function destroy($id){
        
        $profesional = $this->profesionalService->deleteProfesional($id);

        return Redirect::to('propietarios')->with('success','Propietario ha sido eliminado con éxito.');

    }




    public function cambiarValorAlta($id){

        $profesional = $this->profesionalService->cambiarValorAlta($id);

        return Redirect::to('profesionales')->with('success','Profesional ha sido eliminado con éxito.');

    }



}
