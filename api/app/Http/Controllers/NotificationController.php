<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;


use App\Http\Controllers\NotaReemplazoProfesionalController;

use App\Repositories\NotificacionQueries;

use App\Http\Controllers\ExpedienteController;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as FacadesRequest;

class NotificationController extends Controller {


    protected $notifcacionQueries;



    public function __construct(NotificacionQueries $notificacionQueries) {
        $this->middleware('auth');
        $this->notificacionQueries = $notificacionQueries;

    }



    public function index() {
        
        /* $notificaciones = $this->notificacionQueries->getDataForNotificacionIndex($query) ; */

        /* $notificaciones = $notificaciones[]; */

        $user = auth()->user();

        $notificaciones = $user->notifications()->paginate(25);
        // dd($notificaciones);
        // $notificacionesData = $user->notifications->pluck('data');

        /* dd($notificaciones); */

        return view('notificaciones.index',[
            "notificaciones"  => $notificaciones,
            // "notificacionesData"  => $notificacionesData,
        ]);
        
    }






    public function search() {
        
        /* $notificaciones = $this->notificacionQueries->getDataForNotificacionIndex($query) ; */

        /* $notificaciones = $notificaciones[]; */

        $user = auth()->user();

        $notificaciones = $user->notifications;
        $notificacionesData = $user->notifications->pluck('data');

        /* dd($notificaciones); */

        return view('notificaciones.index',[
            "notificaciones"  => $notificaciones,
            "notificacionesData"  => $notificacionesData,
        ]);
        
    }













    /** 
     *  Cada notificación del ícono de notifaciones (campana)
     *  es atendida por el siguiente metodo 'manejador'.
     */

    public function manejador($notification_id, $expediente_id = null, /* NotaReemplazoProfesionalController $nrpController */) {
        
        
        try {

            /* dd($request); */

            $notification = auth()->user()
            ->unreadNotifications
            ->where('id', $notification_id)->first();
            
            $notification->markAsRead();

            switch($notification->type){
            
                case ("App\Notifications\CertificadoGeneradoNotification"):
                case ("App\Notifications\ComprobantePagoCargadoNotification"):
                case ("App\Notifications\ComprobantePagoDesestimadoNotification"):
                case ("App\Notifications\DocumentacionFormularioActualizadaNotification"):
                case ("App\Notifications\ExpedienteCerradoNotification"):
                case ("App\Notifications\ExpedienteLiquidadoNotification"):
                case ("App\Notifications\\finalProcesoAutomatizado_cerrarExpedienteNotification"):
                case ("App\Notifications\FormularioExpedienteAprobadoNotification"):
                case ("App\Notifications\FormularioExpedienteIncompletoNotification"):
                case ("App\Notifications\FormularioExpedienteDesaprobadoNotification"):                                  
                case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalEntrante_Notification"):
                case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalSaliente_Notification"):
                case ("App\Notifications\NuevoFormularioExpedienteNotification"):
                case ("App\Notifications\PrimerRecordatorio_Profesional_subirComprobantePagoNotification"):
                case ("App\Notifications\RenunciaDireccionObraNotification"):
                case ("App\Notifications\SegundoRecordatorio_Profesional_subirComprobantePagoNotification"):
                case ("App\Notifications\ValoracionFormularioExpedienteNotification"):

                    return redirect()->action(
                        [ExpedienteController::class, 'show'], ['id' => $expediente_id]
                    );



                case ("App\Notifications\SolicitudReemplazoProfesionalNotification"):
                case ("App\Notifications\NotaReemplazoProfesionalDesaprobada_ProfesionalEntrante_Notification"):
                    
                    return redirect()->action(
                        [NotaReemplazoProfesionalController::class, 'show'], ['id' => $notification->data['nota_reemplazo_profesional_id']]
                    );

                    // return app('App\Http\Controllers\NotaReemplazoProfesionalController')->show($notification->data['nota_reemplazo_profesional_id']);
                    // return $nrpController->show($notification->data['nota_reemplazo_profesional_id']);

                case ("App\Notifications\NuevoUsuarioNotification"):

                    return redirect()->action(
                        [UsuarioController::class, 'show'], ['id' => $notification->data['usuario']['id']]
                    );

            }


        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


    }




    public function marcarTodasComoLeidas(FacadesRequest $request){


        $notificacionesNoLeidas = auth()->user()->unreadNotifications;
            

        if(($notificacionesNoLeidas->count() == 0) ){
            return Redirect::to('notificaciones')->with('status', 'No hay notificaciones para marcar como leídas');    
        }

        $notificacionesNoLeidas->markAsRead();
        return Redirect::to('notificaciones')->with('success', 'Notificaciones han sido para marcadas como leídas');    

    }










    /**     
     *  Definido pero no implementado en la interfaz por elección de la entidad
     */

    public function destroy($notification){
        
        try {

            DB::beginTransaction();

            $notification->delete();

            DB::commit();


        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


        //REDIRECT? 
        return Redirect::to('notificationes.index');

    }



/* 
    public function create(){
        return view('propietario.create');
    }



    public function store(PropietarioFormRequest $request){
        $propietario= new Propietario; 
        $propietario->propietario_nombre=$request->get('nombre');
        $propietario->propietario_apellido=$request->get('apellido');
        $propietario->propietario_cuit=$request->get('cuit');
        $propietario->alta=true;


        $propietario->save();
        return Redirect::to('propietario');
    }



    public function show($id){
        return view("propietario.show",["propietario"=>Propietario::findOrFail($id)]);
    }



    public function edit($id){
        return view("propietario.edit",["propietario"=>Propietario::findOrFail($id)]);
    }



    public function update(PropietarioFormRequest $request, $id){
        $propietario=Propietario::findOrFail($id);
        $propietario->propietario_nombre=$request->get('nombre');
        $propietario->propietario_apellido=$request->get('apellido');
        $propietario->propietario_cuit=$request->get('cuit');
        $propietario->update();

        return Redirect::to('propietario');
    }

 */
    











}
