<?php

namespace App\Http\Controllers;

use App\Models\Localidad;
use App\Services\EstadisticaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EstadisticaController extends Controller {


    public $estadisticaService;



    public function __construct(EstadisticaService $estadisticaService) {

        $this->middleware('auth');
        $this->estadisticaService = $estadisticaService;

    }







    public function index(Request $request){
    

        // Se requerirán los metodos necesarios para los 3 graficos. 

        // if (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor")) {
        if (Auth()->user()->can('Ver Estadistica en sidebar')) {


            $dataGraficoTiposTareasMasRealizadas            = $this->estadisticaService->getDataForGraficoTiposTareasMasRealizadas($request);
            
            $dataGraficoTipologiasMasRealizadas             = $this->estadisticaService->getDataForGraficoTipologiasMasRealizadas($request);
            
            $dataGraficoCantidadMetrosCuadradosConstruidos  = $this->estadisticaService->getDataForGraficoCantidadMetrosCuadradosConstruidos($request);
            
            /* dd($dataGraficoTipologiasMasRealizadas, $dataGraficoTipologiasMasRealizadas, $dataGraficoCantidadMetrosCuadradosConstruidos); */

            $request->filled('fecha1')      ?  $fecha1      = $request->input('fecha1')     : $fecha1    = null;
            $request->filled('fecha2')      ?  $fecha2      = $request->input('fecha2')     : $fecha2    = null;
            $request->filled('localidad')   ?  $localidadSeleccionada   = Localidad::findOrFail($request->input('localidad'))  : $localidadSeleccionada = null;


            $localidades = Localidad::all();


            return view('estadistica.index',[

                'dataGraficoTiposTareasMasRealizadas'           => json_encode($dataGraficoTiposTareasMasRealizadas),
                'dataGraficoTipologiasMasRealizadas'            => json_encode($dataGraficoTipologiasMasRealizadas),
                'dataGraficoCantidadMetrosCuadradosConstruidos' => $dataGraficoCantidadMetrosCuadradosConstruidos,
                'localidadSeleccionada'                         => $localidadSeleccionada ,
                'fecha1'                                        => $fecha1 ,
                'fecha2'                                        => $fecha2 ,
                'localidades'                                   => $localidades,

            ]);


        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido.');      

        }
    }  












}







