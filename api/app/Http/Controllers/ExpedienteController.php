<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;


use App\Models\Expediente;
use App\Models\ComprobantePago;
use App\Models\Certificado;

use App\Http\Requests\ExpedienteFormRequest;

use App\Services\ExpedienteService;

use App\Repositories\ExpedienteQueries;
use App\Repositories\Interactions\UserQueries;


use App\Events\ValoracionFormularioExpedienteEvent;
use App\Events\ComprobantePagoCargadoEvent;
use App\Events\ComprobantePagoDesestimadoEvent;
use App\Events\ExpedienteLiquidadoEvent;
use App\Events\ExpedienteCerradoEvent;
use App\Http\Requests\ValorarFormularioExpedienteFormRequest;
use App\Jobs\generarCertificado;
use App\Models\Plano;
use App\Models\Tarea;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExpedienteController extends Controller {




    protected $expedienteService;
    protected $expedienteQueries;
    protected $userQueries;



    public function __construct (ExpedienteQueries $expedienteQueries, UserQueries $userQueries, ExpedienteService $expedienteService){

            $this->middleware(['auth',/* 'verified' */]);
            $this->expedienteQueries = $expedienteQueries;
            $this->userQueries = $userQueries;
            $this->expedienteService = $expedienteService;
    }







    // intento añadir livewire

    public function livewireindex(Request $request) {


        $currentUserRole = Auth()->user()->getRoleNames();
        $currentUserRole = $currentUserRole[0];

        $expedientesNumeros = $this->expedienteQueries->getExpedientesNumerosExistentes($currentUserRole);
        $dataFiltros = $this->expedienteQueries->getDataForIndexFiltros($currentUserRole);
        

        $expedientes = $this->expedienteQueries->getDataForExpedienteAdministrativoIndex($request);
        $expedientesProfesional= $this->expedienteQueries->getDataForExpedienteProfesionalIndex($request);
                
                return view ('expediente.livewireindex',[
                    'expedientes'               => $expedientes, 
                    'expedientesProfesional'    => $expedientesProfesional, 
                    'request'                   => $request,
                    'currentUserRole'           => $currentUserRole,
                    'expedientesNumeros'        => $expedientesNumeros,
                    'profesionales'             => $dataFiltros[0],
                    'propietarios'              => $dataFiltros[1],
                    'estados'                   => $dataFiltros[2],
                    'condiciones'               => $dataFiltros[3],
                    'objetos'                   => $dataFiltros[4],
                    'tipos_tareas'              => $dataFiltros[5],
                    'tipologias'                => $dataFiltros[6], 
                    'localidades'               => $dataFiltros[7],
                ]);  
        
            
    }  
    











    public function livewiresearch(Request $request) {


        $currentUserRole = Auth()->user()->getRoleNames();
        $currentUserRole = $currentUserRole[0];

        $expedientesNumeros = $this->expedienteQueries->getExpedientesNumerosExistentes($currentUserRole);
        $dataFiltros = $this->expedienteQueries->getDataForIndexFiltros($currentUserRole);
        

        $expedientes = $this->expedienteQueries->getDataForExpedienteAdministrativoIndex($request);
        $expedientesProfesional= $this->expedienteQueries->getDataForExpedienteProfesionalIndex($request);
                
                return view ('expediente.livewireindex',[
                    'expedientes'               => $expedientes, 
                    'expedientesProfesional'    => $expedientesProfesional, 
                    'request'                   => $request,
                    'currentUserRole'           => $currentUserRole,
                    'expedientesNumeros'        => $expedientesNumeros,
                    'profesionales'             => $dataFiltros[0],
                    'propietarios'              => $dataFiltros[1],
                    'estados'                   => $dataFiltros[2],
                    'condiciones'               => $dataFiltros[3],
                    'objetos'                   => $dataFiltros[4],
                    'tipos_tareas'              => $dataFiltros[5],
                    'tipologias'                => $dataFiltros[6], 
                    'localidades'               => $dataFiltros[7],
                ]);  
        
            
    }  
    












    public function index(Request $request) {

    
        $user = Auth()->user();

        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles = Auth()->user()->getRoleNames();
        $expedientesNumeros = $this->expedienteQueries->getExpedientesNumerosExistentes($currentUserRoles);

        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $expedientes = $this->expedienteQueries->getDataForExpedienteAdministrativoIndex($request);

                break;

            case $currentUserRoles->contains('profesional'):

                $expedientes= $this->expedienteQueries->getDataForExpedienteProfesionalIndex($request);

                break;
        }  


        $dataFiltros = $this->expedienteQueries->getDataForIndexFiltros($currentUserRoles);


        return view ('expediente.index',[
            'expedientes'               => $expedientes, 
            'currentUserRole'           => $currentUserRoles,
            'expedientesNumeros'        => $expedientesNumeros,
            'profesionales'             => $dataFiltros[0],
            'propietarios'              => $dataFiltros[1],
            'estados'                   => $dataFiltros[2],
            'condiciones'               => $dataFiltros[3],
            'objetos'                   => $dataFiltros[4],
            'tipos_tareas'              => $dataFiltros[5],
            'tipologias'                => $dataFiltros[6], 
            'localidades'               => $dataFiltros[7],
        ]);  

    }











    public function search(Request $request) {

    
        $user = Auth()->user();

        if(!$user->hasVerifiedEmail()){
            return view ('auth.instructionsForRegisteredUser');
        } elseif ($user->getRoleNames()->isEmpty()) {
            return view ('auth.waitingForRoleAssignment');
        }

        $currentUserRoles = Auth()->user()->getRoleNames();
        $expedientesNumeros = $this->expedienteQueries->getExpedientesNumerosExistentes($currentUserRoles);

        switch(true){

            case $currentUserRoles->contains('administrativo'):
            case $currentUserRoles->contains('administrativoHibrido'):
            case $currentUserRoles->contains('supervisor'):

                $expedientes = $this->expedienteQueries->getDataForExpedienteAdministrativoIndex($request);

                break;

            case $currentUserRoles->contains('profesional'):

                $expedientes= $this->expedienteQueries->getDataForExpedienteProfesionalIndex($request);

                break;
        }  


        $dataFiltros = $this->expedienteQueries->getDataForIndexFiltros($currentUserRoles);

    

            return view ('expediente.index',[
                    'expedientes'               => $expedientes, 
                    'currentUserRole'           => $currentUserRoles,
                    'expedientesNumeros'        => $expedientesNumeros,
                    'profesionales'             => $dataFiltros[0],
                    'propietarios'              => $dataFiltros[1],
                    'estados'                   => $dataFiltros[2],
                    'condiciones'               => $dataFiltros[3],
                    'objetos'                   => $dataFiltros[4],
                    'tipos_tareas'              => $dataFiltros[5],
                    'tipologias'                => $dataFiltros[6], 
                    'localidades'               => $dataFiltros[7],
                ]);  

    }











    public function create(Request $request){


        $data = $this->expedienteQueries->getDataToCreateExpediente();

        /* dd($data);  */
    
        return view('expediente.create',[
            "localidades"       => $data[0],
            "objetos"           => $data[1],
            "profesionales"     => $data[2],
            "propietarios"      => $data[3],
            "tipos_tareas"      => $data[4],
            "tipologias"        => $data[5],   
        ]);

    }













    public function store(ExpedienteFormRequest $requestExpediente){


        $expediente_id = $this->expedienteService->createExpediente($requestExpediente);
        
        if ($expediente_id != null){
            return redirect()->action(
                [ExpedienteController::class, 'show'], ['id' => $expediente_id]
            )->with('success', 'Expediente creado exitosamente');
        } else {
            return Redirect::to('expedientes')->with('error', 'Algo salió mal en Nuevo Formulario de Expediente');
        }
    }














    public function show($id){


        $expediente = Expediente::findOrfail($id);

        /**
         * Si el profesional tiene el id del expediente.profesional_id 
         *  ó 
         * el usuario tiene rol de administrativo o supervisor 
         * entonces puede ver el expediente
         */


        if (($expediente->profesional_id == Auth()->user()->profesional_id)
            || (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor"))) {

                
            if (($this->expedienteQueries->expedienteHasPlano($id))->isEmpty()){
                
              /*   dd("empty"); */
                $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteSinPLano($id);

            } elseif ($expediente->comprobante_pago_id == null) {
                
                    $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpediente($id);
                
            } else {
                
                $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteConComprobantePago($id);
            }
            /* dd($dataExtraExpediente); */
            /*  dd($expediente); */

            // cambiar nombres en siguiente renglon, variable y metodo
            $condicionesActuales = $this->expedienteQueries->getCondiciones_Actuales($expediente);


            if ($expediente->condicion_id == config('app.formulario_incompleto')){
                $motivosFormularioIncompleto = $this->expedienteQueries->getMotivosFormularioIncompleto($expediente);
            } else {
                $motivosFormularioIncompleto = null;
            }
            

            return view("expediente.show",

                compact(
                    'expediente', 
                    'condicionesActuales',
                    'motivosFormularioIncompleto', 
                ),
                
                ["dataExtraExpediente"  => $dataExtraExpediente/* [0] */, ]);


        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido. Profesionales pueden ver sus expedientes propios únicamente.');      

        }


    }











    public function dynamicshow($id){


        $expediente = Expediente::findOrfail($id);


    
        $historial_condiciones = DB::table('historial_condiciones')
        ->where('expediente_id', '=', $expediente->expediente_id)
        ->orderBy('id', 'desc')
        ->get()->first();

        /* dd($historial_condiciones->condicion_actual_id); */


        /**
         * Si el profesional tiene el id del expediente.profesional_id 
         *  ó 
         * el usuario tiene rol de administrativo o supervisor 
         * entonces puede ver el expediente
         */


        if (($expediente->profesional_id == Auth()->user()->profesional_id)
            || (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor"))) {

                
            if (($this->expedienteQueries->expedienteHasPlano($id))->isEmpty()){
                
              /*   dd("empty"); */
                $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteSinPLano($id);

            } elseif ($expediente->comprobante_pago_id == null) {
                
                    $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpediente($id);
                
            } else {
                
                $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteConComprobantePago($id);
            }
            /* dd($dataExtraExpediente); */
            /*  dd($expediente); */

            // cambiar nombres en siguiente renglon, variable y metodo
            $condicionesActuales = $this->expedienteQueries->getCondiciones_Actuales($expediente);


            if ($expediente->condicion_id == config('app.formulario_incompleto')){
                $motivosFormularioIncompleto = $this->expedienteQueries->getMotivosFormularioIncompleto($expediente);
            } else {
                $motivosFormularioIncompleto = null;
            }
            

            return view("expediente.dynamic-show",

                compact(
                    'expediente', 
                    'condicionesActuales',
                    'motivosFormularioIncompleto', 
                ),
                
                ["dataExtraExpediente"  => $dataExtraExpediente/* [0] */, ]);


        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido. Profesionales pueden ver sus expedientes propios únicamente.');      

        }


    }















    public function edit($id){


        $expediente = Expediente::findOrfail($id);


        if (($expediente->profesional_id == Auth()->user()->profesional_id)
        || (Auth()->user()->hasRole("administrativo") || Auth()->user()->hasRole("supervisor"))) {

            $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpediente($id);

            $dataExtraExpediente = $dataExtraExpediente[0];        


            $dataToCreateExpediente= $this->expedienteQueries->getDataToCreateExpediente();

            $tarea = Tarea::findOrFail($expediente->tarea_id);
            $nombreArchivoTarea = Plano::findOrFail($tarea->plano_id)->nombre_archivo;


            $rutaAlmacenamiento = 'storage/CarpetaPlanosYDocumentos/';

            $archivoTarea = file_get_contents(public_path($rutaAlmacenamiento . $nombreArchivoTarea));
            
            // dd($archivoTarea);
        

            return view("expediente.edit",[
                "expediente"            => $expediente,
                "dataExtraExpediente"   => $dataExtraExpediente,
                "localidades"           => $dataToCreateExpediente[0],
                "objetos"               => $dataToCreateExpediente[1],
                "profesionales"         => $dataToCreateExpediente[2],
                "propietarios"          => $dataToCreateExpediente[3],
                "tipos_tareas"          => $dataToCreateExpediente[4],
                "tipologias"            => $dataToCreateExpediente[5], 
                "archivoTarea"          => $archivoTarea,
            ]);


        } else {

            return Redirect::to('expedientes')->with('error', 'Acceso no permitido. Profesionales pueden editar sus expedientes propios únicamente.');      
        }

    }














    public function update(ExpedienteFormRequest $request, $id){
        
        $expediente = $this->expedienteService->editExpediente($request, $id);
        
        return Redirect::to('expedientes')->with('success', 'Expediente editado exitosamente.');
    }













/* 
    public function destroy($id){
        $expedientes=Expediente::findOrFail($id);
        $expedientes->alta=false;
        $expedientes->update();

        return Redirect::to('expedientes');
    }
 */







    /**
     * La implementación llevada a cabo en los siguientes métodos tiene eventos, que son escuchados por listeners,
     * en donde se realiza acciones en el sistema
     * sobre los objetos, sobre notificaciones, lo que el caso de uso requiera realmente.
     * 
     */


    public function valorarFormularioExpediente(ValorarFormularioExpedienteFormRequest $request, $id){


        $decision = ([
            'PlanosODocumentosCheckbox' => $request->input('PlanosODocumentosCheckbox'),
            'aprobadoCheckbox' => $request->input('aprobadoCheckbox'),
            'desaprobadoCheckbox' => $request->input('desaprobadoCheckbox'),
            'motivo' => $request->input('motivo'),
            'otroMotivo' => $request->input('otroMotivo'),
        ]);


       /*  dd($decision); */


        $expediente = Expediente::findOrFail($id);



        event(New ValoracionFormularioExpedienteEvent($expediente, $decision));


        return Redirect()->back()->with('success', 'Expediente ha sido valorado. Puede observar que la condición ha cambiado');



    }












    public function cargarArchivoDeTarea(Request $request, $id, $tarea_id){


        $archivo = $request->file('archivoSubido');

        /* dd($request); */
        
        $expedienteActualizado = $this->expedienteService->cargarArchivoDeTarea($request, $id, $tarea_id);

        return Redirect()->back()->with('success', 'Archivo ha sido cargado. El Colegio ha sido notificado del cambio en el expediente.');

    }












    public function descargarPlanoODocumento(Request $request, $id, $nombre_archivo ){
    
        /**
         *  En este método NO se implementará evento y listener como en los demás.
         * 
         * En cambio, el servicio se hará cargo totalmente de la lógica 
         * y de enviar la notificación al supervisor.
         * 
         */

        /* dd($request); */

        
        
        //$expediente = Expediente::findOrFail($id);
        //$comprobantePago = ComprobantePago::findOrFail($expediente->comprobante_pago_id);


        $rutaAlmacenamiento = 'storage/CarpetaPlanosYDocumentos/';

        $archivo = $rutaAlmacenamiento . $nombre_archivo;
        
        /* dd($archivo); */
        

        if (file_exists($archivo)) {
            return Response::download(public_path($archivo));
        } else {
            return Redirect()->back()->with('error', 'El archivo solicitado no fue encontrado. Por favor, reporte el error a Administración.');
        }


    }










    public function liquidarExpediente(Request $request, $id){

        /* dd($request); */

        /** DEUDA TÉCNICA -> CAMBIAR $decision por $monto. Se mantiene el array por si surgen más cosas que se quieren enviar,
         * y que posiblemente correspondan con la decisión que toma el usuario sobre algo puntual. 
         */
    
        $decision = ([
            'monto' => $request->input('monto'),
        ]);

        $expediente = Expediente::findOrFail($id);

        /* dd($expediente); */

        event(New ExpedienteLiquidadoEvent($expediente, $decision));


        return Redirect()->back()->with('success', 'Expediente ha sido liquidado. Puede observar que la condición ha cambiado');

    }






    public function propietarioNotificadoLiquidacion($id){


        // CAMBIAR CONDICION EXP 
        $expedienteActualizado = $this->expedienteService->cambiarCondicionExpedientePropietarioNotificadoLiquidacion($id);

        return Redirect()->back()->with('success', 'Gracias! Este dato nos es útil. Puede observar que la condición del expediente ha cambiado');
    }

















    public function cargarComprobantePago(Request $request, $id){
        
    
        $expediente = Expediente::findOrFail($id);
    
        $comprobantePago = $this->expedienteService->cargarComprobantePago($request, $expediente);

        return Redirect()->back()->with('success', 'Comprobante de Pago ha sido cargado con éxito. Puede observar que la condición del expediente ha cambiado');

    }













    public function descargarComprobantePago($id){
    
        /**
         *  En este método NO se implementará evento y listener como en los demás.
         * 
         * En cambio, el servicio se hará cargo totalmente de la lógica 
         * y de enviar la notificación al supervisor.
         * 
         */

        /* dd($request); */

        $expediente = Expediente::findOrFail($id);
    

        $comprobantePago = ComprobantePago::findOrFail($expediente->comprobante_pago_id);


        $rutaAlmacenamiento = 'storage/CarpetaComprobantesPagos/';

        $archivo = $rutaAlmacenamiento . $comprobantePago->nombre_archivo;
        
        /* dd($archivo); */
        

        if (file_exists($archivo)) {
            return Response::download(public_path($archivo));
        } else {
            return Redirect()->back()->with('error', 'El archivo solicitado no fue encontrado. Por favor, reporte el error a Administración.');
        }


    }











    public function desestimarComprobantePago($id, Request $request){
        

        $expediente = Expediente::findOrFail($id);

        $motivo = $request->input('motivo');


        event(New ComprobantePagoDesestimadoEvent($expediente, $motivo));

        return Redirect()->back()->with('success', 'Comprobante de Pago ha sido desestimado. Puede observar que la condición del expediente ha cambiado');

    }















    public function cerrarExpediente($id){

        
        $expediente = Expediente::findOrFail($id);

        $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteConComprobantePago($id);
        
        // $dataExtraExpediente = $dataExtraExpediente[0]; 
        // dd($dataExtraExpediente[0]->objeto);

        event(New ExpedienteCerradoEvent($expediente, $dataExtraExpediente));

        return Redirect()->back()->with('success', 'Expediente ha sido cerrado. Puede observar que la condición ha cambiado');

    }














    public function descargarCertificado($id) {

        $certificado = Certificado::where('expediente_id', $id)->first();

        try{

            if(!$certificado || !Storage::exists('/public/certificados/' . $certificado->certificado)){

                $dataExtraExpediente = $this->expedienteQueries->getDataToShowExpedienteConComprobantePago($id);
                generarCertificado::dispatch(Expediente::findOrFail($id), $dataExtraExpediente);

                return Redirect()->back()->with('error', 'Certificado estará listo en unos segundos. Por favor, intente de nuevo');
            };
            
            return Storage::download('/public/certificados/'. $certificado->certificado);

        } catch (\Exception $e) {
            return Redirect()->back()->with('error', 'Certificado no está listo aún. Intente en unos segundos nuevamente');
        } catch (\Throwable $e) {
            return Redirect()->back()->with('error', 'Certificado no está listo aún. Intente en unos segundos nuevamente');
        }

    }














    public function renunciarDireccionObra($id){


        $this->expedienteService->renunciarDireccionObra($id);

        return redirect()->back()->with('success', 'La renuncia a Dirección de Obra ya se ve reflejada en el expediente. Puede comprobar los cambios en condición y estado');

    }










}
