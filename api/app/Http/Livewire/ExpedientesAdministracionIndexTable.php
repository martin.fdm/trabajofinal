<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ExpedientesAdministracionIndexTable extends Component
{

    public $currentUserRole;


    public function mount($currentUserRole){

        $this->currentUserRole = $currentUserRole;
    }


    

    public function render() {

        // no pude hacer que funcinoe la query a expedienteService directamente - a través de injection en mount()

        // sigue query de getDataForExpedienteAdministrativoIndex sin los filtros en el request

        // debug($this->currentUserRole);
        $this->currentUserRole === "profesional" ? $tipo_prioridad = "expedientes.prioridad_profesional"
        : $tipo_prioridad = "expedientes.prioridad_administracion";
        // debug($tipo_prioridad);
        // debug(gettype($tipo_prioridad));

        $profesional_id =  Auth()->user()->profesional_id;
            

        $expedientes= DB::table('expedientes')

        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expedientes.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional de un expediente
        ->join('profesionales as prof', 'expedientes.profesional_id', '=', 'prof.id')

        //recuperar tipologia de expediente
        ->join('tipologias', 'expedientes.tipologia_id', '=', 'tipologias.tipologia_id')

        //recuperar tipo de tarea de un expediente
        ->join('tareas', 'expedientes.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        //recuperar condicion de expediente
        ->join('condiciones', 'expedientes.condicion_id', '=', 'condiciones.condicion_id')

            

        ->select (
            'expedientes.*',
            'prop.propietario_nombres', 'prop.propietario_apellidos', 
            'prof.profesional_nombres', 'prof.profesional_apellidos', 
            'obras.partida_inmobiliaria',
            'tipologias.tipologia', 
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion_short',
        )

    

        ->when($profesional_id, function ($query, $profesional_id) {
            return $query->where('expedientes.profesional_id', $profesional_id);
        })


        // ->orderBy('expedientes.prioridad_administracion','asc')
        ->orderBy($tipo_prioridad,'asc')
        ->orderBy('expedientes.fecha_inicio', 'desc')
        ->paginate(15);



        return view('livewire.expedientes-index-table', [

            'currentUserRole' => $this->currentUserRole,
            'expedientes' => $expedientes,
        
        ]);
    }
}

