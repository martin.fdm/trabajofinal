<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;




class ValorarFormularioExpedienteFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize() {

        return true;
    }




    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules() {
        

        return [
            
            'PlanosODocumentosCheckbox' =>'required_without_all:aprobadoCheckbox,desaprobadoCheckbox', 
            'aprobadoCheckbox'          =>'required_without_all:PlanosODocumentosCheckbox,desaprobadoCheckbox', 
            'desaprobadoCheckbox'       =>'required_without_all:PlanosODocumentosCheckbox,aprobadoCheckbox', 

            // 'motivo'                    =>'nullable|required_with:PlanosODocumentosCheckbox,on|required_with:desaprobadoCheckbox,on', 
            // 'otroMotivo'                =>'nullable|required_with:PlanosODocumentosCheckbox,on|required_with:desaprobadoCheckbox,on', 

            // 'motivo'                    =>'sometimes|required_unless:desaprobadoCheckbox,null|required_unless:PlanosODocumentosCheckbox,null',
            // 'otroMotivo'                =>'sometimes|required_unless:PlanosODocumentosCheckbox,null|required_unless:desaprobadoCheckbox,null'
            
            

        ];


    }


}
