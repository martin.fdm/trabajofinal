<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;






class PropietarioFormRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [

            'nombres'=>'required|alpha_spaces|regex:/^[\pL\s\-]+$/u|min:3|max:30',
            'apellidos'=>'required|alpha_spaces|regex:/^[\pL\s\-]+$/u|min:3|max:55',
            'cuit'=> 'required|alpha_dash|min:10|max:50',
            'email'=>'unique:App\Models\User,email',
            // 'razonSocial'=> ''       ? 
        ];
    }
}
