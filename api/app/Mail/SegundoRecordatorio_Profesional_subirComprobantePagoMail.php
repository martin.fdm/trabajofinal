<?php

namespace App\Mail;

use App\Models\Configuracion;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;











class SegundoRecordatorio_Profesional_subirComprobantePagoMail extends Mailable {

    use Queueable, SerializesModels;

    public $expediente; 
    public $dataForEmail;
    public $subject;






    /**
    * Crea un mail para enviar un segundo recordatorio al profesional de subir el Comprobante de Pago.
    * 
    * LAS PROPIEDADES DEFINIDAS EN EL CONSTRUCTOR SON POR DEFECTO ENVIADAS AL BLADE DEL MAIL  
    * @return void
    */
    

    public function __construct($expediente, $dataForEmail){


        $this->expediente = $expediente;
        $this->dataForEmail = $dataForEmail;
        $this->from = array([
            'address'   => Configuracion::get()->first()->email,
            'name'      => Configuracion::get()->first()->nombre_institucion,
        ]);
        $this->subject = "CARQ: Recuerde enviarnos el Comprobante de Pago para proceder a cerrar su expediente Nº " . $expediente->expediente_numero;

    }




    /**
     * Build the message.
     *
     * @return $this
     */

    public function build() {
            
        return $this->view('mails.SegundoRecordatorio_Profesional_subirComprobantePago');

    }

    
}
