<?php

namespace App\Mail;

use App\Models\Configuracion;
use App\Models\Expediente;
use App\Models\NotaReemplazoProfesional;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;









class notaReemplazoProfesionalAprobadaMail extends Mailable {


    use Queueable, SerializesModels;
    
    public $expediente; 
    public $nota; 
    public $subject;
    public $from;



    /* EN EL CONSTRUCOTR TENGO QUE AGREGAR TODO LO QUE SEA NECESARIO PARA ENVIAR EL MAIL
        Y LUEGO PASARLE ESAS VARIABLES A LA VISTA DEL MAIL
    */




    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct(NotaReemplazoProfesional $nota, Expediente $expediente) {

        $this->expediente = $expediente;
        $this->nota = $nota;
        $this->from = array([
            'address'   => Configuracion::get()->first()->email,
            'name'      => Configuracion::get()->first()->nombre_institucion,
        ]);
        $this->subject = "CARQ: Su nota de Reemplazo Profesional ha sido aprobada";
    }



    /**
     * Build the message.
     *
     * @return $this
     */

    public function build() {
        return $this->view('mails.notaReemplazoProfesionalAprobada');
    }



}
