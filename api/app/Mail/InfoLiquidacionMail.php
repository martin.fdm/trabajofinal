<?php

namespace App\Mail;

use App\Models\Configuracion;
use App\Models\Expediente;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;









class InfoLiquidacionMail extends Mailable {


    use Queueable, SerializesModels;
    
    public $expediente; 

    public $subject;
    public $from;



    /* EN EL CONSTRUCOTR TENGO QUE AGREGAR TODO LO QUE SEA NECESARIO PARA ENVIAR EL MAIL
        Y LUEGO PASARLE ESAS VARIABLES A LA VISTA DEL MAIL
    */




    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente) {

        $this->expediente = $expediente;
        $this->from = $this->from = array([
            'address'   => Configuracion::get()->first()->email,
            'name'      => Configuracion::get()->first()->nombre_institucion,
        ]);
        $this->subject = "CARQ: Información para abonar la liquidación de su expediente Nº " . $expediente->expediente_numero;
    }



    /**
     * Build the message.
     *
     * @return $this
     */

    public function build() {
        return $this->view('mails.InfoLiquidacion');
    }



}
