<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;



use App\Models\Configuracion;

    





class PrimerRecordatorio_Profesional_subirComprobantePagoMail extends Mailable {


    use Queueable, SerializesModels;

    public $expediente; 
    public $dataForEmail;
    public $subject;
    public $from;



    /* EN EL CONSTRUCOTR TENGO QUE AGREGAR TODO LO QUE SEA NECESARIO PARA ENVIAR EL MAIL
        Y LUEGO PASARLE ESAS VARIABLES A LA VISTA DEL MAIL  */



    /**
     * Create a new message instance.
     * 
     * LAS PROPIEDADES DEFINIDAS EN EL CONSTRUCTOR SON POR DEFECTO ENVIADAS AL BLADE DEL MAIL
     * @return void
     */

    public function __construct($expediente, $dataForEmail){


        $this->expediente = $expediente;
        $this->dataForEmail = $dataForEmail;
        $this->from = array([
            'address'   => Configuracion::get()->first()->email,
            'name'      => Configuracion::get()->first()->nombre_institucion,
        ]);
        $this->subject = "CARQ: Recuerde enviarnos el Comprobante de Pago para proceder a cerrar su expediente Nº " . $this->expediente->expediente_numero ;
    }




    /**
     * Build the message.
     *
     * @return $this
     */

    public function build(){

        return $this->view('mails.PrimerRecordatorio_Profesional_subirComprobantePago');

    }


}
