<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use OwenIt\Auditing\Models\Audit;

class AppServiceProvider extends ServiceProvider {






    /**
     * Register any application services.
     *
     * @return void
     */

    public function register() {

    }






    /**
     *  Bootstrap any application services.
     *
     *  @return void
     */

    public function boot(){

        /**
         *  Esto es para crear una regla custom de validacion de frontend; 
         *  continua en resources/lang/en/validation.php
         * 
        */ 

        //Add this custom validation rule.
        Validator::extend('alpha_spaces', function ($attribute, $value) {

            // This will only accept alpha and spaces. 
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value); 
        });





        /**
         *  Esto es para evitar que la libreria Auditing cree un registro de auditoria 
         *  si old_values y new_values estan vacíos. 
         *  Sucedía con User Model
         */

        // Audit::creating(function (Audit $model) {
        //     if (empty($model->old_values) && empty($model->new_values)) {
        //         return false;
        //     }
        // });





    }





}
