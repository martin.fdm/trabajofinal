<?php

namespace App\Events;

use App\Models\Configuracion;
use App\Models\Expediente;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;





class CertificadoGeneradoEvent {

    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $expediente;
    public $dataExtraExpediente;
    public $emisor;






    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(Expediente $expediente, $dataExtraExpediente){

        $this->expediente = $expediente;
        $this->dataExtraExpediente = $dataExtraExpediente;
        $this->emisor = Configuracion::get()->first()->nombre_institucion;
        
    }






    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
