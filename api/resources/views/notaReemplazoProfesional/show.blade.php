@extends ('layouts.admin')



{{-- {{dd($supervisores)}} --}}

@section('contenido')
    
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
      {{--   {{dd($dataExtraExpediente[0]);}} --}}
{{--     {{   $test = $data[0]   }} --}}
    {{-- {{dd($notaReemplazoProfesional);}} --}}

    <div class="row">
    
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <h2><strong>Viendo Nota de Reemplazo Profesional </strong></h2>
            <br>
            
    
            
            
            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
              
        </div> 

    </div> <!-- .row -->
    


    
            


            {!! Form::open(array('url'=>'expediente','method'=>'POST','autocomplete'=>'off')) !!}
            {{ Form::token() }}







                <div class="row col-lg-8 col-sm-8 col-md-8 col-xs-12" id="columna-izquierda">



                    <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12">

                        <h3><strong>NÚMERO : {{-- {{$expediente->expediente_numero}} --}}</strong>   </h3>
                            
                    </div>



                    <div class=" col-lg-6 col-sm-6 col-md-6 col-xs-12">

                        <!-- Profesional -->

                        <h3><strong>PROFESIONAL :</strong>

                          {{--   {{$dataExtraExpediente[0]->profesional_nombre}}
                            {{$dataExtraExpediente[0]->profesional_apellido}}
 --}}
                        </h3>

                    </div>   
                    
                    


                    <div class=" col-lg-6 col-sm-6 col-md-6 col-xs-12">
                       
                        <h3><strong>CUIT :</strong>
                            {{-- {{$dataExtraExpediente[0]->profesional_numero_matricula}} --}}
                        
                        </h3>

                    </div>    





                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        
                        <!--  Propietarios -->

                        <h3><strong>PROPIETARIO :</strong> 
                        
                           {{--  {{$dataExtraExpediente[0]->propietario_nombre}}
                            {{$dataExtraExpediente[0]->propietario_apellido}} --}}
                          
                        
                        </h3> 

                    </div>
                    
                    
                    <div class=" col-lg-6 col-sm-6 col-md-6 col-xs-12">
                       
                        <h3><strong>CUIT :</strong>
                           {{--  {{$dataExtraExpediente[0]->propietario_cuit}}  --}}
                        
                        </h3>

                    </div>    

                    
                    
                    
                    
                  

                    
                    
               

                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12 ">


                        <!-- UBICACIÓN -->

                        <br>
                        <h3><strong>UBICACIÓN</strong></h3>             
                        
      



                        
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">

                            <!--  Localidad -->
                              
                            <h4><strong>LOCALIDAD :</strong> 
                                {{-- {{$dataExtraExpediente[0]->localidad}}   --}}
                            </h4>

                        </div>






                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">

                            <!-- Calle -->   

                            <h4><strong>CALLE :</strong> 
                                {{-- {{$dataExtraExpediente[0]->calle}}  --}}
                            </h4>
                            
    
                        </div>
                   







                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">

                            <!-- Número -->     

                            <h4><strong>NÚMERO :</strong> 
                                {{-- {{$dataExtraExpediente[0]->numero}}            --}}   
                            </h4>

                        </div>





                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">

                            <!-- Barrio --> 

                            <h4><strong>BARRIO :</strong>
                                {{-- {{$dataExtraExpediente[0]->barrio}}  --}}
                            </h4>

                        </div>
                    
                    

                    </div>     <!-- .row Ubicación -->

                   


                    

                   

                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">

                        <!-- Datos catastrales -->

                        <br>
                        <h3><strong> DATOS CATASTRALES</strong></h3>  
                    



                        <div class="row col-lg-2 col-sm-2 col-md-2 col-xs-12">
                        <!-- Sección -->     

                            <h4><strong>SECCIÓN :</strong>
                                {{-- {{$dataExtraExpediente[0]->seccion}}  --}}
                            </h4>   

                        </div>





                        <div class="row col-lg-2 col-sm-2 col-md-2 col-xs-12">

                            <!-- Manzana --> 

                            <h4><strong>MANZANA :</strong>
                                {{-- {{$dataExtraExpediente[0]->manzana}}  --}}
                            </h4>

                        </div>





                        <div class="row col-lg-2 col-sm-2 col-md-2 col-xs-12">

                            <!-- Chacra -->  

                            <h4><strong>CHACRA :</strong>
                                 {{-- {{$dataExtraExpediente[0]->chacra}}  --}}
                            </h4>

                        </div>





                        <div class="row col-lg-2 col-sm-2 col-md-2 col-xs-12"> 

                            <!-- Parcela -->        

                            <h4><strong>PARCELA :</strong>
                                {{-- {{$dataExtraExpediente[0]->parcela}}  --}}
                            </h4>   
                        
                        
                        </div>




                        <div class="row col-lg-4 col-sm-4 col-md-4 col-xs-12"> 

                            <!-- Partida/s Inmobiliaria/s -->

                            <h4><strong>PARTIDA INMOBILIARIA :</strong> 
                                {{-- {{$dataExtraExpediente[0]->partida_inmobiliaria_id}} --}}
                            </h4>
                        
                        </div>
                        
                        
                       

                    </div>       <!-- .row Datos Catastrales -->
                        
                    
                    



                    
                    
                    





                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">

                        <!-- Obra -->
                    
                        <br>
                        <h3><strong>OBRA</strong></h3>  


                        
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <!-- Objetos -->

                                <h4><strong>OBJETO :</strong> 
                                    {{-- {{$dataExtraExpediente[0]->objeto}}  --}}
                                </h4>
                        
                        </div>







                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                            <!-- Tipología -->

                            <h4><strong>TIPOLOGÍA :</strong> 
                                {{-- {{$dataExtraExpediente[0]->tipologia}} --}} 
                            </h4>

                        
                        </div> 

                    
                    </div>  <!-- .row Obra -->






                   


                     




                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">


                        <!-- SUPERFICIE -->
                    
                        <br>
                        <h3><strong>SUPERFICIE (en m²)</strong></h3> 






                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">

                            <!-- Superficie a constuir -->  

                            <h4><strong>A CONSTRUIR :</strong> 
                                {{-- {{$expediente->superficie_a_construir}}  --}}
                            </h4>
                    
                        </div> 






                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                            
                            <!-- Superficie con permiso -->     
                        
                            <h4><strong>CON PERMISO :</strong>
                                {{-- {{$expediente->superficie_con_permiso}}  --}}
                            </h4>
                        
                        </div> 
                    
                    
                    
                    
                    
                    
                    
                    
                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                            
                            <!-- Superficie sin permiso --> 
                        
                            <h4><strong>SIN PERMISO :</strong> 
                                {{-- {{$expediente->superficie_sin_permiso}}  --}}
                            </h4>
                        
                        </div> 
    



                   
                    </div>   <!-- .row Superficie -->

                    
                    
                    
                  



                    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        

                        <!-- Tareas y Planos/Documentos -->

                        <br>
                        <h3><strong>TAREAS Y PLANOS / DOCUMENTOS</strong></h3> 




                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                            <!-- Tareas -->

                            <h4> <strong>TAREA :</strong> 
                                {{-- {{$dataExtraExpediente[0]->tipo_tarea}} --}}
                            </h4>

                        </div>

                   

                        


                         
                        
                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                            <!-- Plano/s o documento/s -->       

                                <h4><strong>PLANO O DOCUMENTO :</strong> 
                                    {{-- {{$dataExtraExpediente[0]->plano}} --}} <button class="btn">Descargar Plano </button>
                                </h4>


                        </div>
                        
                        
                    </div>   <!-- .row Tareas y Planos / Documentos -->





                </div>   <!-- .row columna izquierda  -->    
               


                




                <div id="columna-intermedia" class="row col-lg-1 col-sm-1 col-md-1 col-xs-12">

                     {{-- Columna Intermedia para generar espacio --}}

                </div>    






                <div class="row col-lg-3 col-sm-3 col-md-3 col-xs-12 border border-dark" id="columna-derecha">


                    <!-- Botones -->       
                    
                    <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12">
        
                       

                            <h3>ACCIONES</h3>


                           {{--  @can('supervisor.expediente.show')
                            <a href="" data-target="#modal-ValorarExpediente-{{$expediente->expediente_id}}" 
                            data-toggle="modal"><button class="btn btn-success">Valorar Expediente</button></a>

                            <br><br>
                            @endcan
            
                            @include('expediente.modal-ValorarExpediente')



                            @can('profesional.expediente.show')
                            <a href="" data-target="#modal-RenunciarDireccionObra-{{$expediente->expediente_id}}" 
                            data-toggle="modal"><button class="btn btn-danger">Renunciar a Dirección de Obra</button></a>

                            <br><br>
                            @endcan

                            @include('expediente.modal-RenunciarDireccionObra')




                            <h3>LIQUIDACION</h3>

                            @can('profesional.expediente.show')
                            <a href="" data-target="#modal-CargarComprobantePago{{$expediente->expediente_id}}"
                                 data-toggle="modal"><button class="btn btn-info">Cargar Comprobante de Pago</button></a>

                            <br><br>
                            @endcan

                            <a href="" data-target="#modal-CargarComprobantePago{{$expediente->expediente_id}}"
                                data-toggle="modal"><button class="btn btn-info">Descargar Comprobante de Pago</button></a>

                           <br><br>



                            <a href="" data-target="#modal-LiquidarExpediente{{$expediente->expediente_id}}"
                                 data-toggle="modal"><button class="btn btn-info">Liquidar Expediente</button></a>

                            <br><br>



                            <a href="" data-target="#modal-LiquidarExpediente{{$expediente->expediente_id}}"
                                data-toggle="modal"><button class="btn btn-info" id="CerrarExpedienteButton" >Cerrar Expediente</button></a>

                            <br><br>



                            <h3>CERTIFICADO</h3>

                            <button class="btn">Descargar Certificado </button>

                            <br> <br>
                        
                            <button class="btn">Imprimir Certificado </button> --}}

                            
                     </div>  
                     


                </div><!-- .row columna derecha -->  




     
             
        {!!Form::close()!!}

                  
        



@endSection


