


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-SubirArchivoNotaFirmada-{{$notaRP->nota_reemplazo_profesional_id}}">
    
    
 

        {{-- {{dd($profesionales);}} --}}



            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
            
            

            
        
            {{--  <form action="{{route('propietario.store')}}" method="post"></form>  
            @csrf   --}}


                <!-- Entrada de Nombre -->
                <div class="modal-dialog">
                    <div class="modal-content">
        
        
                        <div class="modal-header"> 
                            <h2 class="modal-title text-center">Subir Archivo de Nota Firmada

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"> x </span>    
                                </button>
                            </h2>   
                        </div>
                            



                            <div class="modal-body col-lg-12 col-sm-12 col-md-12 col-xs-12">



                                <div class="container col-lg-12 col-sm-12 col-md-12 col-xs-12" >



                                    <div class="row {{-- col-lg-12 col-sm-12 col-md-12 col-xs-12 --}}" name="primerFila">   

                                        {{-- <h2><strong>PRUEBA DE AVANCE</strong></h2> --}}

                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                            {!! Form::open(array('url'=>'reemplazo-profesional','method'=>'POST','autocomplete'=>'off', 'files'=>true)) !!}
                                            {{ Form::token() }}

                                            <div class="form-group">   <!-- Entrada de Prueba de Avance 1 -->    
                                                
                        
                                                <form method="POST" enctype="multipart/form-data" action="{{route('reemplazo-profesional.subirArchivoNotaFirmada', ['id' => $notaRP->nota_reemplazo_profesional_id])}}">
                                                
                                                    @csrf
                                                
                                                    <input type="file" name="notaFirmada" class="form-control" placeholder="vacío"> 

                                                
                                            
                                            </div> <!-- div.form-group -->  



                                        </div>


                                    </div> {{-- primerFila --}}


                            </div> <!--.container -->

                        </div> <!--.modal-body -->







                        <div class="modal-footer">

                            <!-- Botones -->                
                            <div class="form-group">
                                <a href="{{-- {{route('reemplazo-profesional.subirArchivoNotaFirmada', ['id' => $notaRP->nota_reemplazo_profesional_id])}} --}}">
                                    <button type="submit" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-primary" 
                                    formaction="{{url('/reemplazo-profesional/subirArchivoNotaFirmada', ['id' => $notaRP->nota_reemplazo_profesional_id])}}">
                                        <strong>SUBIR</strong> 
                                    </button>
                                </a>

                                <button class="col-lg-5 col-md-5 col-sm-5 col-xs-5 btn btn-danger" type="button" data-dismiss="modal"> <strong>CANCELAR</strong> </button>
                            
                                <br><br>

                                </form>
                                {!!Form::close()!!}
                            </div>
                        </div>  <!--.modal-footer -->



                    </div>    <!--  .modal-content -->
                </div>   <!--  .modal-dialog -->
            
            </form>    
           


   
           


    </div> <!--.modal -->






<!-- SELECT2 -->


@push('scripts')

<script>
    $('#localidades').select2();        
    $('#objetos').select2();        
    $('#profesionales').select2();        
    $('#propietarios').select2();        
    $('#tipologias').select2();        
    $('#tareas').select2();        
</script>


@endpush

