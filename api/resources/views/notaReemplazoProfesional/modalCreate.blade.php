


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-Create">
    
    


        {{-- {{dd($profesionales);}} --}}



            
            
            

            
            
            {!! Form::open(array('url'=>'reemplazo-profesional','method'=>'POST','autocomplete'=>'off', 'files'=>true)) !!}
            {{ Form::token() }}



                <div class="modal-dialog" style="width:50%">
                    <div class="modal-content">
        
        
                        <div class="modal-header"> 
                            <h2 class="modal-title text-center">Solicitud de Reemplazo Profesional 

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"> x </span>    
                                </button>
                            </h2>
                            
                        </div>
                            


                            <div class="modal-body">
                                
                                <div class="container col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                    @if (count($errors)>0)    
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li> {{$error}} </li>
                                                @endforeach
                                            
                                            </ul>
                                        </div>
                                    @endif    


                                    <div class="row" name="primerFila">

            
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6"> <!-- ComboBox de Profesionales -->
                                        
                                            <h2><strong>PROFESIONAL</strong></h2>   
                                        
                                            <div class="form-group col-lg-12 col-sm-12 col-md-12 col-xs-12" name="profesional">
                                            
                                                <x-combo-box.profesionales :content="$profesionales" size=null/>
                                            
                                            </div> 

                                        </div>    

                                    
                                    
                                    
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6"> <!-- ComboBox de Propietarios -->
                                        
                                            <h2><strong>PROPIETARIO</strong></h2> 
                                        
                                            <div class="form-group col-lg-12 col-sm-12 col-md-12 col-xs-12" name="propietario">
                                            
                                                <x-combo-box.propietarios :content="$propietarios" size=null/>
                                            
                                            </div>
                                        
                                        </div>
                                    
                                    
                                    </div> <!--.row name="primerFila"-->

                                
            
                            



                                    {{-- <br> --}}


                                    <div class="row " name="segundaFila">    


                                        <!-- UBICACIÓN -->
                                
                                        <h2><strong>UBICACIÓN</strong></h2> 
                                
                                
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6"> <!-- ComboBox de Localidad -->

                                            <div class="form-group col-lg-12 col-sm-12 col-md-12 col-xs-12" name="ubicacion">  

                                                <p for="localidad">LOCALIDAD</p>    
                                            
                                                <x-combo-box.localidades :content="$localidades" size=null/>
                                            
                                            </div>       
                                        
                                        </div>
                                


                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                                        
                                            <div class="form-group" name="calle">   <!-- Entrada de Calle -->   
                                            
                                                <p for="calle">CALLE</p> 
                                                <input type="text" name="calle" class="form-control" placeholder="Calle">   
                                            
                                            </div>       
                                        
                                        </div>


                                
                                    </div> <!--.row name="segundaFila"-->






                                    <div class="row" name="tercerFila">    
                                
                                
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                                        
                                            <div class="form-group" name="numero">  <!-- Entrada de Número -->        
                                            
                                                <p for="numero">NÚMERO</p>
                                                <input type="text" name="numero" class="form-control" placeholder="Número"> 
                                            
                                            </div>       
                                        
                                        </div>

                                    
                                
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                                        
                                            <div class="form-group" name="barrio">  <!-- Entrada de Barrio -->        
                                            
                                                <p for="barrio">BARRIO</p> 
                                                <input type="text" name="barrio" class="form-control" placeholder="Barrio">     
                                            
                                            </div>
                                        
                                        </div>
                                
                                    
                                
                                        <br><br> <br><br>


                                    </div> <!-- .row  name="tercerFila"-->
                            




                                    <div class="row" name="cuartaFila">   


                                        <!-- Datos catastrales -->

                                        <h2><strong>DATOS CATASTRALES</strong></h2>   
                                        {{--  <h4 class='col-lg-3 col-sm-3 col-md-3 col-xs-12'><strong>PARTIDA INMOBILIARIA</strong></h4> --}} 



                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                                            <div class="form-group" name="seccion">   <!-- Entrada de Sección -->     

                                                <p for="seccion">SECCIÓN</p> {{-- <br> --}}
                                                <input type="text" name="seccion" class="form-control" placeholder="ejemplo: 70">   

                                            </div>
                                    
                                        </div>
                                    
                                    
                                    
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                                            <div class="form-group" name="manzana">    <!-- Entrada de Manzana --> 

                                            <p for="manzana">MANZANA</p> {{-- <br> --}}
                                            <input type="text" name="manzana" class="form-control" placeholder="ejemplo: 70"> 

                                            </div>
                                        
                                        </div>
                                    
                                    

                                    
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                                            <div class="form-group" name="chacra">      <!-- Entrada de Chacra -->  

                                                <p for="chacra">CHACRA</p> {{-- <br> --}}
                                                <input type="text" name="chacra" class="form-control" placeholder="ejemplo: 70">  

                                            </div>
                                        
                                        </div>
                                    
                                    

                                    
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">

                                            <div class="form-group" name="parcela">     <!-- Entrada de Parcela -->        

                                                <p for="parcela">PARCELA</p> {{-- <br> --}}
                                                <input type="text" name="parcela" class="form-control" placeholder="ejemplo: 70">     

                                            </div>
                                        
                                        </div>
                                
                        
                                    </div>    <!--.row  name="cuartaFila"-->










                                    <div class="row" name="quintaFila">    
                                

                                        <h2><strong>SOBRE LA OBRA Y EJECUCIÓN</strong></h2>
                                        <div class="form-group">
                                        
                                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" name="">    <!-- Entrada Avance de obra -->   
                                            
                                                <p for="avance_obra">AVANCE DE OBRA (%)</p>
                                                <input type="text" name="avance_obra" class="form-control" placeholder="ejemplo: 70">  
                                            
                                            </div>       


                                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6" name="">    <!-- Checkbox Cambios Pendientes? -->   
                                            
                                                <p for="pendiente_cambios_checkbox">PLANO CONFORME AL ESTADO DE OBRA</p>
                                                <input class="form-check-input" type="checkbox" data-group="pendiente_cambios" 
                                                    name="pendiente_cambios_checkbox" id="pendiente_cambios_checkbox">
                                                <p style="display:inline" title="¿Piensa utilizar planos propios para realizar la tarea profesional?">
                                                    Marque si piensa subir planos conforme a obra para realizar la tarea
                                                </p>
                                            
                                            </div>     
                                        
                                        </div>
                                
                                
                                        <br><br> <br><br>


                                    </div> <!-- .row  name="quintaFila"-->













                                    <div class="row" name="sextaFila">   


                                        <!-- Imagenes Pruebas de Avance  -->

                                        <h2><strong>PRUEBA DE AVANCE</strong></h2>


                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                                            <div class="form-group">   <!-- Entrada de Prueba de Avance 1 -->    
                                                
                        
                                                <form method="POST" enctype="multipart/form-data" action="{{route('reemplazo-profesional.store')}}">
                                                
                                                    @csrf
                                                
                                                    <input type="file" name="pruebaAvance1" class="form-control" placeholder="vacío"> 

                                                    <input type="file" name="pruebaAvance2" class="form-control" placeholder="vacío"> 
                                                
                                                </form>
                                            
                                            </div> <!-- div.form-group -->  



                                        </div>


                                    </div> {{-- sextaFila --}}



                                

                                
                                </div> <!--.container -->

                        </div> <!--.modal-body -->
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>






                        <div class="modal-footer">

                            <!-- Botones -->                
                            <div class="form-group">
                                <button class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-primary" type="submit" > <strong>ENVIAR</strong> </button>
                                <button class="col-lg-5 col-md-5 col-sm-5 col-xs-5 btn btn-danger" type="button" data-dismiss="modal"> <strong>CANCELAR</strong> </button>
                            
                                <br><br>
                            </div>
                        </div>  <!--.modal-footer -->



                    </div>    <!--  .modal-content -->
                </div>   <!--  .modal-dialog -->
            
            </form>    
            {!!Form::close()!!}





    </div> <!--.modal -->






<!-- SELECT2 -->


@push('scripts')

<script>
    $('#localidades').select2();        
    $('#objetos').select2();        
    $('#profesionales').select2();        
    $('#propietarios').select2();        
    $('#tipologias').select2();        
    $('#tareas').select2();        
</script>


@endpush

