
@extends ('layouts.admin')





@section('contenido')



    {{--     
    
    /**
    /*
    /*
        VISTA DE NOTIFICACIONES INDEX 

    Cada usuario ve únicamente SUS notificaciones;


    --> VARIABLES DE ENTRADA: 

    $notificaciones : notificaciones que corresponden al id del usuario     

    */
    


    --> SALIDA:

    Tabla con Notificaciones que corresponden al id del usuario,
    Botones para realizar casos de uso a la brevedad,
    

    --}}







    {{-- {{dd($notificaciones);}} --}} 
    {{-- {{dd($notificacionesData);}} --}} 

    <div class="row" id="main?">     

        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h2 ><strong>Mis notificaciones</strong></h2>  
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <br>
            <a href="{{route('notificaciones.marcarTodasComoLeidas')}}">
                <button class="btn btn-success" formaction="{{url('notificaciones/marcarTodasComoLeidas')}}" style="font-size: 120%">
                    Marcar todas como leídas
                </button>
            </a>
        </div>

        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif    
    


        


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" style="min-height: 75vh;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                
                <br>
        
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover" style="font-size: 120%">

                        <thead>
                            <th class="col-lg-5 col-md-5 col-sm-5 col-xs-5">ASUNTO</th>
                            <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">EMISOR</th>
                            <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">FECHA</th>
                            <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1">OPCIONES</th>
                        </thead>
                    
                        
                            @forelse ($notificaciones as $notification) 

                                <tr>
                                    
                                    <td>{{$notification->data['asunto']}}</td>
                                    <td>{{isset($notification->data['emisor']) ? $notification->data['emisor'] : ""}}</td>
                                    <td>{{isset($notification->data['fecha']) ? $notification->data['fecha'] : ""}}</td>
                                
                                    <td>
                                        {!! Form::open(array('url'=>'/notificaciones','method'=>'post','autocomplete'=>'off')) !!}
                                        {{ Form::token() }}

                                        @switch($notification->type)

                                            @case ("App\Notifications\CertificadoGeneradoNotification")
                                            @case ("App\Notifications\ComprobantePagoCargadoNotification")
                                            @case ("App\Notifications\ComprobantePagoDesestimadoNotification")
                                            @case ("App\Notifications\DocumentacionFormularioActualizadaNotification")
                                            @case ("App\Notifications\ExpedienteCerradoNotification")
                                            @case ("App\Notifications\ExpedienteLiquidadoNotification")
                                            @case ("App\Notifications\\finalProcesoAutomatizado_cerrarExpedienteNotification")
                                            @case ("App\Notifications\FormularioExpedienteAprobadoNotification")
                                            @case ("App\Notifications\FormularioExpedienteIncompletoNotification")
                                            @case ("App\Notifications\FormularioExpedienteDesaprobadoNotification")                                 
                                            @case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalEntrante_Notification")
                                            @case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalSaliente_Notification")
                                            @case ("App\Notifications\NuevoFormularioExpedienteNotification")
                                            @case ("App\Notifications\PrimerRecordatorio_Profesional_subirComprobantePagoNotification")
                                            @case ("App\Notifications\RenunciaDireccionObraNotification")
                                            @case ("App\Notifications\SegundoRecordatorio_Profesional_subirComprobantePagoNotification")
                                            @case ("App\Notifications\ValoracionFormularioExpedienteNotification")

                                                {{-- {{dd($notification)}} --}}
                                                    @if ($notification->read_at)
                                                        {{-- {{dd($notification->data['expediente_id'])}} --}}
                                                        <a href="{{route('expedientes.show', ['id' => $notification->data['expediente_id']])}}">
                                                            <button type="submit" class="btn btn-success btn-sm" formaction="{{url('expedientes', ['id' => $notification->data['expediente_id']])}}"> 
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </a>
                                                    
                                                    @else
                                                        <a href="{{route('notificaciones.manejador', [
                                                                'notification_id' => $notification->id,
                                                                'expediente_id' => $notification->data['expediente_id']
                                                                ])}}">
                                                            <button type="submit" class="btn btn-success btn-sm" formaction="{{url('notificaciones', [
                                                                'notification_id'   => $notification->id,
                                                                'expediente_id' => $notification->data['expediente_id']
                                                            ])}}">
                                                                <i class="fa fa-eye"></i>
                                                            </button>

                                                    @endif

                                                    @break


                                                @case ("App\Notifications\SolicitudReemplazoProfesionalNotification")
                                                @case ("App\Notifications\NotaReemplazoProfesionalDesaprobada_ProfesionalEntrante_Notification")
                                                        
                                                    @if ($notification->read_at)
                                                        <a href="{{route('reemplazo-profesional.show', ['id' => $notification->data['nota_reemplazo_profesional_id']])}}">
                                                            <button type="submit" class="btn btn-success btn-sm" formaction="{{url('reemplazo-profesional', [
                                                                'id' => $notification->data['nota_reemplazo_profesional_id']
                                                            ])}}">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </a>


                                                    @else
                                                        <a href="{{route('notificaciones.manejador', [
                                                            'notification_id' => $notification->id,
                                                            ])}}">
                                                            <button type="submit" class="btn btn-success btn-sm" formaction="{{url('notificaciones', [
                                                                'notification_id'   => $notification->id,
                                                            ])}}">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </a>


                                                    @endif

                                                    {{--  <a href="#"><button  class="btn btn-success"> <i class="fa fa-eye"></i></button></a> --}}

                                                    @break


                                                @case ("App\Notifications\NuevoUsuarioNotification")

                                                    @if ($notification->read_at)
                                                    <a href="{{route('usuarios.show', ['request' => null, 'id' => $notification->data['usuario']['id']])}}">
                                                        <button type="submit" class="btn btn-success btn-sm" formaction="{{url('usuarios', [
                                                            // 'request'   => null,
                                                            'id'        => $notification->data['usuario']['id']
                                                        ])}}">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                    </a>


                                                    @else
                                                        <a href="{{route('notificaciones.manejador', [
                                                            'notification_id' => $notification->id,
                                                            ])}}">
                                                            <button type="submit" class="btn btn-success btn-sm" formaction="{{url('notificaciones', [
                                                                'notification_id'   => $notification->id,
                                                            ])}}">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </a>
                                                    
                                                    
                                                    @endif

                                                    @break
                                                        
                                                @default
                                                    

                                        {!!Form::close()!!}      
                                        @endswitch()

                                        {{-- {{$notifs->data['expediente_id']}} --}}
                                        {{-- <a href="{{route('expediente.show', ['id' => $notifs->data['expediente_id']])}}"><button  class="btn btn-success"> <i class="fa fa-eye"></i></button></a> --}}
                                    
                                        {{-- <a href="" data-target="#modal-Edit-{{$prop->propietario_id}}" data-toggle="modal"><button class="btn btn-info">Editar</button></a 
                                        <a href="" data-target="#modal-delete-{{$prop->propietario_id}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a> --}}
                                    </td>    
                                </tr>
                            
                                {{--  @include('notificaciones.modalEdit')
                                @include('notifaciones.modalDestroy') --}}

                            @empty

                                <h3>Usted no tiene notifaciones actualmente </h3>                                

                        
                            @endforelse

                                    
                        
                                



                    </table>

                </div>
                <!-- para paginar -->
                {{-- {{$notificaciones->render()}}  --}}



                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">

                        @php
                        $link_limit = 10;
                        @endphp

                        @if ($notificaciones->lastPage() > 1)
                            <ul class="pagination">
                                <li class="{{ ($notificaciones->currentPage() == 1) ? ' disabled' : '' }}">
                                    <a href="{{ $notificaciones->url(1) }}">Primero</a>
                                </li>
                                @for ($i = 1; $i <= $notificaciones->lastPage(); $i++)
                                    <?php
                                    $half_total_links = floor($link_limit / 2);
                                    $from = $notificaciones->currentPage() - $half_total_links;
                                    $to = $notificaciones->currentPage() + $half_total_links;
                                    if ($notificaciones->currentPage() < $half_total_links) {
                                        $to += $half_total_links - $notificaciones->currentPage();
                                    }
                                    if ($notificaciones->lastPage() - $notificaciones->currentPage() < $half_total_links) {
                                        $from -= $half_total_links - ($notificaciones->lastPage() - $notificaciones->currentPage()) - 1;
                                    }
                                    ?>
                                    @if ($from < $i && $i < $to)
                                        <li class="{{ ($notificaciones->currentPage() == $i) ? ' active' : '' }}">
                                            <a href="{{ $notificaciones->url($i) }}">{{ $i }}</a>
                                        </li>
                                    @endif
                                @endfor
                                <li class="{{ ($notificaciones->currentPage() == $notificaciones->lastPage()) ? ' disabled' : '' }}">
                                    <a href="{{ $notificaciones->url($notificaciones->lastPage()) }}">Último</a>
                                </li>
                            </ul>
                        @endif

                    </div> {{-- col-9 --}}

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

                        @if ($notificaciones->total() < $notificaciones->PerPage())
                            <h3> Mostrando {{$notificaciones->total()}} de {{$notificaciones->total()}} registros </h3>
            
                        @else 
            
                            <h3> Mostrando {{$notificaciones->perPage()}} de {{$notificaciones->total()}} registros </h3>
            
                        @endif
            
                    </div> {{-- col-3 --}}


                </div> {{-- row for pagination --}}
                

            </div>
        </div>
            
            
    </div> {{-- class="row" id="main?">   --}}


@endSection


