@extends ('layouts.admin')



{{--
    /**
    /*
    /*
        VISTA DE 

        -> VARIABLES DE ENTRADA: 
    





        -> SALIDA: 



    */ 
--}}
    



    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{ $test = $data[0]}} --}}
    {{-- {{dd($certificado);}} --}}




@section('contenido')
    

    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="title">
        

                <h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                    <strong>Validador de Certificados de C.A.M.</strong>
                </h2>
                

                @if (count($errors)>0)    
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        
                        </ul>
                    </div>
                @endif    
                

                
        </div> <!-- .row  name="title"-->

        <div><br><br><br><br><br><br></div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" 
        style="background-color: lightskyblue; font-size: 200%; width:70%; ">

            
            @if (($certificado) !== null)

                <h1><strong>EL CERTIFICADO ES AUTÉNTICO</strong></h1><br> 

                El Colegio de Arquitectos de la Provincia de Misiones certifica que el profesional <br>
                {{$certificado->profesional_nombres}} {{$certificado->profesional_apellidos}}, 
                M.P. {{$certificado->profesional_numero_matricula}}, CUIT {{$certificado->profesional_cuit}}, <br>
                ha <strong>cerrado exitosamente</strong> el expediente Nº {{$certificado->expediente_numero}}, <br>
                el cual registró el día 
                {{\Carbon\Carbon::createFromTimestamp(strtotime($certificado->fecha_inicio))->format('d-m-Y')}}
                a las {{\Carbon\Carbon::createFromTimestamp(strtotime($certificado->fecha_inicio))->format('H:i:s')}}. <br><br><br> 

            @else

                <h1><strong>EL CERTIFICADO NO FUE EMITIDO POR EL COLEGIO DE ARQUITECTOS DE LA PROVINCIA DE MISIONES.</strong></h1> <br><br><br> 

            @endif




        </div> {{-- col --}}


    </div>  <!-- .container -->  






@endSection


