@extends ('layouts.admin')



{{--
    /**
    /*
    /*

        -> VARIABLES DE ENTRADA: 




        -> SALIDA: 


    




    */ 
--}}
    









    {{-- {{dd($randomNumber, $randomNumbers);}} --}}
    {{-- {{dd($dataExtraExpediente[0]);}} --}}
    {{-- {{ $test = $data[0]}} --}}
    {{-- {{dd($expediente);}} --}}


@section('contenido')
    

    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="title">
        
        

            <h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            
                    <strong>Bienvenido! Aquí podra validar certificados emitidos por C.A.M.</strong>

            </h2>
                {{-- <br> --}}


            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    

        </div> <!-- .row  name="title"-->






        <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">

            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">

                <h4><strong> Ingrese el código de validación impreso en el certificado</strong></h4> <br>

            </div>                                                                                                                                    


            {!! Form::open(array('url'=>'validador_certificados/validar','method'=>'POST','autocomplete'=>'off')) !!}
            {{ Form::token() }}



                <div class="form-group col-lg-4 col-sm-4 col-md-4 col-xs-4" name="codigo">     
                
                    <label for="codigo">Código</label> <br> <!-- Entrada  de Código -->

                    <input type="text" name="codigo" class="form-control" placeholder="ejemplo: 445825123740" value="{{ $randomNumber }}"> <br>
                    <a href="{{route('validador_certificados.validar')}}">
                        <button  type="submit" class="btn btn-success col-lg-12 col-sm-12 col-md-12 col-xs-12" formaction="{{url('/validador_certificados/validar')}}" >
                            Validar
                        </button>
                    </a>
                </div>


            {!!Form::close()!!}

        </div> {{-- row --}}


    </div>  <!-- .container -->  


        





@endSection


