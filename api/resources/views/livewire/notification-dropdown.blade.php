




    <ul wire:poll.10s class="dropdown-menu" style="min-width: 650px; background-color: rgb(241, 241, 241);" name="listaNotificaciones">
                                
        <li class="user-header" style="background-color: rgb(241, 241, 241); font-size: 130%">                           {{--  CONTENIDO DE NOTIFICACIONES  --}}

            @if (!(auth()->user()->unreadNotifications)->isEmpty()) 

                <u>

                @foreach (auth()->user()->unreadNotifications->chunk(10)[0] as $notification) 


                    {!! Form::open(array('url'=>'/notificaciones','method'=>'POST','autocomplete'=>'off', 'style'=>'display:inline' )) !!}
                    {{ Form::token() }}
                    
                        @switch ($notification->type)

                            @case ("App\Notifications\CertificadoGeneradoNotification")
                            @case ("App\Notifications\ComprobantePagoCargadoNotification")
                            @case ("App\Notifications\ComprobantePagoDesestimadoNotification")
                            @case ("App\Notifications\DocumentacionFormularioActualizadaNotification")
                            @case ("App\Notifications\ExpedienteCerradoNotification")
                            @case ("App\Notifications\ExpedienteLiquidadoNotification")
                            @case ("App\Notifications\\finalProcesoAutomatizado_cerrarExpedienteNotification")
                            @case ("App\Notifications\FormularioExpedienteAprobadoNotification")
                            @case ("App\Notifications\FormularioExpedienteIncompletoNotification")
                            @case ("App\Notifications\FormularioExpedienteDesaprobadoNotification")                                 
                            @case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalEntrante_Notification")
                            @case ("App\Notifications\NotaReemplazoProfesionalAprobada_ProfesionalSaliente_Notification")
                            @case ("App\Notifications\NuevoFormularioExpedienteNotification")
                            @case ("App\Notifications\PrimerRecordatorio_Profesional_subirComprobantePagoNotification")
                            @case ("App\Notifications\RenunciaDireccionObraNotification")
                            @case ("App\Notifications\SegundoRecordatorio_Profesional_subirComprobantePagoNotification")
                            @case ("App\Notifications\ValoracionFormularioExpedienteNotification")


                                <div class="row">

                                    <div class="col col-lg-10">

                                        <li>
                                            - {{$notification->data['asunto']}}.
                                        </li>
                                    </div>
                                
                                    <div class="col col-lg-2">

                                        <a href="{{ route('notificaciones.manejador', [
                                            'notification_id'   => $notification->id,
                                            'expediente_id'     => $notification->data['expediente_id'] ])}}">

                                            <button type="submit" class="btn btn-success btn-sm" 
                                            formaction="{{url('/notificaciones', [
                                                    'notification_id'   => $notification->id,
                                                    'expediente_id'     => $notification->data['expediente_id']])}}">
                                                VER
                                            </button>
                                            <br>

                                        </a>
                                    </div>

                                </div>
                                @break;


                            @case ("App\Notifications\SolicitudReemplazoProfesionalNotification")
                            @case ("App\Notifications\NotaReemplazoProfesionalDesaprobada_ProfesionalEntrante_Notification")


                                <div class="row">
                                    <div class="col col-lg-10">

                                        <li>
                                            - {{$notification->data['asunto']}}.
                                        </li>
                                    </div>

                                    <div class="col col-lg-2">

                                        <a href="{{ route('notificaciones.manejador', [
                                            'notification_id'   => $notification->id,
                                            'expediente_id'     => null, ])}}">

                                            <button type="submit" class="btn btn-success btn-sm" 
                                            formaction="{{url('/notificaciones', [ 'notification_id'   => $notification->id, ])}}">
                                                VER
                                            </button>
                                            <br>

                                        </a>
                                    </div>

                                </div>
                                @break;

                            @case ("App\Notifications\NuevoUsuarioNotification")


                                <div class="row">
                                    <div class="col col-lg-10">

                                        <li>
                                            - {{$notification->data['asunto']}}.
                                        </li>
                                    </div>

                                    <div class="col col-lg-2">

                                        <a href="{{ route('notificaciones.manejador', [ 'notification_id'   => $notification->id, ])}}">

                                            <button type="submit" class="btn btn-success btn-sm" 
                                            formaction="{{url('/notificaciones', [ 'notification_id'   => $notification->id,])}}">
                                                VER
                                            </button>
                                            <br>

                                        </a>

                                    </div>
                                </div>    
                                @break;

                            @default

                                <div class="col col-lg-10">

                                    <li>
                                        - {{$notification->data['asunto']}}.
                                    </li>
                                </div>

                                <div class="col col-lg-2">
                                
                                    <a href="">
                                        Ver
                                    </a>

                                </div>
                                @break;

                                @endswitch

                                {!!Form::close()!!}    
                            {{--     </td> 
                            </tr>
                        </tbody>
                    </table> --}}



                @endforeach

                </u>    

            @endif


        <li class="user-footer" id="sidebar_element">   {{-- boton-verTodasNotificaciones --}}

            <div class="pull-right" >
                <a href="{{url('/notificaciones')}}" class="btn btn-default btn-flat" style="min-width: 650px; font-size: 110%; background-color: white; color:black">
                    Ver todas mis notificaciones
                </a>
            </div>
        
        </li>                                           {{--  boton-verTodasNotificaciones --}}












