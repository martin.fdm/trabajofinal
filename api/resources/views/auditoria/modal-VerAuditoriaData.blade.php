    
    {{-- {{dd($expediente)}} --}}




    @php
        use App\Models\Expediente;
    @endphp



    
    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
        id="modal-VerAuditoriaData-{{$auditoria->id}}">

        {{--  {{ Form::open(array('url'=>'expedientes', 'method'=>'GET','autocomplete'=>'off' )) }} --}}

    

            <div class="modal-dialog" style="width:60%">

                <div class="modal-content">


                    <div class="modal-header text-center">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> x </span>    
                        </button>

                        <h4 class="modal-title">Información de Auditoria (id: {{$auditoria->id}})</h4> 
                        
                        @if($auditoria->auditable_type === 'App\Models\Expediente')
                            <h4 class="modal-title">Expediente Nº {{Expediente::findOrFail($auditoria->auditable_id)->expediente_numero}}</h4> 
                        @endif

                    </div> {{-- modal-header --}}



                    <div class="modal-body">





                        {{-- ALGUNA DATA MAS --}}






                        
                        <div class="table-responsive">

                            <table class="table table-striped table-bordered table-hover">

                                <thead>

                                    <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bg-primary text-center">DATOS HISTÓRICOS</th>
                                    <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bg-primary text-center">DATOS NUEVOS</th>

                                </thead>

                                {{-- {{\Carbon\Carbon::createFromTimestamp(strtotime($auditoria->fecha))->format('d-m-Y  H:m:s')}} --}}
                            

                                <tr>
                                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center"><p>{{$auditoria->old_values}}</p></td>
                                    <td class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center"><p>{{$auditoria->new_values}}</p></td>
                                    
                                </tr>



                            </table>        

                        </div> {{-- table-responsive --}}

                    </div> {{-- modal-body --}}


                    
                    <div class="modal-footer">


                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"></div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <button type="button" class="btn btn-default col-lg-12 col-md-12 col-sm-12 col-xs-12 float-right" data-dismiss="modal">
                                Salir
                            </button>

                        </div>

                    </div> {{-- modal-footer --}}




                </div> {{-- modal-content --}}

            </div> {{-- modal-dialog --}}


        {{-- {{ Form::Close() }} --}}


    </div> {{-- modal --}}
    
    




