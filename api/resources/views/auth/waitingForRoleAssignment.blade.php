@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header text-center">
                    Bienvenido {{ucwords(strtolower(Auth()->user()->name))." ". ucwords((strtolower(Auth()->user()->last_name)))}}
                </div>

                <div class="card-body text-center">

                    La administración de {{App\Models\Configuracion::get()->first()->nombre_institucion}} evaluará su solicitud para ingresar al sistema. <br> <br>

                    Gracias
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
