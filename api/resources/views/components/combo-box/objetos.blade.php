


    @php
    /** 
     * 
     *  variables de entrada
     * 
     *  content: contenido a desplegar en el comboBox
     *  size: tamaño del select
     * 
     * 
     * 
     */




    if (!isset($size)){

        // dd($size);
        $size = 12;
    }  



    // dd($value);

    if (!isset($value)){

        // dd($size);
        $value = null;
    }  



    @endphp







        <!-- ComboBox de Objetos -->
        
        <select onchange="toggle()" name="objeto_id" id="objetos" class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" >

            @if (!isset($value)) 

                <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione objeto' }}</option>
            
            @else

                <option value="{{ $value }}" >{{ $content[$value-1]->objeto }}</option>

            @endif


            
            @if (isset($content)) 
        
                @foreach ($content as $objeto)
                    <option value="{{$objeto->objeto_id}}">{{$objeto->objeto}}</option>
                @endforeach
            
            @endif
            
        
        </select>   
