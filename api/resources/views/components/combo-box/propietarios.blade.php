





    @php
    /** 
     * 
     *  variables de entrada
     * 
     *  content: contenido a desplegar en el comboBox
     *  size: tamaño del select
     * 
     * 
     * 
     */



    if (!isset($size)){

        /* dd($size); */
        $size = 12;
    }  


    if (!isset($value)){

        // dd($size);
        $value = null;
    }  else {

        $prop = App\Models\Propietario::findOrFail($value);
        $stringPropietario = $prop->getFullName() . " ("  .$prop->propietario_cuit .')';
    }


    @endphp
    


            <!-- ComboBox de Propietarios -->
            
            <select class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="propietario_id" id="propietarios" >
                
                
            @if (!isset($value)) 

                <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione propietario' }}</option>
        
            @else

                <option value="{{ $value }}">{{ $stringPropietario }}</option>

            @endif
                
                
                @if (isset($content)) 
            
                    @foreach ($content as $propietario)

                        <option value="{{$propietario->id}}">{{$propietario->propietario_nombres }} 
                            {{ $propietario->propietario_apellidos }} ({{$propietario->propietario_cuit}})
                        </option>

                    @endforeach
                
                @endif
            
            
            </select>   