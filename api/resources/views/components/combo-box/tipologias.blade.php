



    @php
    /** 
     * 
     *  variables de entrada
     * 
     *  content: contenido a desplegar en el comboBox
     *  size: tamaño del select
     * 
     * 
     * 
     */




    if (!isset($size)){

        /* dd($size); */
        $size = 12;
    }  


    if (!isset($value)){
    
        // dd($size);
        $value = null;
    }  





    @endphp





        <!-- ComboBox de Tipologías -->
        
        <select class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="tipologia_id" id="tipologias" >
            
            
            @if (!isset($value)) 

                <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione tipologia' }}</option>
            
            @else

                <option value="{{ $value }}">{{ $content[$value-1]->tipologia }}</option>

            @endif
            
            {{-- ESTA LINEA Es UN INTENTO POR MEJORAR LO ACTUAL Y MOSTRAR EL OLD EN CASO DE QUE EXISTA --}}
            {{-- <option value="{{ $select }}" {{ (Input::old("tipologia_id") == $select ? "selected":"") }} selected disabled>{{ isset($select) ? $select : 'Seleccione tipología' }}</option> --}}
            
        
            @if (isset($content)) 
        
                @foreach ($content as $tipologia)
                    <option value="{{$tipologia->tipologia_id}}">{{$tipologia->tipologia}}</option>
                @endforeach
            
            @endif
        
        
        </select>   