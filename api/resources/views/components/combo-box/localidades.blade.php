


 


    @php
    /** 
     * 
     *  variables de entrada
     * 
     *  content: contenido a desplegar en el comboBox
     *  size: tamaño del select
     * 
     * 
     * 
     */




    if (!isset($size)){

        /* dd($size); */
        $size = 12;
    }  



    if (!isset($value)){

        // dd($size);
        $value = null;
    } else {

        $localidad_id = App\Models\Obra::findOrFail($value)->localidad_id;

    }



    @endphp





            <!-- ComboBox de Localidades -->
            
            <select class="combo col-lg-{{$size}} col-md-{{$size}} col-sm-{{$size}} col-xs-{{$size}}" name="localidad_id" id='localidades' >
                
                
            @if (!isset($value)) 

                <option value="{{-- {{ old('objeto_id') }} --}}" selected disabled>{{ isset($select) ? $select : 'Seleccione localidad' }}</option>
        
            @else
                
                <option value="{{ $localidad_id }}">{{ $content[$localidad_id-1]->localidad }}</option>
                
            @endif
                
            
                @if (isset($content))  
            
                    @foreach ($content as $localidad)
                        <option value="{{$localidad->localidad_id}}">{{$localidad->localidad }} </option>
                    @endforeach
            
            
                @endif
            
                
            </select>   