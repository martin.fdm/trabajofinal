


    @php
        /* dd($nota, $request); */
    @endphp






<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


        <TITLE> 
            Nota Reemplazo Profesional
        </TITLE> 
        
        <style> 

            table, th, td { 
                border: 1px solid black; 
                } 

        </style> 
        

        @php
        
            use Carbon\Carbon;

        @endphp

</head>


<body>



    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

            <img src="https://www.arquitectosmisiones.org.ar/wp-content/uploads/2014/04/logo.jpg" alt="LOGO" height="100" class="float-left">
            
        </div>

        <br>   <br>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 float-right text-center ">

            <h5>{{Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('d-m-Y H:i:s')}}</h5> 
            
        </div>
        <br> <br> <br>
        
        

    </div> {{-- container --}}



    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> 
        <h2>CAMBIO DE PROFESIONAL DIRECTOR DE OBRA</h2> <br> 
    </div>

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
    
        <h6>

            Lugar {{$obra['localidad']}}<br>
            Fecha {{\Carbon\Carbon::createFromTimestamp(strtotime($nota->fecha))->format('d-m-Y  H:i:s')}}  <br>

            Sr. Presidente del Colegio De Arquitectos Distrito Sur<br>
            S/D<br><br>


            Mediante la presente queda expresado que el Sr {{ucwords(strtolower($propietario_nombreCompleto))}}, documento Nº {{$propietario_dni}},<br>
            RESUELVE UN CAMBIO DE PROFESIONAL EN LA DIRECCION DE OBRA de su propiedad<br>
            ubicada en la calle {{ucwords(strtolower($request->input('calle')))}}, N° {{$request->input('numero')}}<br>
            Sección {{$request->input('seccion')}}, Manzana n° {{$request->input('manzana')}}, Lote n° {{$request->input('parcela')}}, Catastro n° {{$obra['partida_inmobiliaria']}} <br>
            de la ciudad de {{$obra['localidad']}}.<br><br>

            N” de Expediente {{$nota->expediente_afectado_numero}} del Colegio de Arquitectos / Consejo de Ingenieros (según corresponda)<br>
            cuya tarea se encontraba a cargo del Arq. (Arq./Ing./MMO según corresponda)<br>
            {{ucwords(strtolower($profesional_saliente_nombreCompleto))}}, M.P {{$profesional_saliente_numero_matricula}}.<br>
            Designando al Arq. {{ucwords(strtolower($profesional_entrante_nombreCompleto))}} matricula N° {{$profesional_entrante_numero_matricula}}<br>
            quien toma la encomienda, firmando de común acuerdo al pie de la nota conjuntamente con el comitente.<br>
            Entre ambos se acuerda un avance de obra de {{$nota->avance_obra}}%.<br><br><br><br><br><br><br>






            Firma y sello del Profesional Entrante <br><br><br><br><br><br>





            Firma Comitente<br>
            Aclaración y Nº Documento<br>


            
        </h6>




    </div>



    



</body>




</html>