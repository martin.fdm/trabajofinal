<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
</head>


<body>
    
    <h1> Segundo Recordatorio para subir el Comprobante Pago para cerrar expediente Nº {{$expediente->expediente_numero}} </h1> <br><br>

    <p>
        Estimado profesional, <br><br>

        Recuerde enviarnos el Comprobante de Pago para proceder a cerrar su expediente Nº {{$expediente->expediente_numero}},<br>
        el cual ha sido liquidado hace 20 días. <br>
        El monto a abonar es ${{$expediente->liquidacion}} (${{$expediente->liquidacion}} pesos). <br> <br>


        A los 10 días de recibido este mail,<br>
        el expediente será archivado como TRÁMITE INCONCLUSO, <br>
        teniendo que volver a completar un formulario <br> 
        que seguirá el procedimiento habitual de los expedientes,   <br>
        y cuyo monto de liquidación podría ser diferente. <br> <br>


        

        @if ($expediente->condicion_id == config('app.esperando_comprobante_1'))

            Nos interesa saber si ha provisto al propietario de los datos para realizar el pago. <br>
        
            <a href="192.168.0.138:8000/expedientes/{{$expediente->expediente_id}}"></a>
        
            <br><br>

        @endif


        Los medios de pago con los que contamos son: <br> 

        Transferencia Bancaria: <br>
            CBU BANCO MACRO: {{config('app.cbu_banco')}} <br>
            CUIT: {{config('app.cuit_colegio')}} <br> <br>


        Gracias
        <br><br><br>
    
        
        <strong>Colegio de Arquitectos de la Provincia de Misiones</strong> <br> <br>

        Mensaje enviado de forma automatizada con el sistema de Expedientes del Colegio.
    

    </p>



</body>
</html>