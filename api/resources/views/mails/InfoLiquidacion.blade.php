<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
</head>


    {{-- {{dd($expediente)}} --}}

<body>

    <h1> Información para liquidar su expediente Nº {{$expediente->expediente_numero}} </h1> <br><br>

    <p>
        Estimado profesional, <br> <br>

        Su expediente Nº {{$expediente->expediente_numero}} ha sido liquidado. <br>
        El monto a abonar es ${{$expediente->liquidacion}}. <br>  <br>


        Los medios de pago con los que contamos son:<br>

        Transferencia Bancaria: <br>
        CBU BANCO MACRO: {{config('app.cbu_banco')}} <br>
        CUIT: {{config('app.cuit_colegio')}} <br> <br>


        Gracias
        <br><br><br>
        
        
        <strong> Colegio de Arquitectos de la Provincia de Misiones </strong> <br> <br>

        Mensaje enviado de forma automatizada con el sistema de Expedientes del Colegio.
    

    </p>


</body>
</html>