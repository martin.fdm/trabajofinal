














    <div class="modal fade modal-slide-in-right" aria-hidden="true"
    role="dialog" tabindex="-1" id="modal-Create">
    

        <div class="modal-dialog">
            <div class="modal-content">

                @if (count($errors)>0)    
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        
                        </ul>
                    </div>
                @endif    
        
        
                <div class="modal-header">


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> x </span>    
                    </button>


                    <h3 class="modal-title text-center">Agregar  Propietario </h3>
                            
                </div> {{-- modal-header --}}
                        

                <div class="modal-body">

                   {{--  {!! Form::open(array('url'=>'propietarios','method'=>'POST','autocomplete'=>'off')) !!}
                    {{ Form::token() }} --}}

                        
                    <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">


                        <!-- Entrada de Nombres -->      
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <label for="nombres">NOMBRES</label> <label id="asterisco" for="nombres">*</label>
                            <input type="text" name="nombres" class="form-control" placeholder="Nombre..." value="{{ old('nombres') }}">
                            
                                
                        </div>
                            
                        
                            
                        <!-- Entrada de Apellids -->                
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                
                            <label for="apellidos">APELLIDOS</label> <label id="asterisco">*</label>
                            <input type="text" name="apellidos" class="form-control" placeholder="Apellido..." value="{{ old('apellidos') }}"> 

                        </div>
                            
                            
                            
                        <!-- Entrada de Cuit -->                
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                
                            <label for="cuit">CUIT</label> <label id="asterisco" for="">*</label>
                            <input type="text" name="cuit" class="form-control" placeholder="Cuit..." value="{{ old('cuit') }}">
                            
                                
                        </div>
                            


                        <!-- Entrada de Email -->                
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                
                            <label for="nombre">EMAIL</label>
                            <input type="text" name="email" class="form-control" placeholder="Email..." value="{{ old('email') }}">
                                
                            <p>Ingrese el mail para que enviemos también al propietario la información <br>
                            para realizar el pago de la liquidación de los expedientes que le correspondan.</p>
                        </div>
                    



                    </div> {{-- form-group --}}
                </div> {{-- modal-body --}}
                <br><br><br><br><br><br><br><br><br><br><br><br><br><br>



                <div class="modal-footer">

                    <!-- Botones -->                
                    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <button class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-primary" type="submit" >Guardar</button>
                            <button class="col-lg-5 col-md-5 col-sm-5 col-xs-5 btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>

                    </div>


                </div> {{-- modal-footer --}}

                {!!Form::close()!!}
            
            </div> {{-- modal-content --}}
        </div> {{-- modal-dialog --}}
            
            
           





      


    </div>
