









    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-cambiarAltaProfesional-{{$prof->id}}">
    



        <div class="modal-dialog">
            
            <div class="modal-content">


                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> x </span>    
                    </button>

                    <h4 class="modal-title text-center">Actualizar de alta de Profesional</h4> 
            
                </div> 



                <div class="modal-body text-center">

                    {{-- base code
                        
                        <p>Confirme que desea eliminar el profesional. <br>
                        Esta acción no borrará sus expedientes ni datos relacionados.
                    </p> --}}

                    <p>Confirme que desea actualizar el alta del profesional. <br> </p>

                    <br> {{-- borrar las dos lineas --}}
                    aca va un if que maneje que se muestra en pantalla segun el valor del alta del profesional


                </div>



                <div class="modal-footer">

                    {{ Form::open(array('action'=> array('App\Http\Controllers\ProfesionalController@destroy', $prof->id), 'method'=>'delete' )) }}

                        <button type="submit" class="btn btn-primary col-lg-6 col-md-6 col-sm-6 col-xs-6">Confirmar</button>
                    
                    {{ Form::Close() }}
                    
                    <button type="button" class="btn btn-default col-lg-5 col-md-5 col-sm-5 col-xs-5" data-dismiss="modal">Cancelar</button>
                    
                </div>

                


            </div> {{-- modal-content --}}

        </div> {{-- modal-dialog --}}


    </div> {{-- modal --}}