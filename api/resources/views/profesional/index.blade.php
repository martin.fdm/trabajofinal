
@extends ('layouts.admin')




    {{-- {{dd($profesionales);}}  --}}

{{--     
    
        VISTA DE PROPIETARIOS INDEX


        -> VARIABLES DE ENTRADA: 

    $propietarios, con todos los propietarios con alta = 1 
    y filtrados según el rol del usuario actual


    Dado que es necesario que 
    -> cada profesional vea únicamente SUS expedientes;
    -> administrativos / supervisores ven TODOS los expedientes;

    
    
--}}






@section('contenido')






    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

    

        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h2 class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><strong>Profesionales</strong></h2>  


                @if (count($errors)>0)    
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        
                        </ul>
                    </div>
                @endif  
                
                <br>

                @can('profesional.create')
                <a href="" data-target="#modal-Create" data-toggle="modal">
                    <button class="btn btn-success col-lg-1 col-md-1 col-sm-1 col-xs-1">Nuevo</button>
                </a> 
                @endcan
            
        </div> {{-- row --}}

        {{-- @include('propietario.modalCreate') --}}




    </div> {{-- row --}}

    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
                
            @elseif (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{session('error')}}
                </div>

            @else 
            @endif

        </div> {{-- col --}}





        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" name="filtros"  style="font-size: 115%">

            {{--  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros"> --}}
    
                {!! Form::open(array('url'=>'profesionales/search','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
                {{ Form::token() }}
                {{csrf_field()}}
    
                <br> 
    
    
                
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
    
                    <label for="">Profesional</label>
                    <select name="profesional" id="profesionales" class="form-control">
    
                        <option value="" selected disabled>--Seleccione--</option>
                        @foreach ($profesionales as $profesional)
                            <option value="{{$profesional->id}}">{{$profesional->profesional_nombres}} {{$profesional->profesional_apellidos}} ({{$profesional->profesional_numero_matricula}})</option>
                        @endforeach
    
                    </select>
    
    
                </div>
            
    
    
    
    
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
    
                    <label for="">Alta</label>
                    <select name="alta" id="alta" class="form-control">
    
                        <option value="" selected disabled>--Seleccione--</option>
                            <option value=1>Alta</option>
                            <option value=0>Baja</option>
    
                    </select>
    
    
                </div>
    

            </div>





            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7" name="filtros"  style="font-size: 115%">
    

                <br>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                    <div class="form-group">
                        <label>Desde</label>
                        <input type="date" id="min" name="fecha1" class="form-control">
                    </div>
    
                </div>
    
    
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="form-group">
                        <label>Hasta</label>
                        <input type="date" id="max" name="fecha2" class="form-control">
                    </div>
                </div>
    
    
                <br>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    {{-- <br> --}}
                    <div class="form-group">
                            {{-- {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}} --}}
                            <button type="submit" class="btn btn-primary btn-block">Filtrar</button>
                    </div>
                    
    
                </div>
    
    
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                    {{--  <br> --}}
                    
                    <button type="button" class="btn btn-secondary btn-block" id="limpiar">
                        Limpiar 
                    </button>
    
                </div>


            </div>
    
            {!!Form::close()!!}

        </div>






        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="min-height: 60vh;"> <br>


            <div class="table-responsive">

                <table class="table table-striped table-bordered table-hover" style="font-size: 120%">

                    <thead>
                        {{-- <th>Id</th> --}}
                        <th class="bg-primary text-left">NOMBRES</th>
                        <th class="bg-primary text-left">APELLIDOS</th>
                        <th class="bg-primary text-right">Nº MATRICULA</th>
                        <th class="bg-primary text-right">CUIT</th>
                        <th class="bg-primary text-left">ALTA</th>
                        {{-- <th>Alta</th> --}}
                        <th class="bg-primary text-left">OPCIONES</th>
                    </thead>

                    @forelse ($profesionales as $prof)

                        <tr>
                            {{--  <td>{{$prop->propietario_id}}</td> --}}
                            <td>{{$prof->profesional_nombres}}</td>
                            <td>{{$prof->profesional_apellidos}}</td>
                            <td class="text-right">{{$prof->profesional_numero_matricula}}</td>
                            <td class="text-right">{{$prof->profesional_cuit}}</td>
                            <td>
                                @if ($prof->alta == 1)
                                    Alta
                                @else
                                    Baja
                                @endif
                            </td>
                            <td>

                                {{-- <a href="{{route('propietario.edit', ['id' => $prop->propietario_id])}}"><button class="btn btn-info">Editar</button></a> --}}
                                <a href="" data-target="#modal-EditProfesional-{{$prof->id}}" data-toggle="modal">
                                    <button class="btn btn-info"> 
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                </a>


                                @if($prof->alta == 1)

                                    <a href="" data-target="#modal-deleteProfesional-{{$prof->id}}" data-toggle="modal">
                                        <button class="btn btn-danger">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </a>
                                
                                @else 
                                    <a href="" data-target="#modal-deleteProfesional-{{$prof->id}}" data-toggle="modal">
                                        <button class="btn btn-sucess">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </a>
                                
                                @endif



                                {{-- deberia eliminar el if de arriba e implementar un único modal que tome y maneje completamente el $prof --}}
                            
                                    {{-- lo siguiente no funciona todavia --}}
                            
                                {{-- <a href="" data-target="#modal-cambiarAltaProfesional-{{$prof->id}}" data-toggle="modal">
                                    <button class="btn btn-sucess">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </a> --}}


                            </td>   
                        
                            @include('profesional.modalEdit')
                            @include('profesional.modalDestroy')
                            {{-- @include('profesional.modal-cambiarAltaProfesional') --}}

                        </tr>
                    
                    @empty

                            <h3><strong>No se han registrado profesionales aún</strong></h3>
                    
                    @endforelse

                </table>

            </div> {{-- table-responsive --}}

            

        </div> {{-- col --}}




    </div> {{-- row --}}



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- para paginar -->
                
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
    
            <h3> {{$profesionales->render()}} </h3>

        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

            @if ($profesionales->total() < $profesionales->PerPage())

                <h3> Mostrando {{$profesionales->total()}} de {{$profesionales->total()}} registros </h3>

            @else 

                <h3> Mostrando {{$profesionales->perPage()}} de {{$profesionales->total()}} registros </h3>

            @endif

        </div>

    </div>


@endSection


