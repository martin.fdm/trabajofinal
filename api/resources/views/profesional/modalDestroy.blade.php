









    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-deleteProfesional-{{$prof->id}}">
    



        <div class="modal-dialog">
            
            <div class="modal-content">


                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> x </span>    
                    </button>

                    <h4 class="modal-title text-center">Eliminar Profesional</h4> 
            
                </div> 



                <div class="modal-body text-center">

                    <p>Confirme que desea eliminar el profesional. <br>
                        Esta acción no borrará sus expedientes ni datos relacionados.
                    </p>

                </div>



                <div class="modal-footer">

                    {{ Form::open(array('action'=> array('App\Http\Controllers\ProfesionalController@destroy', $prof->id), 'method'=>'delete' )) }}

                    <button type="submit" class="btn btn-primary col-lg-6 col-md-6 col-sm-6 col-xs-6">Confirmar</button>
                    <button type="button" class="btn btn-default col-lg-5 col-md-5 col-sm-5 col-xs-5" data-dismiss="modal">Cancelar</button>

                    {{ Form::Close() }}

                </div>

                


            </div> {{-- modal-content --}}

        </div> {{-- modal-dialog --}}


    </div> {{-- modal --}}