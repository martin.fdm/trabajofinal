


    {{-- {{dd($prop)}} --}}





    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-EditProfesional-{{$prof->id}}">
    
    

    

            



        <div class="modal-dialog">

            <div class="modal-content">


                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> x </span>    
                    </button>

                    <h3 class="modal-title text-center">Editar Profesional {{$prof->id}}</h3>
                    
                    @if (count($errors)>0)    
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li> {{$error}} </li>
                                @endforeach
                            
                            </ul>
                        </div>
                    @endif  


                </div>  
                    


                <div class="modal-body">

                
                    {{ Form::open(array('action'=> array('App\Http\Controllers\ProfesionalController@update', $prof->id), 'method'=>'patch' )) }}
                    {{ Form::token() }} 
                    {{csrf_field()}}
                    
                    
                        <div class="form-group"> <!-- Entrada de Nombres -->   

                            <label for="nombres">NOMBRES</label> <label id="asterisco" for="asterisco">*</label>
                            <input type="text" name="nombres" class="form-control" value="{{null !== old('nombres') ? old('nombres') : $prof->profesional_nombres}}" >
                        
                        </div>
                    
                    
                    
                                    
                        <div class="form-group"> <!-- Entrada de Apellidos -->   
                        
                            <label for="apellidos">APELLIDOS</label> <label id="asterisco" for="asterisco">*</label>
                            <input type="text" name="apellidos" class="form-control" value="{{null !== old('apellidos') ? old('apellidos') : $prof->profesional_apellidos}}">
                        
                        </div>
                    
                    


                        <div class="form-group"> <!-- Entrada de Número Matrícula -->    
                        
                            <label for="numero_matricula">Nº MATRÍCULA</label> <label id="asterisco" for="asterisco">*</label>
                            <input type="text" name="numero_matricula" class="form-control" value="{{null !== old('numero_matricula') ? old('numero_matricula') : $prof->profesional_cuit}}">
                        
                        </div>

            
                                    
                        <div class="form-group"> <!-- Entrada de Cuit -->    
                        
                            <label for="cuit">CUIT</label> <label id="asterisco" for="asterisco">*</label>
                            <input type="text" name="cuit" class="form-control" value="{{ null !== old('cuit') ? old('cuit') : $prof->profesional_cuit}}">
                        
                        </div>
                


                </div> {{-- modal-body --}}


                
                <div class="modal-footer">

                            
                    <div class="form-group">  <!-- Botones -->    
                        <button type="submit" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-primary">Guardar</button>
                        <button type="button" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 btn btn-danger" data-dismiss="modal">Cancelar</button>
                    
                        <br><br>
                    </div>

                    {!!Form::close()!!}

                </div> {{-- modal-footer --}}
                
            </div>{{-- modal-content --}}

        </div>{{-- modal-dialog --}}

    </div> {{-- modal --}}
            


        



            

            
            


        




