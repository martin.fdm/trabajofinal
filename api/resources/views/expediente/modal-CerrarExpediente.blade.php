


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-CerrarExpediente-{{$expediente->expediente_id}}">
    
    
 

           {{--  {{dd($expediente);}} --}}



            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    
            
            

           

          
            <div class="modal-dialog">

                <div class="modal-content">
    
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> X </span>    
                        </button>
                        <h3 class="modal-title text-center">Cerrar Expediente</h3>
                        
                    
                        <div class="modal-body">

                            {{-- <br> --}}

              
                           


                           
                            {!! Form::open(array('url'=>'expedientes','method'=>'PATCH','autocomplete'=>'off')) !!}
                            {{ Form::token() }}
                            @csrf



                               {{--  <div class="form-group"> --}}

                                    {{-- //AGREGAR CHECKBOX --}}
                                             
                                    

                                    {{-- <div class="form-group">

                                        <!-- Entrada de Número -->        
                                          <label for="monto">TOTAL A ABONAR</label> <br>
                                          <input type="text" name="monto" class="form-control" placeholder="$ Monto"> 
                
                                    </div>  --}}



                                    {{-- <div class="row">
                                        <input class="form-check-input" type="radio" name="liquidarCheckbox">
                                        <label class="form-check-label" for="liquidarCheckbox">
                                            Liquidar Expediente
                                        </label> --}}


                                        {{-- <input class="form-check-input" type="radio" name="desaprobadoCheckbox">
                                        <label class="form-check-label" for="desaprobadoCheckbox">
                                            Desaprobar Formulario
                                          </label> --}}

                                   {{--  </div> --}}


                             {{--    </div>     --}}




                        
                                

                        </div>
                        
                        <div class="modal-footer">
                
                            <!-- Botones --> 
                            <a href="{{route('expedientes.cerrarExpediente', ['id' => $expediente->expediente_id])}}">
                                <button type="submit"  
                                    formaction="{{url('/expedientes/cerrarExpediente', ['id' => $expediente->expediente_id])}}"
                                    class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-success" id="botonAprobarExpediente" >
                                        Confirmar
                                </button>
                            </a>



                            <button type="button" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-danger" data-dismiss="modal">Cancelar</button>



                        </div>
        
                        {!!Form::close()!!}
       

                    </div>

                </div>

            </div>
    




    </div>


