








    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-ValorarFormularioExpediente-{{$expediente->expediente_id}}">
    

        {{--  {{dd($expediente);}} --}}


            
            



        <div class="modal-dialog">

            <div class="modal-content">
    
    
                <div class="modal-header text-center">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> X </span>    
                    </button>
                    <h3 class="modal-title">Valorar Formulario de Expediente</h3>
                        
                </div>


                <div class="modal-body">



                    @if (count($errors)>0)    
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li> {{$error}} </li>
                                @endforeach
            
                            </ul>
                        </div>
                    @endif    

                    <br>
                    {!! Form::open(array('url'=>'expedientes','method'=>'PATCH','autocomplete'=>'off',)) !!}
                    {{ Form::token() }}
                        
                        <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">



                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {{-- checkbox --}}

                                <input class="form-check-input" type="checkbox" data-group="decision" onchange="toggleCheckboxs()"
                                    name="aprobadoCheckbox" id="aprobadoCheckbox"   title="Si hay encontrado todo correcto">

                                <label class="form-check-label" for="aprobadoCheckbox">
                                    Aprobar formulario
                                </label>
                            </div>


                                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {{-- checkbox --}}

                                <input class="form-check-input text " type="checkbox" name="PlanosODocumentosCheckbox" id="PlanosODocumentosCheckbox"
                                    onchange="toggleCheckboxs()">

                                <label class="form-check-label" for="PlanosODocumentosCheckbox"  
                                    name="PlanosODocumentosLabel" id="PlanosODocumentosLabel" title="Falta corregir algo del formulario">
                                    Planos o documentos entregados pendientes de cambio(s)
                                </label>
                                    
                            </div> 
                            
                            {{-- <br> --}}

                                
                                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {{-- checkbox --}}
                                    
                                <input class="form-check-input" type="checkbox" data-group="decision" 
                                    name="desaprobadoCheckbox" id="desaprobadoCheckbox" onchange="toggleCheckboxs()">

                                <label class="form-check-label" for="desaprobadoCheckbox" title="Por favor especifique el motivo por el que desaprueba el formulario">
                                    Desaprobar Formulario definitivamente
                                </label>
                            </div>
                                

                                                                
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <!-- ComboBox de Motivos -->

                                <br>

                                <label for="motivo">Indique motivo(s) u observacion(es)</label> <br>
                                
                                <x-combo-box.motivos  size=12 page="formularioExpediente"/>
        
                                <input type="text" name="otroMotivo" class="form-control" placeholder="Otro: " id="motivoInput"> 
                                    
            
                            </div> 
                            
                                
                        </div>    {{-- form-group --}}


                </div> {{-- modal-body --}}

                <br><br><br><br><br><br><br><br><br>


                        

                        
                <div class="modal-footer">   <!-- Botones --> 
            
                    <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                            
                        <a href="#">
                            <button type="submit" 
                                formaction="{{url('/expedientes/valorarFormularioExpediente', ['id' => $expediente->expediente_id])}}"
                                class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn btn-success botonAprobarExpediente" 
                                id="botonAprobarExpediente">
                                    Confirmar
                            </button>
                        </a>

                        <button type="button" class="btn btn-danger col-lg-6 col-md-6 col-sm-6 col-xs-6" data-dismiss="modal">
                            Cancelar
                        </button>

                        {!!Form::close()!!}


                    </div> {{-- form-group --}}

                </div> {{-- modal-footer --}}
        
                

            </div> {{-- modal-content --}}

        </div> {{-- modal-dialog --}}
    


        <script type="text/javascript" src="{{asset('js/expediente/show.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/expediente/useCases/valorarFormularioExpediente.js')}}"></script>



    </div> {{-- modal --}}






