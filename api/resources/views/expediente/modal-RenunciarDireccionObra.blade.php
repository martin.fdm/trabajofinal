










    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-RenunciarDireccionObra-{{$expediente->expediente_id}}">
    


            @if (count($errors)>0)    
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li> {{$error}} </li>
                        @endforeach
                    
                    </ul>
                </div>
            @endif    


        
            <div class="modal-dialog">

                <div class="modal-content">
    
    
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"> X </span>    
                        </button>

                        <h3 class="modal-title text-center">¿ Desea renunciar a la tarea Dirección de Obra 
                            correspondiente al Expediente Nº {{ $expediente->expediente_numero}} ?
                        </h3>
                        
                    </div>



                    <div class="modal-body">  <br>

                        {!! Form::open(array('url'=>'expedientes','method'=>'PATCH','autocomplete'=>'off')) !!}
                        {{ Form::token() }}
                        @csrf  
                        

                        <div class="form-group">   <!-- Botones -->                                      

                            <a href="{{route('expedientes.renunciarDireccionObra', ['id' => $expediente->expediente_id])}}">
                                <button type="submit" formaction="{{url('/expedientes/renunciarDireccionObra', ['id' => $expediente->expediente_id])}}"                                        
                                    class="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6" id="botonConfirmarRenuncia" >
                                    SI, CONFIRMO
                                </button>
                            </a>

                            {!!Form::close()!!}

                            <button type="button" class="btn btn-danger col-lg-5 col-md-5 col-sm-5 col-xs-5 " data-dismiss="modal"> 
                                <strong>NO, CANCELAR</strong>
                            </button>

                        </div>

                        
                        
                        
                        
                    </div> {{-- modal-body --}}
                        
                        
                        
                    <div class="modal-footer">
            
                    </div>
        


                </div> {{-- modal-content --}}

            </div> {{-- modal-dialog --}}
    



    </div> {{-- modal --}}
