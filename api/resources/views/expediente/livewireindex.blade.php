
@extends ('layouts.admin')







    {{-- Livewire included blade --}}

























@section('contenido')


    {{-- {{ dd($expedientes);  }} --}}
    {{-- {{ dd($expedientes->nextPageUrl());  }} --}}
    {{--  {{ dd($currentUserRole);  }} --}}

    {{-- {{ dd($expedientesProfesional);  }} --}}


    {{--  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"> --}}


        {{-- {{QrCode::format('svg')->size(150)->generate('test');}} --}}


        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <h2 class="col-lg-10 col-md-10 col-sm-10 col-xs-10"><strong>Expedientes</strong></h2>
            
            <br>
            <a href={{route('expedientes.create')}}>
                <button class="btn btn-success col-lg-2 col-md-2 col-sm-2 col-xs-2" title="Registrar Nuevo Expediente">Nuevo</button>
            </a>

            <br> <br>

            <div class="row">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                    
                @elseif (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{session('error')}}
                    </div>

                @else 
                @endif
            </div>

        </div>
        
        {{--  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <x-inputs.search searchText={{$searchText}} url="expedientes" />
        </div> --}}
    {{-- </div> --}}
    

    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros">

        {{--  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" name="filtros"> --}}

            {!! Form::open(array('url'=>'expedientes','method'=>'post','autocomplete'=>'off', 'style'=>'display:inline')) !!}
            {{ Form::token() }}
            {{csrf_field()}}

            {{-- <form class="form" action=""> --}}

            <br> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Número Expediente</label>
                <select name="expediente_numero" id="expedientesNumeros" class="form-control">
                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($expedientesNumeros as $expedienteNumero)
                        <option value="{{$expedienteNumero->expediente_numero}}">{{$expedienteNumero->expediente_numero}}</option>
                    @endforeach

                </select>

            </div>






            @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Profesional</label>
                <select name="profesional" id="profesionales" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($profesionales as $profesional)
                        <option value="{{$profesional->id}}">{{$profesional->profesional_nombres}} {{$profesional->profesional_apellidos}} ({{$profesional->profesional_numero_matricula}})</option>
                    @endforeach

                </select>


            </div>
            @endcan








            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Propietario</label>
                <select name="propietario" id="propietarios" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($propietarios as $propietario)
                        <option value="{{$propietario->id}}">{{$propietario->propietario_nombres}} {{$propietario->propietario_apellidos}} ({{$propietario->propietario_cuit}})</option>
                    @endforeach

                </select>


            </div>


            @can('profesional.expediente.index')

            {{-- para tapar el espacio vacío que tiene el index del profesional --}}
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>

            <br> <br> 

            @endcan



            <br> <br> 


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                {{-- @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                <br> 
                @endcan --}}

                <label for="">Tipologia</label>
                <select name="tipologia" id="tipologias" class="form-control">
                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($tipologias as $tipologia)
                        <option value="{{$tipologia->tipologia_id}}">{{$tipologia->tipologia}}</option>
                    @endforeach

                </select>

            </div>






            
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                {{-- @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                <br> 
                @endcan --}}

                <label for="">Tarea Profesional</label>
                <select name="tipo_tarea" id="tipos_tareas" class="form-control" {{-- data-default-value="empty" --}}>

                    <option value="" selected="selected">--Seleccione--</option>
                    @foreach ($tipos_tareas as $tipo_tarea)
                        <option value="{{$tipo_tarea->tipo_tarea_id}}">{{$tipo_tarea->tipo_tarea}}</option>
                    @endforeach

                </select>


            </div>
            


            


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                {{-- @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
                <br> 
                @endcan --}}

                <label for="">Localidad</label>
                <select name="localidad" id="localidades" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($localidades as $localidad)
                        <option value="{{$localidad->localidad_id}}">{{$localidad->localidad}}</option>
                    @endforeach

                </select>


            </div>

    </div>



    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

            <br> 
        
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Estado</label>
                <select name="estado" id="estados" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($estados as $estado)
                        <option value="{{$estado->estado_id}}">{{$estado->estado}}</option>
                    @endforeach

                </select>


            </div>




            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Condicion</label>
                <select name="condicion" id="condiciones" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($condiciones as $condicion)
                        <option value="{{$condicion->condicion_id}}">{{$condicion->condicion}}</option>
                    @endforeach

                </select>


            </div>



            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <label for="">Objeto</label>
                <select name="objeto" id="objetos" class="form-control">

                    <option value="" selected disabled>--Seleccione--</option>
                    @foreach ($objetos as $objeto)
                        <option value="{{$objeto->objeto_id}}">{{$objeto->objeto}}</option>
                    @endforeach

                </select>

                <br><br>
                
                

            </div>


            @can('administrativo.&.supervisor.columnaProfesional.expediente.index')
            <br> 
            @endcan




            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                <div class="form-group">
                    <label>Desde</label>
                    <input type="date" id="min" name="fecha1" value="" class="form-control">
                </div>

            </div>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">


                <div class="form-group">
                    <label>Hasta</label>
                    <input type="date" id="max" name="fecha2" value="" class="form-control">
                </div>
            </div>



            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">


                <br>
                <div class="form-group">
                        {{-- {{ Form::submit('Filtrar', ["class" => "btn btn-primary btn-block"])}} --}}
                        <button type="submit" class="btn btn-primary btn-block">Filtrar</button>
                </div>

            </div>


            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <br>
                <button type="reset" class="btn btn-secondary btn-block" id="limpiar" {{-- onclick="limpiarCampos()" --}}>
                    Limpiar 
                </button>


            </div>

            {{-- </form> --}}
            {!!Form::close()!!}

    </div>

    <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros">

            <br>

            {{-- <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-6" name="filtros"> --}}

            {{-- </div> --}}
            
        {{-- </div> --}}
    

    
    </div> {{-- row - filtros --}}


    

    <div class="row" style="min-height: 75vh;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            

            <br> 

            
            {{-- @if ($currentUserRole === "administrativo" or $currentUserRole === "supervisor") --}}
                @livewire('expedientes-index-table', [
                    // 'request'               => $request,
                    'currentPage'          => $expedientes->currentPage(),
                    'expedientesNumeros'    => $expedientesNumeros,
                    'currentUserRole'       => $currentUserRole,
                ]);
            {{-- @else
                @livewire('expedientes-profesional-index-table', [
                    'request'               => $request,
                    'expedientesNumeros'    => $expedientesNumeros,
                ]);
            @endif --}}



        </div> {{-- col --}}


    </div> {{-- row --}}



    {{-- <!-- para paginar --> --}}
            


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" >

            <h3> {{$expedientes->render()}} </h3>

        </div>


        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">

            @if ($expedientes->total() < $expedientes->PerPage())
                <h3> Mostrando {{$expedientes->total()}} de {{$expedientes->total()}} registros </h3>

            @else 

                <h3> Mostrando {{$expedientes->perPage()}} de {{$expedientes->total()}} registros </h3>

            @endif

        </div>

    </div>









@endSection





@section('jsScripts')


<script>
        
    $(document).ready(function() {

        $('#limpiar').click(function(){
            $("#tipos_tareas").val($("#tipos_tareas").data("default-value"));
            
        }) ;

        $("#tipos_tareas").data("default-value", $("#tipos_tareas").val())

    });
    
</script>


@endSection








<!-- SELECT2 -->


@push('scripts')












<script>
    $('#expedientesNumeros').select2();        
    $('#profesionales').select2();        
    $('#propietarios').select2();        
    $('#tipologias').select2();        
    $('#tipos_tareas').select2();        
    $('#estados').select2();        
    $('#condiciones').select2();        
    $('#objetos').select2();        
    $('#localidades').select2();        
</script>






@endpush


