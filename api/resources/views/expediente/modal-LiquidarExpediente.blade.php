


    <div class="modal fade modal-slide-in-right" aria-hidden="true"
        role="dialog" tabindex="-1" id="modal-LiquidarExpediente-{{$expediente->expediente_id}}">
    
    


        {{--  {{dd($expediente);}} --}}



        @if (count($errors)>0)    
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{$error}} </li>
                    @endforeach
                
                </ul>
            </div>
        @endif    
            
            




        <div class="modal-dialog">

            <div class="modal-content">
    
    
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> X </span>    
                    </button>
                    <h3 class="modal-title text-center">Liquidar Expediente</h3>
                        
                </div>

                    

                <div class="modal-body">
                        
                    <br>
                    {!! Form::open(array('url'=>'expedientes','method'=>'PATCH','autocomplete'=>'off')) !!}
                    {{ Form::token() }}
                    @csrf


                    <div class="form-group">

                        <label for="monto">TOTAL A ABONAR</label> <br>  <!-- Entrada de Número -->        
                        <input type="text" name="monto" class="form-control" placeholder="$ Monto" onkeyup="formatNumber(this)"> 

                    </div> 


                </div> {{-- modal-body --}}


                    
                <div class="modal-footer"> <!-- Botones --> 
            
                    <a href="{{route('expedientes.liquidarExpediente', ['id' => $expediente->expediente_id])}}">

                        <button type="submit" formaction="{{url('/expedientes/liquidarExpediente', ['id' => $expediente->expediente_id])}}"
                            class="btn btn-success col-lg-6 col-md-6 col-sm-6 col-xs-6" id="botonAprobarExpediente" >
                                Confirmar
                        </button>

                    </a>


                    <button type="button" class="btn btn-danger col-lg-6 col-md-6 col-sm-6 col-xs-6" data-dismiss="modal">Cancelar</button>

                    {!!Form::close()!!}

                </div> {{-- modal-footer --}}
        
                

            </div> {{-- modal-content --}}

        </div> {{-- modal-dialog --}}

        <script type="text/javascript">

            var nf = new Intl.NumberFormat();
            

            function numberWithCommas(value) {

                var parts = value.toString().split(".");
                parts[0]=parts[0].replace(/\B(?=(\d{3})+(?!\d))/g,".");
                return parts.join(".");
            }
        
            // var calculation = 1231739216.34367;
        
            function doIt(input){
                input.value = numberWithCommas((input.value).toFixed(2))
            }




            
            function addDots(input) {
                input.value = input.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            }

            function formatNumber(input) {

                input.value = nf.format(input.value)
                // console.log(<?php echo `input.value`?>)    
                // input.value = <?php echo number_format(`input.value`,2,",",".")?>;

            }
    
        </script>
            
    </div> {{-- modal --}}



   