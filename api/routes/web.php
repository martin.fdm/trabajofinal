<?php

use App\Events\TestEvent;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;



use App\Http\Controllers\ExpedienteController;
use App\Http\Controllers\ProfesionalController;
use App\Http\Controllers\PropietarioController;
use App\Http\Controllers\NotaReemplazoProfesionalController;
use App\Http\Controllers\EstadisticaController;
use App\Http\Controllers\InformeController;
use App\Http\Controllers\AuditoriaController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ConfiguracionController;
use App\Http\Middleware\CheckUserRoles;
use App\Mail\infoLiquidacion;
use App\Mail\InfoLiquidacionMail;
use App\Mail\PrimerRecordatorio_Profesional_subirComprobantePagoMail;
use App\Models\Configuracion;
use App\Notifications\NuevoFormularioExpedienteNotification;
use App\Notifications\TestNotification;

use App\Models\Expediente;
use App\Models\Profesional;
use App\Models\User;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Auth::routes();

// confirmar mail
Auth::routes(['verify' => true]);



// tests que necesitan estar primero

    //vacio

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/expedientes');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::middleware(['auth'])->group(function () {



Route::get('/', function () {  
    
    if (!Auth()){
        return view('auth.login');
    } else {
        return Redirect::to('expedientes');
    }
    
});

// Casos de Uso de Expedientes

Route::patch('/expedientes/renunciarDireccionObra/{id}',      'App\Http\Controllers\ExpedienteController@renunciarDireccionObra')->name('expedientes.renunciarDireccionObra');

Route::patch('/expedientes/valorarFormularioExpediente/{id}', 'App\Http\Controllers\ExpedienteController@valorarFormularioExpediente'
)->name('expedientes.valorarFormularioExpediente');

Route::post('/expedientes/cargarArchivoDeTarea/{id}/{tarea_id}', 'App\Http\Controllers\ExpedienteController@cargarArchivoDeTarea')->name('expedientes.cargarArchivoDeTarea');

Route::patch('/expedientes/liquidarExpediente/{id}', 'App\Http\Controllers\ExpedienteController@liquidarExpediente')->name('expedientes.liquidarExpediente'); 

Route::post('/expedientes/descargarPlanoODocumento/{id}/{nombre_archivo}', 'App\Http\Controllers\ExpedienteController@descargarPlanoODocumento'
)->name('expedientes.descargarPlanoODocumento'); 

Route::post('/expedientes/propietarioNotificadoLiquidacion/{id}', 'App\Http\Controllers\ExpedienteController@propietarioNotificadoLiquidacion'
)->name('expedientes.propietarioNotificadoLiquidacion'); 

Route::post('/expedientes/cargarComprobantePago/{id}', 'App\Http\Controllers\ExpedienteController@cargarComprobantePago')->name('expedientes.cargarComprobantePago'); 

Route::post('/expedientes/descargarComprobantePago/{id}', 'App\Http\Controllers\ExpedienteController@descargarComprobantePago')->name('expedientes.descargarComprobantePago'); 

Route::post('/expedientes/desestimarComprobantePago/{id}', 'App\Http\Controllers\ExpedienteController@desestimarComprobantePago')->name('expedientes.desestimarComprobantePago'); 

Route::patch('/expedientes/cerrarExpediente/{id}', 'App\Http\Controllers\ExpedienteController@cerrarExpediente')->name('expedientes.cerrarExpediente'); 

Route::post('/expedientes/generarCertificado/{id}', 'App\Http\Controllers\ExpedienteController@generarCertificado')->name('expedientes.generarCertificado'); 

Route::post('/expedientes/descargarCertificado/{id}', 'App\Http\Controllers\ExpedienteController@descargarCertificado')->name('expedientes.descargarCertificado'); 


 // Expediente - básicas

// Route::get('/expedientes/{id}',      'App\Http\Controllers\ExpedienteController@dynamicshow')   ->name('expedientes.dynamicshow');
Route::resource('expedientes', ExpedienteController::class);

Route::post('/expedientes/search',   'App\Http\Controllers\ExpedienteController@search') ->name('expedientes.search');
Route::get('/expedientes/{id}',      'App\Http\Controllers\ExpedienteController@show')   ->name('expedientes.show');
Route::post('/expedientes/{id}',     'App\Http\Controllers\ExpedienteController@show')   ->name('expedientes.show');
Route::get('/expedientes/{id}/edit', 'App\Http\Controllers\ExpedienteController@edit')   ->name('expedientes.edit');
Route::patch('/expedientes/{id}',    'App\Http\Controllers\ExpedienteController@update') ->name('expedientes.update');
Route::delete('/expedientes/{id}',   'App\Http\Controllers\ExpedienteController@destroy')->name('expedientes.destroy');

//  livewire  urls

// Route::get('/expedientes', 'App\Http\Controllers\ExpedienteController@livewireindex')   ->name('expedientes.livewireindex');
// Route::post('/search',    'App\Http\Controllers\ExpedienteController@livewiresearch')->name('expedientes.livewiresearch');


// Profesionales
Route::resource('profesionales',        ProfesionalController::class);
Route::post('/profesionales/search',      'App\Http\Controllers\ProfesionalController@search')->name('profesionales.search');
/* Route::get('/profesionales/{id}/edit', 'App\Http\Controllers\ProfesionalController@edit')->name('profesionales.edit'); */
//Route::put('/propietarios/{id}',      'App\Http\Controllers\PropietarioController@update')->name('propietarios.update');
Route::patch('/profesionales/{id}',    'App\Http\Controllers\ProfesionalController@update')->name('profesionales.update');
Route::delete('/profesionales/{id}',   'App\Http\Controllers\ProfesionalController@destroy')->name('profesionales.destroy');
// Route::patch('profesionales/{id}',      'App\Http\Controllers\ProfesionalController@cambiarValorAlta')->name('profesionales.cambiarValorAlta');





// Propietarios
Route::resource('propietarios',        PropietarioController::class);
Route::get('/propietarios/{id}/edit', 'App\Http\Controllers\PropietarioController@edit')->name('propietarios.edit');
Route::patch('/propietarios/{id}',    'App\Http\Controllers\PropietarioController@update')->name('propietarios.update');
Route::delete('/propietarios/{id}',   'App\Http\Controllers\PropietarioController@destroy')->name('propietarios.destroy');
Route::post('/propietarios/search',      'App\Http\Controllers\PropietarioController@search')->name('propietarios.search');
Route::post('/propietarios/registrarse_como_propietario', 'App\Http\Controllers\PropietarioController@registrarseComoPropietario')->name('propietarios.registrarseComoPropietario');




// Nota Reemplazo Profesional 

Route::resource('reemplazo-profesional', NotaReemplazoProfesionalController::class);
Route::post('/reemplazo-profesional', 'App\Http\Controllers\NotaReemplazoProfesionalController@store')->name('reemplazo-profesional.store');
Route::get('/reemplazo-profesional/{id}', 'App\Http\Controllers\NotaReemplazoProfesionalController@show')->name('reemplazo-profesional.show');
Route::post('/reemplazo-profesional/{id}', 'App\Http\Controllers\NotaReemplazoProfesionalController@show')->name('reemplazo-profesional.show');

Route::post('/reemplazo-profesional/subirArchivoNotaFirmada/{id}', 'App\Http\Controllers\NotaReemplazoProfesionalController@subirArchivoNotaFirmada')
->name('reemplazo-profesional.subirArchivoNotaFirmada'); 


Route::post('/reemplazo-profesional/descargarPruebaAvance/{id}/{prueba_avance}', 'App\Http\Controllers\NotaReemplazoProfesionalController@descargarPruebaAvance')
->name('reemplazo-profesional.descargarPruebaAvance'); 


Route::post('/reemplazo-profesional/valorarNotaReemplazoProfesional/{id}', 'App\Http\Controllers\NotaReemplazoProfesionalController@valorarNotaReemplazoProfesional')
->name('reemplazo-profesional.valorarNotaReemplazoProfesional'); 

Route::post('/reemplazo_profesional/search/', 'App\Http\Controllers\NotaReemplazoProfesionalController@search')->name('reemplazo-profesional.search');




// Notificaciones
Route::get('/notificaciones', [NotificationController::class, 'index']);
// Route::get('/notifications/{expediente_id}', 'App\Http\Controllers\NotificationController@manejador')->name('notifications.manejador');
Route::post('/notificaciones/{notification_id}/{expediente_id?}', 'App\Http\Controllers\NotificationController@manejador')->name('notificaciones.manejador');
Route::get('/notificaciones/marcarTodasComoLeidas', 'App\Http\Controllers\NotificationController@marcarTodasComoLeidas')->name('notificaciones.marcarTodasComoLeidas');
// Route::get('/notificaciones', 'App\Http\Controllers\NotificationController@index')->name('notificationes.index');




//Estadistica
Route::resource('estadistica', EstadisticaController::class);
Route::post('/estadistica',      'App\Http\Controllers\EstadisticaController@index')->name('estadistica');





//Informes
Route::resource('informes', InformeController::class);

Route::post(
    '/informes/generarInformeExpedientesTramitesInconclusos', 'App\Http\Controllers\InformeController@generarInformeExpedientesTramitesInconclusos'
)->name('informes.generarInformeExpedientesTramitesInconclusos');

Route::post(
    '/informes/generarInformeProfesionalesMasExpedientes', 'App\Http\Controllers\InformeController@generarInformeProfesionalesMasExpedientes'
)->name('informes.generarInformeProfesionalesMasExpedientes');






//Auditoria
Route::resource('auditoria', AuditoriaController::class);
Route::post('/auditoria/search',  'App\Http\Controllers\AuditoriaController@search')->name('auditoria.search');



//Configuracion
Route::resource('configuracion', ConfiguracionController::class);



//Usuarios
Route::resource('usuarios', UsuarioController::class);
Route::post('/usuarios/search/',    'App\Http\Controllers\UsuarioController@search')->name('usuarios.search');
Route::post('/usuarios/{id}/',    'App\Http\Controllers\UsuarioController@show')->name('usuarios.show');
// Route::patch('/usuarios/{id}',    'App\Http\Controllers\UsuarioController@update')->name('usuarios.update');
Route::patch('/usuarios/updateRolesPermisos/{id}',    'App\Http\Controllers\UsuarioController@updateRolesPermisos')->name('usuarios.updateRolesPermisos');



//Auth
Route::get('/logout','App\Http\Controllers\Auth\LoginController@logout');

});





// rutas de validador de certificados deben permanecer fuera de Auth ya que son públicas

Route::get('/validador_certificados/{randomNumber}', 'App\Http\Controllers\ValidadorCertificadosController@index')->name('validador_certificados.index');
Route::post('/validador_certificados/validar', 'App\Http\Controllers\ValidadorCertificadosController@validar')->name('validador_certificados.validar');

// Validación de Certificados
/* Route::get('/validador_certificados', [ValidadorCertificadosController::class, 'index']); */
// Route::get('/validarCertificado', ValidadorCertificadosController::class);




// lo siguiente no hace falta, y deberia sacar proximamente


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



// Route::group(
//     array('prefix' => 'Auth'), 
//     function() {
//         Route::get('logout', 'App\Http\Controllers\LogoutController@perform')->name('logout');
//     });


// });


/* Route::group(['middleware' => ['web']], function() { 


    Route::get('/logout', 'App\Http\Controllers\Auth\LogoutController@perform')->name('logout');
}); */






// tests que necesitan o pueden ir al final


    // no anda pero sirve de modelo 
Route::get('enviarNotifNewExpForm', function (){
    
    $profesional = Profesional::find(1);
    $emisor = $profesional->profesional_nombres;
    $expediente = Expediente::find(2);
    

    $users = User::all();


    // ->each(function(User $user) use($expediente){
        Notification::send($users, new TestNotification($emisor));
   /*  }); */

    $message = "here we go";

    $evento = event(New TestEvent($message));


}) ;











// Mails


Route::get('enviar_mail', function (){
    
    $expediente = Expediente::find(64);

    // $dataForEmail = $this->expedienteQueries->getDataForEmail($expediente->expediente_id);       

    $dataForEmail = DB::table('expedientes as expt')


        //recuperar nombre completo del propietario de una obra
        ->join('obras', 'expt.obra_id', '=', 'obras.obra_id')
        ->join('propietarios as prop', 'obras.propietario_id', '=', 'prop.id')

        //recuperar nombre completo del profesional del expediente
        ->join('profesionales as prof', 'expt.profesional_id', '=', 'prof.id')

        ->join('tareas', 'expt.tarea_id', '=', 'tareas.tarea_id')
        ->join('tipos_tareas', 'tareas.tipo_tarea_id', '=', 'tipos_tareas.tipo_tarea_id')

        ->join('condiciones', 'expt.condicion_id', '=', 'condiciones.condicion_id')

        // ->join('historial_condiciones','expt.expediente_id','=', 'historial_condiciones.expediente_id')


        ->select (
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.propietario_email',
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula',
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion',
            /* 'historial_condiciones.condicion_actual_id','historial_condiciones.fecha', 'historial_condiciones.expediente_id as historial_expediente_id', */
            'expt.expediente_id as expt_id',
        )

        ->where('expt.expediente_id', '=', $expediente->expediente_id)
        /* ->where('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_1'))
        ->orWhere('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_2'))
        ->orWhere('historial_condiciones.condicion_actual_id','=', config('app.esperando_comprobante_3')) */
        ->groupBy(    
            'prop.propietario_nombres', 'prop.propietario_apellidos', 'prop.propietario_cuit', 'prop.propietario_email',
            'prof.profesional_nombres', 'prof.profesional_apellidos', 'prof.profesional_numero_matricula',
            'tipos_tareas.tipo_tarea',
            'condiciones.condicion',
            /*'historial_condiciones.condicion_actual_id', 'historial_condiciones.fecha',  'historial_expediente_id', */
            'expt_id')
        // ->orderBy('','')
        ->get();


    // dd($dataForEmail);

    $profesional_email = (User::where('profesional_id','=', $expediente->profesional_id)->get()->first())->email;            /* dd($profesional_mail); */
    
    // dd($profesional_email);
    // dd(Configuracion::get()->first()->email);
    // dd(config('app.APP_NAME'));

    $propietario_email = $dataForEmail[0]->propietario_email;

    if ($propietario_email != "") {

        $emails = array($profesional_email, $propietario_email);
    }
    
    $correo = new PrimerRecordatorio_Profesional_subirComprobantePagoMail($expediente, $dataForEmail);
    Mail::to(isset($emails) ? $emails : $profesional_email)->send($correo);

    return ("mensaje enviado");
}) ;



// Route::get('phpinfo', function (){
    
//     xdebug_info();
    
// }) ;









