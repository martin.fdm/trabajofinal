<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;









class CreateProfesionalPropietarioTable extends Migration {




    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {


        Schema::create('profesional_propietario', function (Blueprint $table) {

            
        
            $table->unsignedBigInteger('profesional_id');
            $table->foreign('profesional_id')
            ->references('id')
            ->on('profesionales');

            $table->unsignedBigInteger('propietario_id');
            $table->foreign('propietario_id')
            ->references('id')
            ->on('propietarios');



        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('profesional_propietario');
    }
}
