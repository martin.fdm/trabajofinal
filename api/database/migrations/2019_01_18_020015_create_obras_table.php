<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class CreateObrasTable extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {

        Schema::create('obras', function (Blueprint $table) {


            $table->bigIncrements('obra_id');

   
           /*  $table->unsignedBigInteger('expediente_id')->nullable()->unique;
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes'); */ 


            $table->unsignedBigInteger('propietario_id')->nullable(false)->unique;
            $table->foreign('propietario_id')
            -> references ('id')
            -> on('propietarios'); 



            $table->unsignedBigInteger('localidad_id')->nullable(false)->unique;
            $table->foreign('localidad_id')
            -> references ('localidad_id')
            -> on('localidades'); 



            $table->integer('seccion')->nullable(false);
            $table->integer('chacra')->nullable(false);
            $table->integer('manzana')->nullable(false);
            $table->integer('parcela')->nullable(false);
            
            $table->string('calle', 80)->nullable(false);
            $table->integer('numero')->nullable(false);
            $table->string('barrio', 100)->nullable(false);

            $table->integer('partida_inmobiliaria')->nullable(false);
            



            //$table->timestamps();
        });

    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('obras');
    }

    
}
