<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;









class CreatePropietariosTable extends Migration {




    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {


        Schema::create('propietarios', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('propietario_nombres', 30)->nullable(false);

            $table->string('propietario_apellidos', 55)->nullable(false);

            $table->string('propietario_cuit', 30)->nullable(false);
            
            $table->string('propietario_email', 100)->nullable();

            $table->boolean('alta')->nullable(false);

            //$table->timestamps();

        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('propietarios');
    }
}
