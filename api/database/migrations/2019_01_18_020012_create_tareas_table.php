<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;








class CreateTareasTable extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('tareas', function (Blueprint $table) {

            $table->bigIncrements('tarea_id');

            $table->unsignedBigInteger('tipo_tarea_id')->nullable(false)->unique;
            $table->foreign('tipo_tarea_id')
            -> references ('tipo_tarea_id')
            -> on('tipos_tareas'); 


            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_fin')->nullable()->default(null);


            $table->unsignedBigInteger('plano_id')->nullable()->unique;
            $table->foreign('plano_id')
            -> references ('plano_id')
            -> on('planos');
            

               
            /* $table->unsignedBigInteger('expediente_id')->nullable();
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */


            $table->boolean('reemplazo')->nullable();


            //$table->timestamps();

        });

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('tareas');
    }


}
