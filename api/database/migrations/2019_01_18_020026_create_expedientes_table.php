<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class CreateExpedientesTable extends Migration {




    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()  {

        Schema::create('expedientes', function (Blueprint $table) {


            $table->bigIncrements('expediente_id');

            $table->string('expediente_numero', 50)->nullable(false);
            
            
            $table->unsignedBigInteger('profesional_id')->nullable(false)->unique;
            $table->foreign('profesional_id')
            ->references ('id')
            ->on('profesionales'); 
            


            $table->unsignedBigInteger('obra_id')->nullable(false)->unique;
            $table->foreign('obra_id')
            ->references ('obra_id')
            ->on('obras'); 



            $table->unsignedBigInteger('estado_id')->nullable(false)->unique;
            $table->foreign('estado_id')
            ->references ('estado_id')
            ->on('estados')
            ->nullable()
            ->default('0'); 



            $table->unsignedBigInteger('objeto_id')->nullable(false)->unique;
            $table->foreign('objeto_id')
            -> references ('objeto_id')
            -> on('objetos'); 



            $table->unsignedBigInteger('tipologia_id')->nullable(false)->unique;
            $table->foreign('tipologia_id')
            -> references ('tipologia_id')
            -> on('tipologias'); 



            $table->unsignedBigInteger('tarea_id')->nullable(false)->unique;
            $table->foreign('tarea_id')
            -> references ('tarea_id')
            -> on('tareas'); 



            $table->double('superficie_a_construir')->nullable();
            $table->double('superficie_con_permiso')->nullable();
            $table->double('superficie_sin_permiso')->nullable();


            $table->boolean('aprobado')->nullable();


            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_cierre')->nullable();
            $table->double('liquidacion')->nullable();



            $table->unsignedBigInteger('condicion_id')->nullable(false)->unique;
            $table->foreign('condicion_id')
            -> references ('condicion_id')
            -> on('condiciones');  



            $table->unsignedBigInteger('historial_condicion_id')->nullable()->unique;
            $table->foreign('historial_condicion_id')
            -> references ('id')
            -> on('historial_condiciones'); 



            $table->smallInteger('prioridad_administracion')->nullable;
            $table->smallInteger('prioridad_profesional')->nullable;


            $table->unsignedBigInteger('comprobante_pago_id')->nullable()->unique;
            $table->foreign('comprobante_pago_id')
            -> references ('comprobante_pago_id')
            -> on('comprobantes_pago');  



            $table->unsignedBigInteger('certificado_id')->nullable()->unique;
            $table->foreign('certificado_id')
            -> references ('certificado_id')
            -> on('certificados');  



            $table->unsignedBigInteger('nota_reemplazo_profesional_id')->nullable()->unique;
            $table->foreign('nota_reemplazo_profesional_id')
            -> references ('nota_reemplazo_profesional_id')
            -> on('notas_reemplazo_profesional');  
            




            //$table->timestamps();
        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()  {

        Schema::dropIfExists('expedientes');
    }
}
