<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;







class CreatePlanosTable extends Migration {



    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('planos', function (Blueprint $table) {

            $table->bigIncrements('plano_id');

            $table->binary('plano')->nullable();

            $table->string('nombre_archivo', 150)->nullable();

            $table->timestamp('fecha')->nullable();


               
           /*  $table->unsignedBigInteger('tarea_id')->nullable();
            $table->foreign('tarea_id')
            -> references ('tarea_id')
            -> on('tareas');  */
            
            //$table->timestamps();

        });

    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down() {

        Schema::dropIfExists('planos');
    }



}
