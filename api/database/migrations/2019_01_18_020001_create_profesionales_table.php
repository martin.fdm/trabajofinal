<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;







class CreateProfesionalesTable extends Migration {




    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {
        

        Schema::create('profesionales', function (Blueprint $table) {


            $table->bigIncrements('id');


            $table->string('profesional_nombres', 30)->nullable(false);

            $table->string('profesional_apellidos', 55)->nullable(false);

            $table->integer('profesional_numero_matricula')->nullable(false);

            $table->string('profesional_cuit', 30)->nullable(false);
            
            $table->timestamp('fecha_registro')->nullable()->default(Carbon::now()->setTimeZone('America/Argentina/Buenos_Aires')->format('Y-m-d H:i:s'));
            
            $table->boolean('alta')->nullable(false)->default(0);




            /* $table->unsignedBigInteger('expediente_id')->nullable()->unique;
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */



            /* $table->unsignedBigInteger('propietario_id')->nullable()->unique;
            $table->foreign('propietario_id')
            -> references ('propietario_id')
            -> on('propietarios');  */



            /* $table->unsignedBigInteger('nota_reemplazo_profesional_id')->nullable()->unique;
            $table->foreign('nota_reemplazo_profesional_id')
            -> references ('nota_reemplazo_profesional_id')
            -> on('notas_reemplazo_profesional');  */ 



            //$table->timestamps();
        });

    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('profesionales');
    }
}
