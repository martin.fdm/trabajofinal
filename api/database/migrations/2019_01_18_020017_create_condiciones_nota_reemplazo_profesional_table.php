<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;







class CreateCondicionesNotaReemplazoProfesionalTable extends Migration {



    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up() {

        Schema::create('condiciones_nota_reemplazo_profesional', function (Blueprint $table) {

            $table->bigIncrements('condicion_nota_reemplazo_profesional_id');


            $table->string('condicion_nota_reemplazo_profesional', 256)->nullable(false);


            //$table->timestamps();

        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('condiciones_nota_reemplazo_profesionaltable');
    }
}
