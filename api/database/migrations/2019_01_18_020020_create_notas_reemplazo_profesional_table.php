<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;








class CreateNotasReemplazoProfesionalTable extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {

        Schema::create('notas_reemplazo_profesional', function (Blueprint $table) {


            $table->bigIncrements('nota_reemplazo_profesional_id');

            $table->unsignedBigInteger('profesional_entrante_id')->nullable(false)->unique;
            $table->foreign('profesional_entrante_id')
            -> references ('id')
            -> on('profesionales'); 


            $table->unsignedBigInteger('profesional_saliente_id')->nullable(false)->unique;
            $table->foreign('profesional_saliente_id')
            -> references ('id')
            -> on('profesionales'); 


            /* ESTE VA EN EL UPDATE MIGRATE
            $table->unsignedBigInteger('expediente_afectado_id')->nullable(false)->unique;
            $table->foreign('expediente_afectado_id')
            -> references ('expediente_id')
            -> on('expedientes'); */ 
            

            $table->string('expediente_afectado_numero', 50)->nullable();

            $table->timestamp('fecha')->nullable();

            $table->binary('nota_reemplazo_profesional')->nullable();

            $table->string('nombre_archivo', 256)->nullable();

            $table->binary('prueba_avance_1')->nullable();
            $table->binary('prueba_avance_2')->nullable();

            $table->smallInteger('avance_obra')->nullabe(false);

            $table->boolean('nota_aprobada')->nullable();

            
            $table->unsignedBigInteger('condicion_nrp_id')->nullable(false);
            $table->foreign('condicion_nrp_id')
            -> references ('condicion_nota_reemplazo_profesional_id')
            -> on('condiciones_nota_reemplazo_profesional'); 

            $table->string('observaciones', 256)->nullable();

            $table->boolean('pendiente_cambios')->nullable()->default(0);

            $table->boolean('alta')->nullable()->default(0);




            //$table->timestamps();

        });

    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {
        
        Schema::dropIfExists('notas_reemplazo_profesional');
    }
}
