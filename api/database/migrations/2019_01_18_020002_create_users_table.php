<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;





class CreateUsersTable extends Migration {


    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up() {

        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('last_name', 100);
            $table->string('email')->unique();
            $table->string('cuit', 30)->nullable(false);
            $table->integer('numero_matricula')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            // $table->string('role')->nullable();
            

            $table->unsignedBigInteger('profesional_id')->nullable()->unique;
            $table->foreign('profesional_id')
            ->references ('id')
            ->on('profesionales'); 


            $table->unsignedBigInteger('propietario_id')->nullable()->unique;
            $table->foreign('propietario_id')
            ->references ('id')
            ->on('propietarios'); 



            $table->rememberToken();
            $table->timestamps();
        });
    }



    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down() {

        Schema::dropIfExists('users');
    }
}
