<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;






class CreateHistorialCondicionesTable extends Migration {
    
    
    
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up() {


        Schema::create('historial_condiciones', function (Blueprint $table) {

            $table->id('id');

            /* $table->unsignedBigInteger('expediente_id')->nullable(false)->unique;
            $table->foreign('expediente_id')
            -> references ('expediente_id')
            -> on('expedientes');  */

    
            $table->unsignedBigInteger('condicion_actual_id')->nullable(false)->unique;
            $table->foreign('condicion_actual_id')
            -> references ('condicion_id')
            -> on('condiciones');


            $table->timestamp('fecha')->nullable();

        
            //$table->timestamps();
        });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */


    public function down()  {

        Schema::dropIfExists('historial_condiciones');
    }
}
