<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


use App\Models\User;




class RoleSeeder extends Seeder {


    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {


        // CREAMOS PERMISOS

        $permissions = [

            // Sidebar Access

            'Ver Expedientes en sidebar',
            'Ver Profesionales en sidebar',
            'Ver Propietarios en sidebar',
            'Ver Reemplazo Profesional en sidebar',
            'Ver Estadistica en sidebar',
            'Ver Informes en sidebar',
            'Ver Auditoria en sidebar',
            'Ver Usuarios en sidebar',

            // Expedientes related

            'Valorar formulario de expediente',
            'Liquidar expediente',
            'Cerrar expediente',
            'Desestimar Comprobante de Pago',
            'Cargar Archivo de Tarea',
            'Cargar Comprobante de Pago',
            'Renunciar a Direccion de Obra',
            'Eliminar expediente',

            // Expediente Graphic Interface

            'Ver filtro de Profesional en el indice de expedientes',
            'Ver tabla de expedientes en el indice de expedientes como Administrativo',
            'Ver tabla de expedientes en el indice de expedientes como Profesional',
            'Ver comboBox de Profesionales en Completar formulario de expediente y Editar formulario de Expediente',
            'Ver boton para Desestimar Comprobante de Pago al visualizar un expediente',
            'Tiene el bache del espacio vacio en la interfaz de expediente index porque no ve el filtro de Profesional. (Permiso [temporal] para profesionales)',


            // Propietarios related

            'Ver boton Nuevo Propietario en indice de Propietarios',
            'Ver boton Registrarme como Propietario en indice de Propietarios',

            //Reemplazo Profesional related

            'Valorar Solicitud Reemplazo Profesional',
            'Completar Formulario de Nota de Reemplazo Profesional',
            'Subir archivo de Nota de Reemplazo Profesional firmada',
            'Ver columna de Profesional Entrante en indice de Reemplazo Profesional',


            // Usuarios related

            'Ver roles y permisos de usuarios',
            'Ver y editar roles y permisos de usuarios',


            // Configuracion related

            'Manipular Configuracion del sistema',
        ];



        foreach($permissions as $permission){

            Permission::updateOrCreate([
                'name'  =>  $permission, 
            ]);
        }





        $roleAdministrativo = Role::updateOrCreate(['name'=>'administrativo']);

        $administrativoPermissions = [

            'Ver Expedientes en sidebar',
            'Ver Propietarios en sidebar',
            'Ver Reemplazo Profesional en sidebar',
            'Ver Estadistica en sidebar',
            'Ver Informes en sidebar',
            'Ver Usuarios en sidebar',
            'Desestimar Comprobante de Pago',
            'Ver filtro de Profesional en el indice de expedientes',
            'Ver tabla de expedientes en el indice de expedientes como Administrativo',
            'Ver comboBox de Profesionales en Completar formulario de expediente y Editar formulario de Expediente',
            'Ver boton para Desestimar Comprobante de Pago al visualizar un expediente',
            'Ver columna de Profesional Entrante en indice de Reemplazo Profesional',
            'Ver roles y permisos de usuarios',

        ];


        foreach($administrativoPermissions as $permission){
            $roleAdministrativo->givePermissionTo($permission);            
        }




        $roleSupervisor     = Role::updateOrCreate(['name'=>'supervisor']);


        $supervisorPermissions = [

            'Ver Expedientes en sidebar',
            'Ver Profesionales en sidebar',
            'Ver Propietarios en sidebar',
            'Ver Reemplazo Profesional en sidebar',
            'Ver Estadistica en sidebar',
            'Ver Informes en sidebar',
            'Ver Auditoria en sidebar',
            'Ver Usuarios en sidebar',
            'Valorar formulario de expediente',
            'Liquidar expediente',
            'Cerrar expediente',
            'Desestimar Comprobante de Pago',
            'Eliminar expediente',
            'Ver filtro de Profesional en el indice de expedientes',
            'Ver tabla de expedientes en el indice de expedientes como Administrativo',
            'Ver comboBox de Profesionales en Completar formulario de expediente y Editar formulario de Expediente',
            'Ver boton para Desestimar Comprobante de Pago al visualizar un expediente',
            'Valorar Solicitud Reemplazo Profesional',
            'Ver columna de Profesional Entrante en indice de Reemplazo Profesional',
            'Ver y editar roles y permisos de usuarios',
            'Manipular Configuracion del sistema',

        ];


        foreach($supervisorPermissions as $permission){
            $roleSupervisor->givePermissionTo($permission);            
        }




        $roleAdministrativoHibrido = Role::updateOrCreate(['name' => 'administrativoHibrido']);






        $roleProfesional    = Role::updateOrCreate(['name'=>'profesional']);

        $profesionalPermissions = [

            'Ver Expedientes en sidebar',
            'Ver Propietarios en sidebar',
            'Ver Reemplazo Profesional en sidebar',
            'Ver boton Registrarme como Propietario en indice de Propietarios',
            'Cargar Archivo de Tarea',
            'Cargar Comprobante de Pago',
            'Renunciar a Direccion de Obra',
            'Ver tabla de expedientes en el indice de expedientes como Profesional',
            'Tiene el bache del espacio vacio en la interfaz de expediente index porque no ve el filtro de Profesional. (Permiso [temporal] para profesionales)',
            'Ver boton Nuevo Propietario en indice de Propietarios',
            'Completar Formulario de Nota de Reemplazo Profesional',
            'Subir archivo de Nota de Reemplazo Profesional firmada',

        ];

        foreach($profesionalPermissions as $permission){
            $roleProfesional->givePermissionTo($permission);            
        }





        $rolePropietario    = Role::updateOrCreate(['name'=>'propietario']);












        // ASIGNAMOS ROLES Y PERMISOS -->> COMENTAR PARA PRODUCCION 

        // $supervisor1 = User::findOrFail(1);
        // $supervisor2 = User::findOrFail(2);

        // $administrativo1 = User::findOrFail(3);
        // $administrativo2 = User::findOrFail(4);
        
        // $profesional1_Martin_Feldman = User::findOrFail(5);
        // $profesional2_Ariana_Duarte = User::findOrFail(6);
        // $profesional3_Marcos_Ferreira = User::findOrFail(7);
        // $profesional4_Arquimedes = User::findOrFail(8);
        // $profesional5_Arturo_Lopez = User::findOrFail(9);
        // $profesional6_Aristobulo_Sosa = User::findOrFail(10);
        


        // $supervisor1->assignRole('supervisor');
        // $supervisor2->assignRole('supervisor');

        // $administrativo1->assignRole('administrativo');
        // $administrativo2->assignRole('administrativo');
        
        // $profesional1_Martin_Feldman->assignRole('profesional');
        // $profesional2_Ariana_Duarte->assignRole('profesional');
        // $profesional3_Marcos_Ferreira->assignRole('profesional');
        // $profesional4_Arquimedes->assignRole('profesional');
        // $profesional5_Arturo_Lopez->assignRole('profesional');
        // $profesional6_Aristobulo_Sosa->assignRole('profesional');


    }


}
