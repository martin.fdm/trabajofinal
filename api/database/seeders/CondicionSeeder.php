<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;




use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;



class CondicionSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){


        // CONDICIONES


        DB::table('condiciones')->insert([
            'condicion' => 'Creado',
            'condicion_short' => 'Creado',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Formulario a ser revisado por el Colegio',
            'condicion_short' => 'Formulario a revisar',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Formulario desaprobado',
            'condicion_short' => 'Formulario desaprobado',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Formulario incompleto',
            'condicion_short' => 'Formulario incompleto',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Listo para liquidación',
            'condicion_short' => 'Listo para liquidación',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'A la espera de Comprobante de Pago, sin confirmación de que el propietario dispone de los datos para realizar el pago',
            'condicion_short' => 'A la espera de Comprobante de Pago',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'A la espera de Comprobante de Pago; con confirmación de que el propietario dispone de los datos para realizar el pago',
            'condicion_short' => 'A la espera de Comprobante de Pago',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'A la espera de Carga de Comprobante de Pago.
            El sistema ha enviado los datos para realizar el pago a los emails del profesional y del propietario',
            'condicion_short' => 'A la espera de Comprobante de Pago',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Para cerrar',
            'condicion_short' => 'Para cerrar',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado exitosamente',
            'condicion_short' => 'Cerrado exitosamente',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado exitosamente, sin final de obra en Dirección de Obra',
            'condicion_short' => 'Cerrado exitosamente, sin final de obra',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado exitosamente, con final de obra en Dirección de Obra',
            'condicion_short' => 'Cerrado exitosamente, con final de obra',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado a falta de tareas tras reemplazo profesional en Dirección de Obra',
            'condicion_short' => 'Cerrado a falta de tareas',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado a falta de tareas tras renuncia del profesional en Dirección de Obra',
            'condicion_short' => 'Cerrado a falta de tareas',
        ]);



        DB::table('condiciones')->insert([
            'condicion' => 'Cerrado a falta de Comprobante de Pago',
            'condicion_short' => 'Cerrado a falta de Comprobante de Pago',
        ]);
        
    }



}
