<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;




use Illuminate\Support\Facades\DB;



class ConfiguracionSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    public function run(){


        // básico

        DB::table('elementos_configurables')->insert([
            'nombre_institucion'                                        => 'Colegio de Arquitectos',
            'email'                                                     => 'colegio@gmail.com',
            'localidad'                                                 => 'Posadas',
            'calle'                                                     => 'Noruega',
            'numero'                                                    => '3000',
            'logo'                                                      => null,
            'nombre_archivo_logo'                                       => null,
            'tiempo_primer_recordatorio_1er_proceso_automatizado'       => 10,
            'tiempo_segundo_recordatorio_1er_proceso_automatizado'      => 10,
            'tiempo_cerrar_expediente_1er_proceso_automatizado'         => 10,
        ]);

    }


}
