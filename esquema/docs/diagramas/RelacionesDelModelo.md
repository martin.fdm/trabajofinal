


## Relaciones del Modelo del Sistema de Gestión de Expedientes


- 1 obra tiene: 1 superficie;

- M obras pueden tener: 1 objeto, 1 tipologia, 1 localidad, 1 propietario, M partidas inmobiliarias;

<br>


- 1 tarea tiene: 1 plano, 

- M tareas pueden tener: 1 tipo de tarea;

<br>


- 1 expediente tiene: 1 comprobante de Pago, M tareas;

- M expedientes pueden tener: 1 obra, 1 condición, 1 estado, 1 profesional;

<br>


- 1 Nota Reemplazo Profesional tiene: 1 expediente, 1 profesional; 

- M Notas Reemplazo Profesional pueden tener: 1 condicion de Nota Reemplazo Profesional.

<br>


- 1 certificado tiene: 1 expediente.