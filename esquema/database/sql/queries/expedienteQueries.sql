	

-- Query from Show Expediente

	select 
        prop.propietario_nombre, prop.propietario_apellido, prop.propietario_cuit, 
        prof.profesional_nombre, prof.profesional_apellido, prof.profesional_numero_matricula,
        objetos.objeto,
        tipologias.tipologia, 
        tipos_tareas.tipo_tarea,
        localidades.localidad,
        obras.*,
        planos.plano
         
        from expedientes expt 
         
        -- recuperar nombre completo del propietario de una obra
        inner join obras on expt.obra_id = obras.obra_id
        inner join propietarios as prop on obras.propietario_id = prop.propietario_id

        -- recuperar nombre completo del profesional del expediente
        inner join profesionales as prof on expt.profesional_id = prof.profesional_id


        inner join objetos on expt.objeto_id = objetos.objeto_id

        inner join tipologias on expt.tipologia_id = tipologias.tipologia_id


        inner join tareas on expt.tarea_id = tareas.tarea_id
        inner join tipos_tareas on tareas.tipo_tarea_id = tipos_tareas.tipo_tarea_id


        -- ->join('obras', 'localidades.localidad_id', '=', 'obras.localidad_id') */
        inner join localidades on obras.localidad_id = localidades.localidad_id


        inner join planos on tareas.plano_id = planos.plano_id

     	--    ->where('estado_id','=','1') */
        where expediente_numero LIKE '%id%'
        order By expediente_id desc    ;

--  




